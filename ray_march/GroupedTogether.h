#pragma once
#include "Scene.h"
#include "Shapes.h"

class GroupedTogether : public Scene
{
public:
	GroupedTogether() = default;

	void init() override;
	Color backgroundColor() const override;
	Transform cameraTransform() override;
	std::shared_ptr<Scene> clone();
	const std::string name() const;

	~GroupedTogether();
};
