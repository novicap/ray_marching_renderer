#include "Box.h"
#include "Config.h"
#include <algorithm>
#include <iostream>

double Box::sdf()
{
	Vector3 q = m_sdfVector.abs() - m_vertex;
	return q.max(0.0).length() + std::min(std::max(q.x(), std::max(q.y(), q.z())), 0.0);
}

void Box::stream(std::ostream &strm) const
{
	strm << "Box(Relative vertex position: " << m_vertex << ")";
}

Box::~Box()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Box destroyed." << std::endl;
	}
}