#pragma once

#include <ostream>

class Vector
{
public:
	Vector(const double x, const double y) : m_x{ x }, m_y{ y } {};
	Vector() : m_x{ 0 }, m_y{ 0 } {};
	Vector(const Vector& vector) {
		m_x = vector.m_x;
		m_y = vector.m_y;
	}
	static Vector vector(double, double);

	double x() const;
	double y() const;

	void setX(const double x);
	void setY(const double y);

	double length() const;
	Vector abs() const;
	double dot(const Vector &v) const;
	//per component min/max
	Vector max(const double d) const;
	Vector min(const double d) const;

	Vector & operator+=(const Vector &v);
	Vector operator+(const Vector &v) const;
	Vector & operator-=(const Vector &v);
	Vector operator-(const Vector &v) const;
	Vector & operator*=(const Vector &v);
	Vector operator*(const Vector &v) const;
	Vector & operator/=(const Vector &v);
	Vector operator/(const Vector &v) const;
	Vector & operator*=(double d);
	Vector operator*(double d) const;
	Vector & operator/=(double d);
	Vector operator/(double d) const;
	Vector operator-() const;
	bool operator==(const Vector &v) const;
	bool operator!=(const Vector &v) const;

	~Vector();

	friend std::ostream& operator<<(std::ostream &strm, const Vector &vec) {
		return strm << "Vector(" << vec.m_x << ", " << vec.m_y << ")";
	}
private:
	double m_x, m_y;
};
