#include "Torus.h"
#include "Config.h"
#include <iostream>

double Torus::sdf()
{
	Vector xz = Vector(m_sdfVector.x(), m_sdfVector.z());
	Vector q = Vector(xz.length() - m_bigRadius, m_sdfVector.y());
	return q.length() - m_smallRadius;
}

bool Torus::isConvexOptimizable()
{
	return m_isConvexOptimizable;
}

void Torus::stream(std::ostream &strm) const
{
	strm << "Torus(Small radius: " << m_smallRadius << ", Big radius: " << m_bigRadius << ")";
}

Torus::~Torus()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Torus destroyed." << std::endl;
	}
}