#include "BufferToneMapping.h"
#include "Config.h"
#include <iostream>

int*& BufferToneMapping::imageData()
{
	return m_img;
}

void BufferToneMapping::resetImageData()
{
	m_img = new int[WIDTH*HEIGHT];
}

BufferToneMapping::~BufferToneMapping()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Buffered tone mapper destroyed." << std::endl;
	}
}