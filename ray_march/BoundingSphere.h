#pragma once
#include "Math.h"
#include "Vector3.h"
#include "Transform.h"
#include <vector>

class BoundingSphere
{
public:
	BoundingSphere();

	BoundingSphere(double radius, const Vector3& center=Vector3::zero()) {
		m_radius = radius;
		m_center = center;
		m_rayTransformation = Transform::translation(center).inverse();
		calculateVolume();
	}

	BoundingSphere(const BoundingSphere& sphere) {
		m_volume = sphere.m_volume;
		m_radius = sphere.m_radius;
		m_sdfVector = Vector3(sphere.m_sdfVector);
		m_center = Vector3(sphere.m_center);
		m_ray = Ray(sphere.m_ray);
		m_rayTransformation = Transform(sphere.m_rayTransformation);
		m_transformations = sphere.m_transformations;
		m_inverseTransformations = sphere.m_inverseTransformations;
	}

	double sdf();
	void initRay(const Ray &r);
	void applyDistance(double dO);
	const Vector3& sdfVector() const;
	const double radius() const;
	const Vector3& center() const;

	virtual void transform(const Transform &t, bool append = true);
	void applyTransformations();
	virtual const std::vector<Transform>& transformations() const;
	virtual const std::vector<Transform>& inverseTransformations() const;

	const Transform& rayTransformation() const;

	double volume() const;
	double width() const;
	double height() const;
	double depth() const;

	BoundingSphere merge(const BoundingSphere&);

	~BoundingSphere();
	friend std::ostream& operator<<(std::ostream &strm, const BoundingSphere &bs) {
		return strm << "BoundingSphere(Radius: " << bs.m_radius << ", Center: " << bs.m_center << ")";
	}
private:
	double m_volume;
	double m_radius;
	Vector3 m_sdfVector;
	Vector3 m_center;
	Ray m_ray;

	Transform m_rayTransformation = Transform::identity();
	std::vector<Transform> m_transformations;
	std::vector<Transform> m_inverseTransformations;

	void calculateVolume();
};
