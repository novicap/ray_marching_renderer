#pragma once

#include "Vector3.h"
#include <ostream>

class Color
{
public:
	Color();
	Color(const Color& c) {
		m_r = c.m_r;
		m_g = c.m_g;
		m_b = c.m_b;
	}
	static Color rgb(const double r, const double g, const double b);
	static Color hsb(const double h, const double s, const double b);
	static Color vector(const Vector3 &v);
	static Color white();
	static Color black();
	static Color gray(double k);

	void setR(const double);
	void setG(const double);
	void setB(const double);

	double r();
	double g();
	double b();

	Color & operator+=(const Color &c);
	Color operator+(const Color &c) const;
	Color & operator*=(const Color &c);
	Color operator*(const Color &c) const;
	Color & operator*=(double c);
	Color operator*(double c) const;
	Color & operator/=(const Color &c);
	Color operator/(const Color &c) const;
	bool operator==(const Color &c) const;
	bool operator!=(const Color &c) const;

	Color & pow(double c);

	double luminance() const;
	double perceivedLightness() const;
	double inverseGamma(double v) const;
	double gamma(double v) const;

	int code();
	int codeClamp();

	bool notZero() const;

	~Color();

	friend std::ostream& operator<<(std::ostream &strm, const Color &c) {
		return strm << "Color (" << c.m_r << ", " << c.m_g << ", " << c.m_b << ")";
	}
private:
	double m_r, m_g, m_b;
	Color(const double r, const double g, const double b) : m_r{ r }, m_g{ g }, m_b{ b } {}
	int toByte(const double);
	int toByteClamp(const double);
};
