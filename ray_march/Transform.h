#pragma once

#include "Vector3.h"
#include "Ray.h"
#include "Test.h"
#include "Matrix3.h"
#include <array>
#include <ostream>
#include "CGAL_setup.h"

class Transform
{
public:
	Transform(
		const double m00, const double m01, const double m02, const double m03,
		const double m10, const double m11, const double m12, const double m13,
		const double m20, const double m21, const double m22, const double m23) :
		m_00{ m00 }, m_01{ m01 }, m_02{ m02 }, m_03{ m03 },
		m_10{ m10 }, m_11{ m11 }, m_12{ m12 }, m_13{ m13 },
		m_20{ m20 }, m_21{ m21 }, m_22{ m22 }, m_23{ m23 } {};

	Transform() { identity(); };
	//copy constructor
	Transform(const Transform &t) :
		m_00{ t.m_00 }, m_01{ t.m_01 }, m_02{ t.m_02 }, m_03{ t.m_03 },
		m_10{ t.m_10 }, m_11{ t.m_11 }, m_12{ t.m_12 }, m_13{ t.m_13 },
		m_20{ t.m_20 }, m_21{ t.m_21 }, m_22{ t.m_22 }, m_23{ t.m_23 } {};

	Transform(const Transformation_3 &t) :
		m_00{ t.m(0, 0) }, m_01{ t.m(0, 1) }, m_02{ t.m(0, 2) }, m_03{ t.m(0, 3) },
		m_10{ t.m(1, 0) }, m_11{ t.m(1, 1) }, m_12{ t.m(1, 2) }, m_13{ t.m(1, 3) },
		m_20{ t.m(2, 0) }, m_21{ t.m(2, 1) }, m_22{ t.m(2, 2) }, m_23{ t.m(2, 3) }
	{}

	Transform(const Matrix3 &m) :
		m_00{ m.values()[0] }, m_01{ m.values()[1] }, m_02{ m.values()[2] }, m_03{ 0 },
		m_10{ m.values()[3] }, m_11{ m.values()[4] }, m_12{ m.values()[5] }, m_13{ 0 },
		m_20{ m.values()[6] }, m_21{ m.values()[7] }, m_22{ m.values()[8] }, m_23{ 0 } {};

	static Transform identity();
	static Transform translation(const Vector3 &v);
	static Transform rotationAroundX(const double angle);
	static Transform rotationAroundY(const double angle);
	static Transform rotationAroundZ(const double angle);
	static Transform scale(const double s);
	static Transform scale(const Vector3 &v);

	Transform& andThen(const Transform &t);
	Vector3& applyTo(Vector3 &v) const;
	Vector3& applyToWithoutTranslation(Vector3 &v) const;
	Ray& applyTo(Ray &r) const;
	Transform inverse();
	friend bool operator==(const Transform &t1, const Transform &t2);
	bool equals(const Transform &t1, double acc = 0.0000000001);

	~Transform();

	friend std::ostream& operator<<(std::ostream &strm, const Transform &t) {
		return strm << "Transform(\n"
			<< t.m_00 << ",\t " << t.m_01 << ",\t " << t.m_02 << ",\t " << t.m_03 << std::endl
			<< t.m_10 << ",\t " << t.m_11 << ",\t " << t.m_12 << ",\t " << t.m_13 << std::endl
			<< t.m_20 << ",\t " << t.m_21 << ",\t " << t.m_22 << ",\t " << t.m_23 << ")";
	}
private:
	double
		m_00, m_01, m_02, m_03,
		m_10, m_11, m_12, m_13,
		m_20, m_21, m_22, m_23;

	double determinant();
};
