#include "BoundingSphereTree.h"

std::shared_ptr<BoundingSphereTree> BoundingSphereTree::clone()
{
	std::shared_ptr<BoundingSphereTree> newBoundingSphereTree = std::make_shared<BoundingSphereTree>();
	newBoundingSphereTree->m_root = std::make_shared<BoundingSphereNode>(*m_root);
	copyNode(newBoundingSphereTree->m_root, m_root);
	return newBoundingSphereTree;
}

void BoundingSphereTree::print()
{
	if (m_root == nullptr) {
		return;
	}

	print(m_root);
}

void BoundingSphereTree::copyNode(std::shared_ptr<BoundingSphereNode> destination, std::shared_ptr<BoundingSphereNode> node)
{
	if (node == nullptr) {
		return;
	}

	if (node->left() != nullptr) {
		std::shared_ptr<BoundingSphereNode> left = std::make_shared<BoundingSphereNode>(*node->left());
		destination->addLeft(left, destination->parent());
		copyNode(destination->left(), node->left());
	}

	if (node->right() != nullptr) {
		std::shared_ptr<BoundingSphereNode> right = std::make_shared<BoundingSphereNode>(*node->right());
		destination->addRight(right, destination->parent());
		copyNode(destination->right(), node->right());
	}
}

void BoundingSphereTree::print(std::shared_ptr<BoundingSphereNode> node)
{
	if (node == nullptr) {
		return;
	}

	std::cout << node->boundingSphere().center() << std::endl;
	print(node->left());
	print(node->right());
}

std::vector<IntersectedBody> BoundingSphereTree::traverseTreeST(const Ray& r, Color& c) {
	std::vector<IntersectedBody> bodies;
	m_steps = 0;
	traverseTreeST(r, m_root, bodies, c, 0.0);
	return bodies;
}

//hierarchy image is correct, halfsphere appear due to (*******)
//taking scene objects to calculate sphere that encompasses them
void BoundingSphereTree::traverseTreeST(const Ray& r, std::shared_ptr<BoundingSphereNode>& current,
	std::vector<IntersectedBody>& bodies, Color& c, double passed)
{
	current->boundingSphere().initRay(r);
	double dO = passed;
	current->boundingSphere().applyDistance(dO);

	double prevDistance = DBL_MAX;
	double epsilon = EPSILON;
	double maxDist = MAX_DIST;
	double distance = 2 * epsilon;
	std::shared_ptr<BoundingSphereNode> left = current->left();
	std::shared_ptr<BoundingSphereNode> right = current->right();

	while (dO < maxDist && distance > epsilon) {
		distance = current->boundingSphere().sdf();
		//"convex optimisation"
		if (OPTIMISE_FOR_CONVEXITY) {
			if (distance < prevDistance) {
				prevDistance = distance;
			}
			else {
				dO = maxDist;
				break;
			}
		}
		//end of "convex optimisation"

		dO += distance;

		if (distance < epsilon && current->isLeaf()) {
			bodies.push_back(IntersectedBody(current->bodies()[0], dO));
			return;
		}

		if (distance < epsilon && left != nullptr) {
			traverseTreeST(r, left, bodies, c, dO);
		}

		if (distance < epsilon && right != nullptr) {
			
			traverseTreeST(r, right, bodies, c, dO);
		}
		m_steps++;
		current->boundingSphere().applyDistance(dO);
	}

	c.setR(m_steps); c.setG(m_steps); c.setB(m_steps);
}