#pragma once
class KDTree
{
public:
	KDTree();
	~KDTree();
private:
	enum axis
	{
		x, y, z
	};

	axis nextAxis(axis current);
};

