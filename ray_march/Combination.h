#pragma once
#include "Shape.h"

class Combination : public Shape
{
public:
	Combination(const std::shared_ptr<Shape> firstOperand, const std::shared_ptr<Shape> secondOperand) :
		m_firstOperand{ firstOperand }, m_secondOperand{ secondOperand } {
		makeAABB();
		makeOBB();
		makeBoundingSphere();
	};

	Combination(const Combination& c) : Shape(c) {
		m_firstOperand = c.m_firstOperand->clone();
		m_secondOperand = c.m_secondOperand->clone();
	}

	virtual const std::shared_ptr<Shape> clone() const = 0;

	virtual double sdf() = 0;
	virtual Vector3 estNormal_(const Vector3 &v);
	virtual bool isConvexOptimizable() override;
	std::shared_ptr<Shape>& firstOperand();
	std::shared_ptr<Shape>& secondOperand();
	void makeAABB();
	void makeOBB();
	void makeBoundingSphere();
	void applyBvhTransformations();
	void initRay(const Ray &r) override;
	void applyDistance(double dO) override;
	void setSdfVector(const Vector3 &v) override;
	void transform(const Transform &t, bool append = true) override;
	void setAndTransformSdfVector(const Vector3 &v) override;
	~Combination();

protected:
	bool m_isConvexOptimizable = false;
	void stream(std::ostream& str) const;
	std::shared_ptr<Shape> m_firstOperand;
	std::shared_ptr<Shape> m_secondOperand;
};
