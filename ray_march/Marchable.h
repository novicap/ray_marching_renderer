#pragma once

class Marchable
{
public:
	virtual double sdf() = 0;
	virtual void initRay(const Ray &r) = 0;
};