#include "Inline.h"
#include "Sphere.h"
#include "Cone.h"

void Inline::init()
{
	for (int i = 0; i < 150; i++) {
		std::shared_ptr<Sphere> s = std::make_shared<Sphere>(1);
		Transform t = Transform::translation(Vector3::vector(0, 0, 3 * i));
		s->transform(t);
		s->applyBvhTransformations();

		Scene::addBody(Body(s, Material(Color::rgb((double)rand() / RAND_MAX, (double)rand() / RAND_MAX, (double)rand() / RAND_MAX))));
	}

	std::vector<Light> lights{
		Light(Vector3::vector(0.0, 0, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(2.0, 0, -1.0), Color::gray(1.0)),
		Light(Vector3::vector(-2.0, 0,-1.0), Color::gray(1.0)),
		Light(Vector3::vector(2.0, 0, 10.0), Color::gray(1.0)),
		Light(Vector3::vector(-2.0, 0, 10.0), Color::gray(1.0)),
		Light(Vector3::vector(2.0, 0, 20.0), Color::gray(1.0)),
		Light(Vector3::vector(-2.0, 0, 20.0), Color::gray(1.0)),
		Light(Vector3::vector(2.0, 0, 30.0), Color::gray(1.0)),
		Light(Vector3::vector(-2.0, 0, 30.0), Color::gray(1.0)),
		Light(Vector3::vector(2.0, 0, 40.0), Color::gray(1.0)),
		Light(Vector3::vector(-2.0, 0, 40.0), Color::gray(1.0)),
		Light(Vector3::vector(2.0, 0, 50.0), Color::gray(1.0)),
		Light(Vector3::vector(-2.0, 0, 50.0), Color::gray(1.0)),
		Light(Vector3::vector(2.0, 0, 60.0), Color::gray(1.0)),
		Light(Vector3::vector(-2.0, 0, 60.0), Color::gray(1.0)),
		Light(Vector3::vector(2.0, 0, 70.0), Color::gray(1.0)),
		Light(Vector3::vector(-2.0, 0, 70.0), Color::gray(1.0)),
		Light(Vector3::vector(2.0, 0, 80.0), Color::gray(1.0)),
		Light(Vector3::vector(-2.0, 0, 80.0), Color::gray(1.0)),
		Light(Vector3::vector(2.0, 0, 90.0), Color::gray(1.0)),
		Light(Vector3::vector(-2.0, 0, 90.0), Color::gray(1.0)),
		Light(Vector3::vector(2.0, 0, 100.0), Color::gray(1.0)),
		Light(Vector3::vector(-2.0, 0, 100.0), Color::gray(1.0)),
		Light(Vector3::vector(2.0, 0, 110.0), Color::gray(1.0)),
		Light(Vector3::vector(-2.0, 0, 110.0), Color::gray(1.0))
	};

	Scene::addLights(lights);
}

Color Inline::backgroundColor() const
{
	return Color::gray(0.1);
}

Transform Inline::cameraTransform()
{
	return Transform::translation(Vector3::vector(0, 0, -3));
}

std::shared_ptr<Scene> Inline::clone()
{
	std::shared_ptr<Inline> sb = std::make_shared<Inline>();

	for (auto& body : m_bodies) {
		sb->addBody(body.clone());
	}
	for (auto& light : m_lights) {
		sb->addLight(Light(light));
	}

	return sb;
}

const std::string Inline::name() const
{
	return "Inline";
}

Inline::~Inline()
{
}
