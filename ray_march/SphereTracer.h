#pragma once
#include <memory>
#include <array>
#include "Color.h"
#include "Config.h"

class SphereTracer
{
public:
	virtual void renderIteration(Color*&, int*&, double&) = 0;
	virtual std::string name() = 0;
	virtual void initScene() = 0;
};