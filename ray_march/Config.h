#pragma once
#include <random>
#include <omp.h>

#define OUTPUT_DESTRUCTORS false
#define OMP_THREADS 16

#define OPTIMISE_FOR_CONVEXITY true
#define OPTIMISE_FOR_RAY_LENGTH true

#define OUTPUT_HEATMAP false

/*
if true uses bv of bodies for calculations
else uses bounding volumes for calculations
*/
#define USE_BODIES_FOR_BU_BVH true

#define ANTI_ALIASING true

#ifdef _DEBUG
#define EPSILON (double) 0.01;
#define MAX_STEPS (int) 100;
#define MAX_DIST  (double) 100.0;
#define WIDTH (int)1
#define HEIGHT (int)1
#else
#define EPSILON (double) 0.001;
#define MAX_DIST  (double) 1000.0;
#define WIDTH (int)1200
#define HEIGHT (int)600
#endif

/*
##note to myself:
Transformations are "RayCentric"
due to screen coordinate system starting at top left corner
when rendering with sdl it is required to either use
SDL_RendererFlip flip = SDL_FLIP_VERTICAL;
or multiply y coordinate in scene space with -1 (divide by -m)
when scanning scene
+----------------------------------------------------------->
|(0,0)													(x)+
|
|
|
|
|
|
|
|
|
|
|(y)+
V
*/
