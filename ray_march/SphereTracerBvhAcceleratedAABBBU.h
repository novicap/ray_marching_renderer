#pragma once
#include <memory>
#include <map>
#include "AABBTreeBottomUp.h"
#include "SphereTracer.h"
#include "Config.h"
#include "Scene.h"
#include "Camera.h"
#include "Vector.h"
#include "Ray.h"
#include "ClosestBody.h"
#include "IntersectedBody.h"

namespace single_object
{
	class SphereTracerBvhAcceleratedAABBBU : public SphereTracer
	{
	public:
		SphereTracerBvhAcceleratedAABBBU(std::shared_ptr<Scene> scene, const Camera &camera, const Vector &vector);
		void renderIteration(Color*&, int*&, double&) override;
		virtual std::string name();
		void initScene();
		~SphereTracerBvhAcceleratedAABBBU();
	private:
		ClosestBody rayMarch(const Ray &ray, const std::vector<IntersectedBody>& bodies, bool primaryRay);
		Color sampleRay(const Ray &ray, const Scene &scene, AABBTree &aabbTree, int depth, bool primaryRay);

		std::shared_ptr<Scene> m_scene;
		std::shared_ptr<AABBTree> m_aabbTree;
		std::shared_ptr<Camera> m_camera;
		Vector m_size;
		int m_iterations;
		std::unique_ptr<Color[]> m_colors;
	};
}
