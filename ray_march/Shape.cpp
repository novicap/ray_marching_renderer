#include "Shape.h"
#include "Config.h"
#include "Ray.h"
#include <iostream>

void Shape::setSdfVector(const Vector3 &v)
{
	m_sdfVector = v;
}

void Shape::setAndTransformSdfVector(const Vector3 &v)
{
	m_sdfVector = Vector3(v);
	for (auto &transform : m_inverseTransformations) {
		transform.applyTo(m_sdfVector);
	}
}

const AABB& Shape::aabb() const
{
	return m_aabb;
}

const OBB & Shape::obb() const
{
	return m_obb;
}

const BoundingSphere & Shape::boundingSphere() const
{
	return m_boundingSphere;
}

void Shape::recalculateAABB()
{
	m_aabb.calculateAABB();
}

void Shape::applyBvhTransformations()
{
	m_aabb.applyTransformations();
	m_obb.applyTransformations();
	m_obb.calculateCentroid();
	m_boundingSphere.applyTransformations();
}

const Vector3 & Shape::getSdfVector() const
{
	return m_sdfVector;
}

Vector3 Shape::applyTransforms(const Vector3 &v)
{
	Vector3 p = Vector3::vector(v.x(), v.y(), v.z());

	for (auto &transformation : transformations()) {
		transformation.applyTo(p);
	}
	return p;
}

Vector3 Shape::applyInverseTransforms(const Vector3 &v)
{
	Vector3 p = Vector3::vector(v.x(), v.y(), v.z());

	for (auto &transformation : inverseTransformations()) {
		transformation.applyTo(p);
	}

	return p;
}

bool Shape::isConvexOptimizable()
{
	return m_isConvexOptimizable;
}

//Sets Ray m_ray and Vector3 m_sdfVector, all sdfs are calculated
//for Vector3 m_sdfVector. Transformations are applied on
//initRay, and transformed ray and Vector3 m_sdfVector
//is used for all sdfs. Sdfs are calculated in "shape space".
void Shape::initRay(const Ray &r)
{
	m_ray = Ray(r);
	for (auto &transform : inverseTransformations()) {
		transform.applyTo(m_ray);
	}
	m_sdfVector = m_ray.source();
}

//Based on distance dO that ray marcher "marcher" and transformed m_ray
//sets Vector3 m_sdfVector.
void Shape::applyDistance(double dO)
{
	m_sdfVector = m_ray.source() + (m_ray.direction() * dO);
}

//Estimates normal for shape.
Vector3 Shape::estNormal_(const Vector3 &v)
{
	double epsilon = EPSILON;
	setAndTransformSdfVector(v);
	double d = sdf();

	Vector3 px = v - Vector3::vector(epsilon, 0.0, 0.0);
	setAndTransformSdfVector(px);
	double sdfPx = sdf();

	Vector3 py = v - Vector3::vector(0.0, epsilon, 0.0);
	setAndTransformSdfVector(py);
	double sdfPy = sdf();

	Vector3 pz = v - Vector3::vector(0.0, 0.0, epsilon);
	setAndTransformSdfVector(pz);
	double sdfPz = sdf();

	return Vector3::vector(d - sdfPx, d - sdfPy, d - sdfPz).normalized();
}

void Shape::transform(const Transform &t, bool append)
{
	Transform inverse = Transform(t).inverse();

	if (append) {
		m_transformations.push_back(t);
		m_inverseTransformations.push_back(inverse);
	}
	else {
		m_transformations.insert(m_transformations.begin(), t);
		m_inverseTransformations.insert(m_inverseTransformations.begin(), inverse);
	}
	
	m_aabb.transform(t, append);
	m_obb.transform(t, append);
	m_boundingSphere.transform(t, append);
}

void Shape::transformAABB(const Transform & t)
{
	m_aabb.transform(t);
	m_aabb.applyTransformations();
}

std::vector<Transform>& Shape::transformations()
{
	return m_transformations;
}

std::vector<Transform>& Shape::inverseTransformations()
{
	return m_inverseTransformations;
}

Shape::~Shape()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Shape destroyed." << std::endl;
	}
}