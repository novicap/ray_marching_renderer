#pragma once
#include "SmoothCombination.h"
class SmoothSubtraction : public SmoothCombination
{
public:
	SmoothSubtraction(std::shared_ptr<Shape> shape1, std::shared_ptr<Shape> shape2, double smoothing) :
		SmoothCombination(shape1, shape2, smoothing) {};
	virtual double sdf() override;
	const std::shared_ptr<Shape> clone() const
	{
		return std::make_shared<SmoothSubtraction>(*this);
	};
	~SmoothSubtraction();
protected:
	void stream(std::ostream&) const;
};
