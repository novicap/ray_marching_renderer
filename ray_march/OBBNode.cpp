#include "OBBNode.h"

void OBBNode::addBody(const Body& b)
{
	m_bodies.push_back(b);
}

void OBBNode::addBodies(const std::vector<Body>& b)
{
	for (auto &body : b) {
		m_bodies.push_back(body);
	}
}

void OBBNode::cloneBodies(const std::vector<Body>& b)
{
	for (auto &body : b) {
		addBody(Body(body.shape()->clone(), Material(body.material())));
	}
}

const std::vector<Body>& OBBNode::bodies() const
{
	return m_bodies;
}

OBB& OBBNode::obb()
{
	return m_obb;
}

const bool OBBNode::isLeaf() const
{
	return m_left == nullptr;
}

const double OBBNode::volume() const
{
	return m_obb.volume();
}

const std::shared_ptr<OBBNode>& OBBNode::left() const
{
	return m_left;
}

const std::shared_ptr<OBBNode>& OBBNode::right() const
{
	return m_right;
}

const std::shared_ptr<OBBNode>& OBBNode::parent() const
{
	return m_parent;
}

void OBBNode::addLeft(const std::shared_ptr<OBBNode>& node, const std::shared_ptr<OBBNode>& parent)
{
	m_left = node;
	m_parent = parent;
}

void OBBNode::addRight(const std::shared_ptr<OBBNode>& node, const std::shared_ptr<OBBNode>& parent)
{
	m_right = node;
	m_parent = parent;
}

bool OBBNode::isUsableForCalculation()
{
	return m_isUsableForCalculation;
}

void OBBNode::setCalculationUsability(bool isUsableForCalculation)
{
	m_isUsableForCalculation = isUsableForCalculation;
}

OBBNode::~OBBNode()
{
}

OBB OBBNode::calculateOBB()
{
	if (m_bodies.size() == 0) {
		throw("No bodies to calculate OBB.");
	}

	std::vector<Point_3> points;
	for (auto& body : m_bodies) {
		for (auto &point : body.obb().vertices()) {
			points.push_back(VectorPointCGALAdapter::toCGALPoint3(point));
		}
	}
	std::array<Point_3, 8> cgalObb;
	//CGAL min obb
	CGAL::oriented_bounding_box(points, cgalObb, CGAL::parameters::use_convex_hull(true));
	return OBBCGALAdapter::toObb(cgalObb);
}

std::array<Vector3, 8> OBBNode::vertices()
{
	return m_obb.vertices();
}