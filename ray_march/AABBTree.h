#pragma once
#include <vector>
#include "Body.h"
#include "IntersectedBody.h"
#include "AABBNode.h"
class AABBTree
{
public:
	std::shared_ptr<AABBTree> clone();
	virtual std::vector<IntersectedBody> traverseTreeST(const Ray& r, Color& c);
	void print();
protected:
	void copyNode(std::shared_ptr<AABBNode> destination, std::shared_ptr<AABBNode> node);
	void print(std::shared_ptr<AABBNode> node);
	std::shared_ptr<AABBNode> m_root;
	void traverseTreeST(const Ray& r, std::shared_ptr<AABBNode>& current,
		std::vector<IntersectedBody>& bodies, Color& c, double passed);
private:
	int m_steps;
};
