#include "BufferRendering.h"
#include "Config.h"
#include <iostream>
#include <omp.h>

Color*& BufferRendering::pixelColors() {
	return m_pixelColors;
}

void BufferRendering::resetPixelColors()
{
	m_pixelColors = new Color[WIDTH*HEIGHT];
}

BufferRendering::~BufferRendering()
{
	delete[] m_pixelColors;
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Buffered rendering destroyed." << std::endl;
	}
}