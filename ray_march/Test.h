#pragma once

#include "Vector3.h"

class Test
{
public:
	Test(Vector3 &a, Vector3 &b) : m_a { a }, m_b{ b } {};
	Vector3 & a() { return m_a; }
	Vector3 & b() { return m_b; }
	friend std::ostream& operator<<(std::ostream &strm, const Test &t) {
		return strm << "Test(" << t.m_a << ", " << t.m_b << ")";
	}
	~Test();
private:
	Vector3 &m_a, &m_b;
};

