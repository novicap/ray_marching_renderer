#include "Cone.h"
#include "Config.h"
#include "Vector.h"
#include "Vector3.h"
#include "Math.h"
#include <math.h>
#include <iostream>

double Cone::sdf()
{
	double xz = Vector(m_sdfVector.x(), m_sdfVector.z()).length();

	Vector w = Vector::vector(xz, m_sdfVector.y());
	Vector a = w - m_q * std::clamp((w.dot(m_q) / m_q.dot(m_q)), 0.0, 1.0);
	Vector b = w - Vector(m_q.x() * std::clamp(w.x() / m_q.x(), 0.0, 1.0), m_q.y());

	double d = std::min(a.dot(a), b.dot(b));

	double s = std::max((w.y()*m_q.x() - w.x()*m_q.y()), (m_q.y() - w.y()));

	return Math::fastSqrt(d)*SIGNUM(s);
}

void Cone::stream(std::ostream &strm) const
{
	strm << "Cone(Angle: " << m_angle << ", Height: " << m_height << ")";
}

Cone::~Cone()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Cone destroyed." << std::endl;
	}
}