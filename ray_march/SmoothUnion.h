#pragma once
#include "SmoothCombination.h"
class SmoothUnion : public SmoothCombination
{
public:
	SmoothUnion(std::shared_ptr<Shape> shape1, std::shared_ptr<Shape> shape2, double smoothing) :
		SmoothCombination(shape1, shape2, smoothing) {};
	const std::shared_ptr<Shape> clone() const
	{
		return std::make_shared<SmoothUnion>(*this);
	};
	virtual double sdf() override;
	~SmoothUnion();
protected:
	void stream(std::ostream&) const;
};
