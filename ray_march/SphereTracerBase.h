#pragma once
#include "Scene.h"
#include "Camera.h"
#include "Color.h"
#include "ClosestBody.h"
#include "SphereTracer.h"
#include <omp.h>

namespace single_object
{
	class SphereTracerBase : public SphereTracer
	{
	public:
		SphereTracerBase(std::shared_ptr<Scene> scene, const Camera &camera, const Vector &vector);
		void renderIteration(Color*&, int*&, double&) override;
		virtual std::string name();
		void initScene();
		~SphereTracerBase();
	private:
		ClosestBody rayMarch(const Ray &ray, const Scene &scene);
		Color sampleRay(const Ray &ray, const Scene &scene, int depth);

		std::shared_ptr<Scene> m_scene;
		std::shared_ptr<Camera> m_camera;
		Vector m_size;
		int m_iterations;
		std::unique_ptr<Color[]> m_colors;
	};
}
