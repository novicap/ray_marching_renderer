#pragma once
#include "Combination.h"
class Union : public Combination
{
public:
	Union() = default;
	Union(std::shared_ptr<Shape> shape1, std::shared_ptr<Shape> shape2) :
		Combination(shape1, shape2) {};

	const std::shared_ptr<Shape> clone() const
	{
		return std::make_shared<Union>(*this);
	};

	double sdf() override;
	~Union();
protected:
	void stream(std::ostream&) const;
};
