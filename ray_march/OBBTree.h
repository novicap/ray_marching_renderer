#pragma once
#include "Body.h"
#include "IntersectedBody.h"
#include "OBBNode.h"
class OBBTree
{
public:
	std::shared_ptr<OBBTree> clone();
	virtual std::vector<IntersectedBody> traverseTreeST(const Ray& r, Color& c);
	void print();
protected:
	void copyNode(std::shared_ptr<OBBNode> destination, std::shared_ptr<OBBNode> node);
	void print(std::shared_ptr<OBBNode> node);
	std::shared_ptr<OBBNode> m_root;
	void traverseTreeST(const Ray& r, std::shared_ptr<OBBNode>& current,
		std::vector<IntersectedBody>& bodies, Color& c, double passed);
private:
	int m_steps;
};
