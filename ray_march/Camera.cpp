#include "Camera.h"
#include "Config.h"
#include <iostream>

std::shared_ptr<Camera> Camera::clone()
{
	return std::make_shared<Camera>(m_z);
}

void Camera::sampleExitingRay(const Vector &pixel, Ray& ray)
{
	ray.setSource(Vector3::zero());
	ray.setDirection(Vector3::vec2z(pixel, m_z).normalized());
}

Camera::~Camera()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Camera destroyed." << std::endl;
	}
}