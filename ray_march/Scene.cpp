#include "Scene.h"
#include "Config.h"
#include <iostream>

void Scene::addBody(const Body& body)
{
	m_bodies.push_back(body);
}

void Scene::addBodies(const std::vector<Body> &bodies)
{
	for (auto &body : bodies) {
		addBody(body);
	}
}

const std::vector<Body>& Scene::getBodies() const
{
	return m_bodies;
}

void Scene::addLight(const Light& light)
{
	m_lights.push_back(light);
}

void Scene::addLights(const std::vector<Light> &lights)
{
	for (auto &light : lights) {
		addLight(light);
	}
}

const std::vector<Light>& Scene::getLights() const
{
	return m_lights;
}

Scene::~Scene()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Scene destroyed." << std::endl;
	}
}