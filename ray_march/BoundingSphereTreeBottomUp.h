#pragma once
#include "BoundingSphereTree.h"
#include "BoundingSphereNode.h"
#include "IntersectedBody.h"

class BoundingSphereTreeBottomUp : public BoundingSphereTree
{
public:
	BoundingSphereTreeBottomUp(const std::vector<Body>& bodies);
	~BoundingSphereTreeBottomUp();
private:
	void buildBottomUp(std::vector<std::shared_ptr<BoundingSphereNode>>& nodes);
};
