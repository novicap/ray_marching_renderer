#pragma once

#include "Shape.h"
#include "Vector3.h"
#include <ostream>

class Box : public Shape
{
public:
	Box(const Vector3 &vertex) : m_vertex{ vertex } {
		m_aabb = AABB(Vector3::vector(vertex.x(), vertex.y(), vertex.z()));
		m_obb = OBB(Vector3::vector(vertex.x(), vertex.y(), vertex.z()));
		m_boundingSphere = BoundingSphere(vertex.length());
		m_volume = 2 * vertex.x() * 2 * vertex.y() * 2 * vertex.z();
	};

	Box(const Box& box) : Shape(box) {
		m_vertex = Vector3(box.m_vertex);
	}
	const std::shared_ptr<Shape> clone() const override {
		return std::make_shared<Box>(*this);
	}

	std::shared_ptr<Box> duplicate() const
	{
		return std::make_shared<Box>(*this);
	}

	double sdf() override;
	~Box();

protected:
	void stream(std::ostream&) const;
	Vector3 m_vertex;
};
