#pragma once
#include "Scene.h"
class Heatmap : public Scene
{
public:
	Heatmap() = default;

	void init() override;
	Color backgroundColor() const override;
	Transform cameraTransform() override;
	std::shared_ptr<Scene> clone() override;
	const std::string name() const;

	~Heatmap();
};

