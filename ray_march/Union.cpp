#include "Union.h"
#include "Config.h"
#include <iostream>

double Union::sdf()
{
	double firstSdf = m_firstOperand->sdf();
	double secondSdf = m_secondOperand->sdf();

	return std::min(firstSdf, secondSdf);
}

void Union::stream(std::ostream &strm) const
{
	strm << "Union(\n\tFirst: " << *m_firstOperand << ",\n\tSecond: " << *m_secondOperand << ")";
}

Union::~Union()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Union destroyed." << std::endl;
	}
}