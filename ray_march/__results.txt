Date and Time: Wed Oct 27 03:47:57 2021

Renderer: SphereTracerBvhAcceleratedAABBBU - Axis Aligned Bounding Box Bottom Up
Output: Scene heatmap
Scene: Chess board
Threads: 16
Convexity optimisation: ACTIVE
Ray length optimisation: ACTIVE
Iterations: 130
Average: 0.154281
Min: 0.133887
Max: 0.194047
Bigger than average: 74
10 worst times: 
	0.169392
	0.171039
	0.17151
	0.171545
	0.172746
	0.17299
	0.173685
	0.177441
	0.180455
	0.194047
###################################################################

Date and Time: Wed Oct 27 04:02:56 2021

Renderer: SphereTracerBvhAcceleratedAABBBU - Axis Aligned Bounding Box Bottom Up
Output: Scene heatmap
Scene: Chess board
Threads: 16
Convexity optimisation: ACTIVE
Ray length optimisation: ACTIVE
Iterations: 130
Average: 0.143014
Min: 0.122578
Max: 0.172238
Bigger than average: 74
10 worst times: 
	0.157517
	0.158526
	0.159274
	0.159753
	0.160052
	0.161448
	0.161789
	0.162163
	0.162913
	0.172238
###################################################################

Date and Time: Thu Oct 28 00:02:38 2021

Renderer: Rend
Output: Scene
Scene: Chess board
Threads: 16
Convexity optimisation: ACTIVE
Ray length optimisation: ACTIVE
Iterations: 330
Average: 56.1895
Min: 2.52586
Max: 3.97489
Bigger than average: 0
10 worst times: 
	2.7242
	2.72451
	2.72478
	2.72479
	2.72492
	2.725
	2.72521
	2.72528
	2.72532
	2.72543
###################################################################

