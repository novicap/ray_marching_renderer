#pragma once
#include "Vector3.h"
#include "Shape.h"
#include <iostream>

class Sphere : public Shape
{
public:
	Sphere(double r) : m_radius{ r } {
		m_aabb = AABB(Vector3::vector(r, r, r));
		m_obb = OBB(Vector3::vector(r, r, r));
		m_boundingSphere = BoundingSphere(r);
		m_volume = m_radius * m_radius * m_radius * M_PI * 4 / 3;
	};

	Sphere(const Sphere& sphere) : Shape(sphere) {
		m_radius = sphere.m_radius;
	}

	const std::shared_ptr<Shape> clone() const override
	{
		return std::make_shared<Sphere>(*this);
	}

	std::shared_ptr<Sphere> duplicate() const
	{
		return std::make_shared<Sphere>(*this);
	}

	double sdf() override;
	~Sphere();

protected:
	void stream(std::ostream&) const;
private:
	double m_radius;
};
