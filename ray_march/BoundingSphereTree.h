#pragma once
#include <vector>
#include "Config.h"
#include "Body.h"
#include "IntersectedBody.h"
#include "BoundingSphereNode.h"

class BoundingSphereTree
{
public:
	std::shared_ptr<BoundingSphereTree> clone();
	virtual std::vector<IntersectedBody> traverseTreeST(const Ray& r, Color& c);
	void print();
protected:
	void copyNode(std::shared_ptr<BoundingSphereNode> destination, std::shared_ptr<BoundingSphereNode> node);
	void print(std::shared_ptr<BoundingSphereNode> node);
	std::shared_ptr<BoundingSphereNode> m_root;
	void traverseTreeST(const Ray& r, std::shared_ptr<BoundingSphereNode>& current,
		std::vector<IntersectedBody>& bodies, Color& c, double passed);
private:
	int m_steps;
};