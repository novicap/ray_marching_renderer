#include "Math.h"
#include <cmath>
#include <random>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <emmintrin.h>

Math::~Math()
{
}

double Math::degreeToRadian(const double angle)
{
	return angle * M_PI / 180.0;
}

double Math::radianToDegree(const double radian)
{
	return radian * 180 / M_PI;
}

double Math::mod1(const double d)
{
	return std::fmod(d, 1);
}

double Math::randomSign()
{
	srand(time(0));
	int r = rand();
	std::cout << rand() << "  " << r % 2 << std::endl;
	return r % 2 == 0 ? 1 : -1;
}

double Math::fastSqrt(double d)
{
	__m128d temp = _mm_set_sd(d);
	temp = _mm_sqrt_sd(temp, temp);
	double sqrt = _mm_cvtsd_f64(temp);

	return sqrt;
}
