#include "Sphere.h"
#include "Config.h"
#include <iostream>

double Sphere::sdf() {
	return m_sdfVector.length() - m_radius;
}

void Sphere::stream(std::ostream &strm) const
{
	strm << "Sphere(Radius:" << m_radius << ")";
}

Sphere::~Sphere()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Sphere destroyed." << std::endl;
	}
}