#pragma once
#include "SmoothCombination.h"

class SmoothIntersection : public SmoothCombination
{
public:
	SmoothIntersection(std::shared_ptr<Shape> shape1, std::shared_ptr<Shape> shape2, double smoothing) :
		SmoothCombination(shape1, shape2, smoothing) {};
	const std::shared_ptr<Shape> clone() const
	{
		return std::make_shared<SmoothIntersection>(*this);
	};
	virtual double sdf() override;
	~SmoothIntersection();
protected:
	void stream(std::ostream&) const;
};
