#pragma once
#include "Scene.h"
class Rings : public Scene
{
public:
	Rings() = default;

	void init() override;
	Color backgroundColor() const override;
	Transform cameraTransform() override;
	std::shared_ptr<Scene> clone() override;
	const std::string name() const;

	~Rings();
};

