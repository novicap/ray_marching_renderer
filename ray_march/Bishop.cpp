#include "Bishop.h"
#include "Cylinder.h"
#include "SmoothUnion.h"
#include "Union.h"
#include "Subtraction.h"
#include "Torus.h"
#include "Sphere.h"
#include "Box.h"
#include "Cone.h"

void Bishop::init()
{
	Body b = body();
	b.setMaterial(Material(Color::rgb(0.3, 0.1, 0.5)));

	std::vector<Body> bodies{
		b
	};
	std::vector<Light> lights{
		Light(Vector3::vector(0.0, 1.5, 1.0), Color::gray(1.0)),
		Light(Vector3::vector(1.0, 1.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(-1.0, 1.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 1.5, 8.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, 1.0), Color::gray(1.0)),
		Light(Vector3::vector(1.0, 3.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(-1.0, 3.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, 8.0), Color::gray(1.0)),
		Light(Vector3::vector(2.0, -3.5, 1.0), Color::gray(1.0)),
		Light(Vector3::vector(5.0, -3.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(-5.0, -3.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(-2.0, -3.5, 8.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 1.5, -1.0), Color::gray(1.0)),
		Light(Vector3::vector(1.0, 1.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(-1.0, 1.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 1.5, -8.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, -1.0), Color::gray(1.0)),
		Light(Vector3::vector(1.0, 3.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(-1.0, 3.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, -8.0), Color::gray(1.0)),
		Light(Vector3::vector(2.0, -3.5, -1.0), Color::gray(1.0)),
		Light(Vector3::vector(5.0, -3.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(-5.0, -3.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(-2.0, -3.5, -8.0), Color::gray(1.0))
	};
	Scene::addBodies(bodies);
	Scene::addLights(lights);
}

Color Bishop::backgroundColor() const
{
	return Color::gray(0.5);
}

Transform Bishop::cameraTransform()
{
	return Transform::rotationAroundX(25).andThen(Transform::translation(Vector3::vector(0.0, 2.0, -8.0)));
}

std::shared_ptr<Scene> Bishop::clone()
{
	std::shared_ptr<Bishop> sb = std::make_shared<Bishop>();

	for (auto& body : m_bodies) {
		sb->addBody(body.clone());
	}
	for (auto& light : m_lights) {
		sb->addLight(Light(light));
	}

	return sb;
}

const std::string Bishop::name() const
{
	return "Bishop";
}

Body Bishop::body()
{
	std::shared_ptr<Cylinder> headBase = std::make_shared<Cylinder>(0.4, 0.25);
	std::shared_ptr<Torus> ring1 = std::make_shared<Torus>(0.2, 0.25);
	std::shared_ptr<SmoothUnion> s1 = std::make_shared<SmoothUnion>(headBase, ring1, 0.4);
	std::shared_ptr<Box> opening = std::make_shared<Box>(Vector3::vector(0.5, 0.04, 0.5));
	Transform t1 = Transform::translation(Vector3::vector(0.4, 0.3, 0));
	Transform r1 = Transform::rotationAroundZ(35);
	opening->transform(t1);
	opening->transform(r1);
	opening->applyBvhTransformations();
	std::shared_ptr<Subtraction> s2 = std::make_shared<Subtraction>(opening, s1);
	std::shared_ptr<Sphere> headTip = std::make_shared<Sphere>(0.25);
	Transform t2 = Transform::translation(Vector3::vector(0, 0.55, 0));
	headTip->transform(t2);
	headTip->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s3 = std::make_shared<SmoothUnion>(s2, headTip, 0.03);



	std::shared_ptr<Cone> bodyBase = std::make_shared<Cone>(15, 2.3);
	/*Transform tt2 = Transform::translation(Vector3::vector(0, 0, 0));
	bodyBase->transform(tt2);
	bodyBase->applyBvhTransformations();*/
	std::shared_ptr<SmoothUnion> s4 = std::make_shared<SmoothUnion>(bodyBase, s3, 0.3);

	std::shared_ptr<Torus> ring4 = std::make_shared<Torus>(0.15, 0.35);
	Transform tt8 = Transform::translation(Vector3::vector(0, -0.9, 0));
	ring4->transform(tt8);
	ring4->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s5 = std::make_shared<SmoothUnion>(ring4, s4, 0.2);
	std::shared_ptr<Torus> ring5 = std::make_shared<Torus>(0.05, 0.25);
	Transform tt9 = Transform::translation(Vector3::vector(0, -0.6, 0));
	ring5->transform(tt9);
	ring5->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s6 = std::make_shared<SmoothUnion>(ring5, s5, 0.2);


	std::shared_ptr<Cylinder> baseBase = std::make_shared<Cylinder>(0.35, 0.65);
	Transform tt3 = Transform::translation(Vector3::vector(0, -2.3, 0));
	baseBase->transform(tt3);
	baseBase->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s7 = std::make_shared<SmoothUnion>(baseBase, s6, 0.3);
	std::shared_ptr<Torus> ring2 = std::make_shared<Torus>(0.05, 0.5);
	Transform t6 = Transform::translation(Vector3::vector(0, -1.75, 0));
	ring2->transform(t6);
	ring2->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s8 = std::make_shared<SmoothUnion>(ring2, s7, 0.3);

	std::shared_ptr<Torus> ring3 = std::make_shared<Torus>(0.1, 0.65);
	Transform t7 = Transform::translation(Vector3::vector(0, -2.6, 0));
	ring3->transform(t7);
	ring3->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s9 = std::make_shared<SmoothUnion>(ring3, s8, 0.03);

	return Body(s9, Material(Color::gray(0.9)));
}


Bishop::~Bishop()
{
}
