#pragma once
#include "BoundingSphere.h"
#include "BoundingSphereCGALAdapter.h"
#include "Body.h"

class BoundingSphereNode
{
public:
	BoundingSphereNode() : m_parent{ nullptr }, m_left{ nullptr }, m_right{ nullptr }, m_boundingSphere{ BoundingSphere() } {};

	//pass to boundingsphere node either vector of bodies or single body,
	//node will calculate boundingsphere box for itself with
	//calculateBoundingSphere method
	BoundingSphereNode(const std::vector<Body>& b) {
		m_parent = nullptr;
		m_left = nullptr;
		m_right = nullptr;
		addBodies(b);
		m_boundingSphere = calculateBoundingSphere();
	};

	BoundingSphereNode(const Body& b) {
		m_parent = nullptr;
		m_left = nullptr;
		m_right = nullptr;
		addBody(b);
		m_boundingSphere = b.boundingSphere();
	};

	BoundingSphereNode(const std::shared_ptr<BoundingSphereNode>& first,
		const std::shared_ptr<BoundingSphereNode>& second) {
		m_parent = nullptr;
		m_left = nullptr;
		m_right = nullptr;
		m_boundingSphere = first->boundingSphere().merge(second->boundingSphere());
	}

	//copy constructor for root nodes
	BoundingSphereNode(const BoundingSphereNode& boundingSphereNode) {
		m_parent = nullptr;
		m_left = nullptr;
		m_right = nullptr;
		m_boundingSphere = BoundingSphere(boundingSphereNode.m_boundingSphere);
		cloneBodies(boundingSphereNode.bodies());
	}

	BoundingSphere& boundingSphere();

	int size();

	//width
	double bodySetXAxisLength() const;
	//height
	double bodySetYAxisLength() const;
	//depth
	double bodySetZAxisLength() const;

	double minBodyXAxis() const;
	double minBodyYAxis() const;
	double minBodyZAxis() const;
	double maxBodyXAxis() const;
	double maxBodyYAxis() const;
	double maxBodyZAxis() const;

	void addBody(const Body& b);
	void addBodies(const std::vector<Body>& b);
	void cloneBodies(const std::vector<Body>& b);

	const std::vector<Body>& bodies() const;
	const bool isLeaf() const;
	const double volume() const;
	const std::shared_ptr<BoundingSphereNode>& left() const;
	const std::shared_ptr<BoundingSphereNode>& right() const;
	const std::shared_ptr<BoundingSphereNode>& parent() const;

	void addLeft(const std::shared_ptr<BoundingSphereNode>& node, const std::shared_ptr<BoundingSphereNode>& parent);
	void addRight(const std::shared_ptr<BoundingSphereNode>& node, const std::shared_ptr<BoundingSphereNode>& parent);

	BoundingSphere merge(const std::shared_ptr<BoundingSphereNode>&);

	~BoundingSphereNode();
private:
	std::shared_ptr<BoundingSphereNode> m_parent, m_left, m_right;
	int m_height;
	BoundingSphere m_boundingSphere;
	std::vector<Body> m_bodies;
	BoundingSphere calculateBoundingSphere();
};
