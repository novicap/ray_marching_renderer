#pragma once
#define _USE_MATH_DEFINES
#include <omp.h>
#include <math.h>

#ifndef SIGNUM
#define SIGNUM(m_v) (((m_v) < 0) ? (-1.0) : (+1.0))
#endif

class Math
{
public:
	~Math();
	static double degreeToRadian(const double angle);
	static double radianToDegree(const double radian);
	static double mod1(const double d);
	static double randomSign();
	static double fastSqrt(double d);
};
