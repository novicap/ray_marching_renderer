#pragma once
#include "Shape.h"
class Alteration : public Shape
{
public:
	Alteration(std::shared_ptr<Shape> shape) : m_shape{ shape } {};

	virtual double sdf() override = 0;
	void initRay(const Ray &r) override;
	void applyDistance(double dO) override;
	void setSdfVector(const Vector3 &v) override;
	void setAndTransformSdfVector(const Vector3 &v) override;
	Vector3 estNormal_(const Vector3 &v) override;
	~Alteration();
protected:
	std::shared_ptr<Shape> m_shape;
	void stream(std::ostream& str) const = 0;
};
