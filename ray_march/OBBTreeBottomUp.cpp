#include "OBBTreeBottomUp.h"
#include "Config.h"
#include "IntersectedBody.h"
#include <iostream>

OBBTreeBottomUp::OBBTreeBottomUp(const std::vector<Body>& bodies)
{
	if (bodies.size() == 0) {
		throw ("Can't build tree, no bodies.");
	}
	std::cout << "Building BVH Tree - OBBTreeBottomUp (will take some time)." << std::endl;
	std::vector<std::shared_ptr<OBBNode>> nodes;
	for (auto &body : bodies) {
		nodes.push_back(std::make_shared<OBBNode>(body));
	}
	buildBottomUp(nodes);
}

void OBBTreeBottomUp::buildBottomUp(std::vector<std::shared_ptr<OBBNode>>& nodes)
{
	int size = nodes.size();
	int maxSize = 2 * size;
	int elements = nodes.size();

	if (nodes.size() == 1) {
		m_root = nodes[0];
		nodes.clear();
		return;
	}

	//will be passed to recursive call
	std::vector<std::shared_ptr<OBBNode>> candidates(maxSize * maxSize, nullptr);
	while (size > 0) {

		int minI, minJ;
		double minVolume = DBL_MAX;

		std::shared_ptr<OBBNode> testNode, node;
		//find that makes obb of minimal volume
		for (int i = 0; i < nodes.size() - 1; i++) {
			for (int j = i + 1; j < nodes.size(); j++) {
				int index = (i * maxSize + j);

				if (!nodes[i]->isUsableForCalculation()) {
					break;
				}
				//node is unusable if it was used for calculating some bv
				//for example if 1 and 2 were used to calculate (1,2) bv, then
				//neither 1 or 2 can be used later on its own, since it could generate
				//bvs (1,3), (1,4) etc...
				if (!nodes[j]->isUsableForCalculation()) 
				{
					continue;
				}

				if (candidates[index] == nullptr) 
				{
					if (USE_BODIES_FOR_BU_BVH) 
					{
						std::vector<Body> bodies = nodes[i]->bodies();
						bodies.insert(bodies.end(), nodes[j]->bodies().begin(), nodes[j]->bodies().end());
						candidates[index] = std::make_shared<OBBNode>(bodies);
					}
					else 
					{
						candidates[index] = std::make_shared<OBBNode>(nodes[i], nodes[j]);
					}
				}
				
				double volume = candidates[index]->volume();
				if (volume < minVolume) 
				{
					minI = i;
					minJ = j;
					minVolume = volume;
					node = candidates[index];
				}
			}
		}

		//children of created node are minI-th and minJ-th node
		node->addLeft(nodes[minI], node);
		node->addRight(nodes[minJ], node);

		//clear nodes for while loop (first greter index to avoid index shift)
		nodes[minJ]->setCalculationUsability(false);
		nodes[minI]->setCalculationUsability(false);
		size -= 1;
		elements += 1;

		//add new node to vector to be used
		nodes.push_back(node);
		if (elements == maxSize - 1) {
			m_root = nodes[maxSize - 2];
			return;
		}
	}
}

OBBTreeBottomUp::~OBBTreeBottomUp()
{
}