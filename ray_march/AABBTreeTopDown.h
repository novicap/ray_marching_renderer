#pragma once
#include "AABBTree.h"
#include "Body.h"
#include <functional>

class AABBTreeTopDown : public AABBTree
{
public:
	AABBTreeTopDown(const std::vector<Body>& bodies);
	~AABBTreeTopDown();
private:
	void buildTopDown(const std::shared_ptr<AABBNode>& parent, const std::vector<Body>& b, int start, int end, bool isLeftChild);
	std::function<bool(const Body&, const Body&)> sortMethod(const std::shared_ptr<AABBNode>& node);
};
