#pragma once
#include <iostream>
#include "Shape.h"

// Cone in `cannonical` form
//			^y  z
//			|  /
//			| /
//	 -------+------>x
//		   /|\
//		  /	| \
//		 /__|__\
//		/	|   \
//		\___|___/

class Cone : public Shape
{
public:
	Cone(double angle, double height) : m_angle{ angle }, m_height{ height } {
		double angleRad = Math::degreeToRadian(angle);
		m_angleSin = sin(angleRad);
		m_angleCos = cos(angleRad);
		double radius = m_height * (m_angleSin / m_angleCos);
		m_q = Vector::vector(radius, -m_height);
		
		m_aabb = AABB(Vector3::vector(radius, height / 2, radius));
		m_obb = OBB(Vector3::vector(radius, height / 2, radius));
		m_boundingSphere = BoundingSphere(m_aabb.vertex().length());
		//box is created with centroid on (0, 0, 0) so we shift it down to encompass cone
		m_aabb.transform(Transform::translation(Vector3::vector(0, -height / 2, 0)));
		m_obb.transform(Transform::translation(Vector3::vector(0, -height / 2, 0)));
		m_boundingSphere.transform(Transform::translation(Vector3::vector(0, -height / 2, 0)));

		m_volume = radius * radius * M_PI * height / 3;
	};

	Cone(const Cone& cone) : Shape(cone) {
		m_angle = cone.m_angle;
		m_angleSin = cone.m_angleSin;
		m_angleCos = cone.m_angleCos;
		m_height = cone.m_height;
		m_q = Vector(cone.m_q);
	}

	const std::shared_ptr<Shape> clone() const override {
		return std::make_shared<Cone>(*this);
	}

	double sdf() override;
	~Cone();
protected:
	void stream(std::ostream&) const;
private:
	double m_angle;
	double m_angleSin;
	double m_angleCos;
	double m_height;
	Vector m_q;
};
