#include "Ray.h"
#include "Config.h"
#include "iostream"

Ray Ray::sd(const Vector3 &s, const Vector3 &d)
{
	return Ray(s, d);
}

Ray Ray::sv(Vector3 &s, Vector3 &v)
{
	v -= s;
	return sd(s, v);
}

const Vector3 & Ray::source() const
{
	return m_source;
}

const Vector3 & Ray::direction() const
{
	return m_direction;
}

void Ray::setSource(const Vector3 &v)
{
	m_source = v;
}

void Ray::setDirection(const Vector3 &v)
{
	m_direction = v;
}

Ray & Ray::normalized_()
{
	m_direction.normalize();
	return *this;
}

Ray::~Ray()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Ray destroyed." << std::endl;
	}
}