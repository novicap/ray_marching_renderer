#pragma once
#include "AABBTree.h"
#include "AABBNode.h"
#include "IntersectedBody.h"

class AABBTreeBottomUp : public AABBTree
{
public:
	AABBTreeBottomUp(const std::vector<Body>& bodies);
	~AABBTreeBottomUp();
private:
	void buildBottomUp(std::vector<std::shared_ptr<AABBNode>>& nodes);
};
