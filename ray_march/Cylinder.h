#pragma once

#include "Shape.h"
#include "Math.h"

class Cylinder : public Shape
{
public:
	Cylinder(double height, double radius) : m_height{ height }, m_radius{ radius }
	{
		m_aabb = AABB(Vector3::vector(radius, height, radius));
		m_obb = OBB(Vector3::vector(radius, height, radius));
		m_boundingSphere = BoundingSphere(sqrt(height*height + radius * radius));
		m_volume = radius * radius * M_PI * height;
	};

	Cylinder(const Cylinder& cylinder) : Shape(cylinder)
	{
		m_height = cylinder.m_height;
		m_radius = cylinder.m_radius;
	}

	const std::shared_ptr<Shape> clone() const override
	{
		return std::make_shared<Cylinder>(*this);
	}

	double sdf() override;
	~Cylinder();
protected:
	void stream(std::ostream&) const;
private:
	double m_height;
	double m_radius;
};
