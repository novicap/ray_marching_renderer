#include "IntersectedBody.h"

double IntersectedBody::distance() const
{
	return m_distance;
}

const Body & IntersectedBody::body() const
{
	return m_body;
}

IntersectedBody::~IntersectedBody()
{
}