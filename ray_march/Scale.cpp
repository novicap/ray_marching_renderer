#include "Scale.h"
#include <iostream>

const std::shared_ptr<Shape> Scale::clone() const
{
	return std::make_shared<Scale>(m_shape->clone(), m_scaleFactor);
}

double Scale::sdf()
{
	Vector3 sdfVector = m_shape->getSdfVector() / m_scaleFactor;
	m_shape->setSdfVector(sdfVector);
	return m_shape->sdf()*m_scaleFactor;
}

void Scale::stream(std::ostream &strm) const
{
	strm << "ScaledShape(" << *m_shape << ", ScalingFactor:" << m_scaleFactor << ")" << std::endl;
}

Scale::~Scale()
{
}