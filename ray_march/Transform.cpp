#include "Transform.h"
#include "Math.h"
#include "Config.h"
#include <iostream>

Transform Transform::identity()
{
	return Transform(
		1.0, 0.0, 0.0, 0.0,
		0.0, 1.0, 0.0, 0.0,
		0.0, 0.0, 1.0, 0.0
	);
}

Transform Transform::translation(const Vector3 &v)
{
	return Transform(
		1.0, 0.0, 0.0, v.x(),
		0.0, 1.0, 0.0, v.y(),
		0.0, 0.0, 1.0, v.z()
	);
}

Transform Transform::rotationAroundX(const double angle)
{
	double radian = Math::degreeToRadian(angle);
	return Transform(
		1.0, 0.0, 0.0, 0.0,
		0.0, cos(radian), -sin(radian), 0.0,
		0.0, sin(radian), cos(radian), 0.0);
}

Transform Transform::rotationAroundY(const double angle)
{
	double radian = Math::degreeToRadian(angle);
	return Transform(
		cos(radian), 0.0, sin(radian), 0.0,
		0.0, 1.0, 0.0, 0.0,
		-sin(radian), 0.0, cos(radian), 0.0
	);
}

Transform Transform::rotationAroundZ(const double angle)
{
	double radian = Math::degreeToRadian(angle);
	return Transform(
		cos(radian), -sin(radian), 0.0, 0.0,
		sin(radian), cos(radian), 0.0, 0.0,
		0.0, 0.0, 1.0, 0.0
	);
}

Transform Transform::scale(const double coef)
{
	return Transform(
		coef, 0.0, 0.0, 0.0,
		0.0, coef, 0.0, 0.0,
		0.0, 0.0, coef, 0.0
	);
}

Transform Transform::scale(const Vector3 &v)
{
	return Transform(
		v.x(), 0.0, 0.0, 0.0,
		0.0, v.y(), 0.0, 0.0,
		0.0, 0.0, v.z(), 0.0);
}

Transform & Transform::andThen(const Transform &v)
{
	double a_00 = v.m_00*m_00 + v.m_01*m_10 + v.m_02*m_20;
	double a_01 = v.m_00*m_01 + v.m_01*m_11 + v.m_02*m_21;
	double a_02 = v.m_00*m_02 + v.m_01*m_12 + v.m_02*m_22;
	double a_03 = v.m_00*m_03 + v.m_01*m_13 + v.m_02*m_23 + v.m_03;

	double a_10 = v.m_10*m_00 + v.m_11*m_10 + v.m_12*m_20;
	double a_11 = v.m_10*m_01 + v.m_11*m_11 + v.m_12*m_21;
	double a_12 = v.m_10*m_02 + v.m_11*m_12 + v.m_12*m_22;
	double a_13 = v.m_10*m_03 + v.m_11*m_13 + v.m_12*m_23 + v.m_13;

	double a_20 = v.m_20*m_00 + v.m_21*m_10 + v.m_22*m_20;
	double a_21 = v.m_20*m_01 + v.m_21*m_11 + v.m_22*m_21;
	double a_22 = v.m_20*m_02 + v.m_21*m_12 + v.m_22*m_22;
	double a_23 = v.m_20*m_03 + v.m_21*m_13 + v.m_22*m_23 + v.m_23;

	m_00 = a_00; m_01 = a_01; m_02 = a_02; m_03 = a_03;
	m_10 = a_10; m_11 = a_11; m_12 = a_12; m_13 = a_13;
	m_20 = a_20; m_21 = a_21; m_22 = a_22; m_23 = a_23;

	return *this;
}

Transform Transform::inverse()
{
	double det = determinant();
	if (det == 0) {
		throw("Determinant is 0.");
	}

	return Transform(
		(m_11*m_22 - m_12 * m_21) / det, -(m_01*m_22 - m_02 * m_21) / det, (m_01*m_12 - m_02 * m_11) / det, -(m_01*m_12*m_23 + m_02 * m_13*m_21 + m_03 * m_11*m_22 - m_03 * m_12*m_21 - m_02 * m_11*m_23 - m_01 * m_13*m_22) / det,
		-(m_10*m_22 - m_12 * m_20) / det, (m_00*m_22 - m_02 * m_20) / det, -(m_00*m_12 - m_02 * m_10) / det, (m_00*m_12*m_23 + m_02 * m_13*m_20 + m_03 * m_10*m_22 - m_03 * m_12*m_20 - m_02 * m_10*m_23 - m_00 * m_13*m_22) / det,
		(m_10*m_21 - m_11 * m_20) / det, -(m_00*m_21 - m_01 * m_20) / det, (m_00*m_11 - m_01 * m_10) / det, -(m_00*m_11*m_23 + m_01 * m_13*m_20 + m_03 * m_10*m_21 - m_03 * m_11*m_20 - m_01 * m_10*m_23 - m_00 * m_13*m_21) / det);
}

bool Transform::equals(const Transform & t1, double acc)
{
	bool value =
		abs(t1.m_00 - m_00) > acc || abs(t1.m_01 - m_01) > acc || abs(t1.m_02 - m_02) > acc || abs(t1.m_03 - m_03) > acc ||
		abs(t1.m_10 - m_10) > acc || abs(t1.m_11 - m_11) > acc || abs(t1.m_12 - m_12) > acc || abs(t1.m_13 - m_13) > acc ||
		abs(t1.m_20 - m_20) > acc || abs(t1.m_21 - m_21) > acc || abs(t1.m_22 - m_22) > acc || abs(t1.m_23 - m_23) > acc;

	return !value;
}

Vector3& Transform::applyTo(Vector3 &v) const
{
	double x = m_00 * v.x() + m_01 * v.y() + m_02 * v.z() + m_03;
	double y = m_10 * v.x() + m_11 * v.y() + m_12 * v.z() + m_13;
	double z = m_20 * v.x() + m_21 * v.y() + m_22 * v.z() + m_23;

	v.setX(x);
	v.setY(y);
	v.setZ(z);

	return v;
}

Vector3 & Transform::applyToWithoutTranslation(Vector3 &v) const
{
	double x = m_00 * v.x() + m_01 * v.y() + m_02 * v.z();
	double y = m_10 * v.x() + m_11 * v.y() + m_12 * v.z();
	double z = m_20 * v.x() + m_21 * v.y() + m_22 * v.z();

	v.setX(x);
	v.setY(y);
	v.setZ(z);

	return v;
}

Ray& Transform::applyTo(Ray &r) const
{
	Vector3 source = r.source();
	Vector3 direction = r.direction();

	r.setSource(applyTo(source));
	r.setDirection(applyToWithoutTranslation(direction));
	return r;
}

double Transform::determinant()
{
	return (
		+(m_00 * m_11 * m_22)
		+ (m_01 * m_12 * m_20)
		+ (m_02 * m_10 * m_21)
		- (m_02 * m_11 * m_20)
		- (m_01 * m_10 * m_22)
		- (m_00 * m_12 * m_21)
		);
}

Transform::~Transform()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Transform destroyed." << std::endl;
	}
}

bool operator==(const Transform & t1, const Transform & t2)
{
	bool value =
		t1.m_00 != t2.m_00 || t1.m_01 != t2.m_01 || t1.m_02 != t2.m_02 || t1.m_03 != t2.m_03 ||
		t1.m_10 != t2.m_10 || t1.m_11 != t2.m_11 || t1.m_12 != t2.m_12 || t1.m_13 != t2.m_13 ||
		t1.m_20 != t2.m_20 || t1.m_21 != t2.m_21 || t1.m_22 != t2.m_22 || t1.m_23 != t2.m_23;

	return !value;
}
