#include "Matrix3.h"

Matrix3 Matrix3::inverse()
{
	double det = determinant();
	if (det == 0) {
		throw("Zero determinant.");
	}

	Matrix3 cof = cofactors();
	cof.transpose();
	return cof * (1 / det);
}

Matrix3 Matrix3::minors()
{
	return Matrix3(
		m_11*m_22 - m_12 * m_21, m_10*m_22 - m_12 * m_20, m_10*m_21 - m_11 * m_20,
		m_01*m_22 - m_02 * m_21, m_00*m_22 - m_02 * m_20, m_00*m_21 - m_01 * m_20,
		m_01*m_12 - m_11 * m_02, m_00*m_12 - m_02 * m_10, m_00*m_11 - m_01 * m_10
	);
}

Matrix3 Matrix3::cofactors()
{
	Matrix3 mrs = minors();
	mrs.m_01 = -mrs.m_01;
	mrs.m_10 = -mrs.m_10;
	mrs.m_12 = -mrs.m_12;
	mrs.m_21 = -mrs.m_21;
	return mrs;
}

Matrix3 Matrix3::transpose()
{
	double temp = m_10;
	m_10 = m_01;
	m_01 = temp;
	temp = m_20;
	m_20 = m_02;
	m_02 = temp;
	temp = m_21;
	m_21 = m_12;
	m_12 = temp;
	return *this;
}

double Matrix3::determinant()
{
	return (+(m_00 * m_11 * m_22)
		+ (m_01 * m_12 * m_20)
		+ (m_02 * m_10 * m_21)
		- (m_02 * m_11 * m_20)
		- (m_01 * m_10 * m_22)
		- (m_00 * m_12 * m_21));
}

const std::array<double, 9> Matrix3::values() const
{
	return std::array<double, 9> {
		m_00, m_01, m_02,
			m_10, m_11, m_12,
			m_20, m_21, m_22
	};
}

Matrix3 & Matrix3::operator+=(const Matrix3 & m)
{
	m_00 += m.m_00;
	m_01 += m.m_01;
	m_02 += m.m_02;
	m_10 += m.m_10;
	m_11 += m.m_11;
	m_12 += m.m_12;
	m_20 += m.m_20;
	m_21 += m.m_12;
	m_22 += m.m_22;

	return *this;
}

Matrix3 Matrix3::operator+(const Matrix3 & m) const
{
	return Matrix3(
		m_00 + m.m_00, m_01 + m.m_01, m_02 + m.m_02,
		m_10 + m.m_10, m_11 + m.m_11, m_12 + m.m_12,
		m_20 + m.m_20, m_21 + m.m_12, m_22 + m.m_22);
}

Matrix3 & Matrix3::operator-=(const Matrix3 & m)
{
	m_00 -= m.m_00;
	m_01 -= m.m_01;
	m_02 -= m.m_02;
	m_10 -= m.m_10;
	m_11 -= m.m_11;
	m_12 -= m.m_12;
	m_20 -= m.m_20;
	m_21 -= m.m_12;
	m_22 -= m.m_22;

	return *this;
}

Matrix3 Matrix3::operator-(const Matrix3 & m) const
{
	return Matrix3(
		m_00 - m.m_00, m_01 - m.m_01, m_02 - m.m_02,
		m_10 - m.m_10, m_11 - m.m_11, m_12 - m.m_12,
		m_20 - m.m_20, m_21 - m.m_12, m_22 - m.m_22);
}

Matrix3 Matrix3::operator*(const Matrix3 & m) const
{
	return Matrix3(
		m_00 * m.m_00 + m_01 * m.m_10 + m_02 * m.m_20, m_00 * m.m_01 + m_01 * m.m_11 + m_02 * m.m_21, m_00 * m.m_02 + m_01 * m.m_12 + m_02 * m.m_22,
		m_10 * m.m_00 + m_11 * m.m_10 + m_12 * m.m_20, m_10 * m.m_01 + m_11 * m.m_11 + m_12 * m.m_21, m_10 * m.m_02 + m_11 * m.m_12 + m_12 * m.m_22,
		m_20 * m.m_00 + m_21 * m.m_10 + m_22 * m.m_20, m_20 * m.m_01 + m_21 * m.m_11 + m_22 * m.m_21, m_20 * m.m_02 + m_21 * m.m_12 + m_22 * m.m_22
	);
}

Matrix3 & Matrix3::operator*=(double d)
{
	m_00 = d * m_00;
	m_01 = d * m_01;
	m_02 = d * m_02;
	m_10 = d * m_10;
	m_11 = d * m_11;
	m_12 = d * m_12;
	m_20 = d * m_20;
	m_21 = d * m_21;
	m_22 = d * m_22;

	return *this;
}

Matrix3 Matrix3::operator*(double d) const
{
	return Matrix3(
		d*m_00, d*m_01, d*m_02,
		d*m_10, d*m_11, d*m_12,
		d*m_20, d*m_21, d*m_22
	);
}

Matrix3::~Matrix3()
{
}