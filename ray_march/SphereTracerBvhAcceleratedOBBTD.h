#pragma once
#include <memory>
#include <map>
#include "Config.h"
#include "Scene.h"
#include "Camera.h"
#include "Vector.h"
#include "Ray.h"
#include "ClosestBody.h"
#include "IntersectedBody.h"
#include "OBBTreeTopDown.h"
#include "SphereTracer.h"

namespace single_object
{
	class SphereTracerBvhAcceleratedOBBTD : public SphereTracer
	{
	public:
		SphereTracerBvhAcceleratedOBBTD(std::shared_ptr<Scene> scene, const Camera& camera, const Vector& size);
		void renderIteration(Color*&, int*&, double&) override;
		virtual std::string name();
		void initScene();
		~SphereTracerBvhAcceleratedOBBTD();
	private:
		ClosestBody rayMarch(const Ray &ray, const std::vector<IntersectedBody>& bodies, bool primaryRay);
		Color sampleRay(const Ray &ray, const Scene &scene, OBBTree &obbTree, int depth, bool primaryRay);

		std::shared_ptr<Scene> m_scene;
		std::shared_ptr<OBBTree> m_obbTree;
		std::shared_ptr<Camera> m_camera;
		Vector m_size;
		int m_iterations;
		std::unique_ptr<Color[]> m_colors;
	};
}
