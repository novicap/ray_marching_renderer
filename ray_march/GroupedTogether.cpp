#include "GroupedTogether.h"
#include "Config.h"
#include "Rounding.h"
#include "Scale.h"
#include "SmoothUnion.h"
#include "Sphere.h"

void GroupedTogether::init()
{
	srand(5);
	for (int i = 0; i < 150; i++) {
		double size = ((double)rand() / RAND_MAX)+0.1;
		int signRand = rand() % 2;
		double x = (double)rand() / RAND_MAX * 10;
		double y = (double)rand() / RAND_MAX * 10;
		double z = (double)rand() / RAND_MAX * 10;

		std::shared_ptr<Box> shape = std::make_shared<Box>(Vector3::vector(size/1.4, size/1.4, size/1.4));

		shape->transform(Transform::rotationAroundZ(45).andThen(Transform::rotationAroundX(45)).andThen(Transform::translation(Vector3::vector(x, y, z))));
		shape->applyBvhTransformations();
		Body b = Body(shape, Material(Color::rgb((double)rand() / RAND_MAX, (double)rand() / RAND_MAX, (double)rand() / RAND_MAX)));
		Scene::addBody(b);
	}

	//lights
	std::vector<Light> lights{
		Light(Vector3::vector(-1.0, 0, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(3.0, 0, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(6.0, 0, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(11.0, 0, -3.0), Color::gray(1.0)),

		Light(Vector3::vector(-1.0, 3.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(3.0, 3.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(6.0, 3.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(11.0, 3.5, -3.0), Color::gray(1.0)),

		Light(Vector3::vector(-1.0, 5.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(3.0, 5.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(6.0, 5.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(11.0, 5.5, -3.0), Color::gray(1.0)),

		Light(Vector3::vector(-1.0, 7.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(3.0, 7.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(6.0, 7.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(11.0, 7.5, -3.0), Color::gray(1.0)),

		Light(Vector3::vector(-1.0, 9.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(3.0, 9.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(6.0, 9.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(11.0, 9.5, -3.0), Color::gray(1.0)),

		Light(Vector3::vector(-1.0, 11.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(3.0, 11.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(6.0, 11.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(11.0, 11.5, -3.0), Color::gray(1.0)),

		Light(Vector3::vector(5.0, 5., 5.0), Color::gray(1.0)),
	};

	Scene::addLights(lights);
}

Color GroupedTogether::backgroundColor() const
{
	return Color::gray(0.0);
}

Transform GroupedTogether::cameraTransform()
{
	return Transform::translation(Vector3::vector(4, 4.5, -18));
}

std::shared_ptr<Scene> GroupedTogether::clone()
{
	std::shared_ptr<GroupedTogether> scene = std::make_shared<GroupedTogether>();
	for (auto& body : m_bodies) {
		scene->addBody(body.clone());
	}
	for (auto& light : m_lights) {
		scene->addLight(Light(light));
	}

	return scene;
}

const std::string GroupedTogether::name() const
{
	return "Grouped Together";
}

GroupedTogether::~GroupedTogether()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Everyone destroyed." << std::endl;
	}
}