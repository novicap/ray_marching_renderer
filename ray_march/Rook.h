#pragma once

#include "Scene.h"

class Rook : public Scene
{
public:
	Rook() = default;

	void init() override;
	Color backgroundColor() const override;
	Transform cameraTransform() override;
	std::shared_ptr<Scene> clone() override;
	Body body();
	const std::string name() const;

	~Rook();
};

