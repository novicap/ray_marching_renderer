#include "Rings.h"

void Rings::init()
{
}

Color Rings::backgroundColor() const
{
	return Color();
}

Transform Rings::cameraTransform()
{
	return Transform();
}

std::shared_ptr<Scene> Rings::clone()
{
	return std::shared_ptr<Scene>();
}

const std::string Rings::name() const
{
	return "Rings";
}


Rings::~Rings()
{
}
