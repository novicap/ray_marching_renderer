#pragma once

#include "CGAL_setup.h"
#include "Vector3.h"
#include "BoundingSphere.h"
#include "VectorPointCGALAdapter.h"

class BoundingSphereCGALAdapter
{
public:
	static BoundingSphere_3 toCGALSphere(const BoundingSphere& s) {
		return BoundingSphere_3(
			VectorPointCGALAdapter::toCGALPoint3(s.center()),
			FT(s.radius())
		);
	};

	static BoundingSphere toBoundingSphere(Min_sphere& ms) {
		double radius = ms.radius();
		Min_sphere::Cartesian_const_iterator ccib = ms.center_cartesian_begin();

		double x = ms.center_cartesian_begin()[0];
		double y = ms.center_cartesian_begin()[1];
		double z = ms.center_cartesian_begin()[2];
		Vector3 center = Vector3::vector(x, y, z);
		BoundingSphere b(radius, center);
		return b;
	}
};
