#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <fstream>
#include <vector>
#include <SDL.h>
#include <ctime>
#include "Config.h"

#include "RandomSpread.h"
#include "GroupedTogether.h"
#include "ChessBoard.h"
#include "ManyObjects.h"
#include "Inline.h"
#include "Heatmap.h"

#include "BufferRendering.h"
#include "BufferToneMapping.h"

#include "SphereTracer.h"
#include "SphereTracerBase.h"
#include "SphereTracerBvhAcceleratedAABBBU.h"
#include "SphereTracerBvhAcceleratedAABBTD.h"
#include "SphereTracerBvhAcceleratedBoundingSphereTD.h"
#include "SphereTracerBvhAcceleratedBoundingSphereBU.h"
#include "SphereTracerBvhAcceleratedOBBBU.h"
#include "SphereTracerBvhAcceleratedOBBTD.h"
//without tone mapper
int main(int argc, char *argv[])
{
	std::cout << "Program start. Initializing scene and Bounding Volumes." << std::endl;
	Vector dimensions = Vector::vector(WIDTH, HEIGHT);
	BufferRendering br;
	BufferToneMapping btm;
	Camera perspectiveCamera;

	//one scene should be active
	std::shared_ptr<ChessBoard> scene = std::make_shared<ChessBoard>();
	//std::shared_ptr<Scene> scene = std::make_shared<RandomSpread>();
	//std::shared_ptr<Scene> scene = std::make_shared<GroupedTogether>();
	//std::shared_ptr<Scene> scene = std::make_shared<Inline>();
	//std::shared_ptr<Scene> scene = std::make_shared<ManyObjects>();
	//std::shared_ptr<Heatmap> scene = std::make_shared<Heatmap>();
	scene->init();

	//one renderer should be active
	//std::shared_ptr<SphereTracer> rm = std::make_shared<single_object::SphereTracerBase>(scene, perspectiveCamera, dimensions);
	//AABB
	//std::shared_ptr<SphereTracer> rm = std::make_shared<single_object::SphereTracerBvhAcceleratedAABBBU>(scene, perspectiveCamera, dimensions);
	std::shared_ptr<SphereTracer> rm = std::make_shared<single_object::SphereTracerBvhAcceleratedAABBTD>(scene, perspectiveCamera, dimensions);
	//Sphere
	//std::shared_ptr<SphereTracer> rm = std::make_shared<single_object::SphereTracerBvhAcceleratedBoundingSphereBU>(scene, perspectiveCamera, dimensions);
	//std::shared_ptr<SphereTracer> rm = std::make_shared<single_object::SphereTracerBvhAcceleratedBoundingSphereTD>(scene, perspectiveCamera, dimensions);
	//OBB
	//std::shared_ptr<SphereTracer> rm = std::make_shared<single_object::SphereTracerBvhAcceleratedOBBBU>(scene, perspectiveCamera, dimensions);
	//std::shared_ptr<SphereTracer> rm = std::make_shared<single_object::SphereTracerBvhAcceleratedOBBTD>(scene, perspectiveCamera, dimensions);

	//SDL init
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
		return 1;
	}

	std::cout << "Rendering initiated." << std::endl;

	////rendering part
	bool quit = false;
	bool outputFinished = false;
	SDL_Window* window = SDL_CreateWindow(rm->name().c_str(), SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_RESIZABLE);
	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);
	SDL_Texture* buffer = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, WIDTH, HEIGHT);
	SDL_Event event;
	int pitch = 8 * WIDTH;

	//iterating with time tracing
	int maxIterations = 160;
	int skip = 0;
	int iterations = 0;
	int biggerThanAverage = 0;
	double min = DBL_MAX;
	double max = DBL_MIN;
	double time = 0.0;
	double iterTime = 0.0;
	double average = 0.0;
	std::vector<double> iterTimes;
	//render loop
	while (!quit) {
		SDL_PollEvent(&event);
		if (event.type == SDL_QUIT) {
			quit = true;
		}
		if (iterations < maxIterations)
		{
			SDL_UnlockTexture(buffer);
			SDL_RenderCopy(renderer, buffer, NULL, NULL);
			SDL_RenderPresent(renderer);
			SDL_RenderClear(renderer);
			SDL_LockTexture(buffer, NULL, (void**)&btm.imageData(), &pitch);
			rm->renderIteration(br.pixelColors(), btm.imageData(), iterTime);
			if (iterations >= skip)
			{
				iterTimes.push_back(iterTime);
				time += iterTime;
				if (iterTime > max)
				{
					max = iterTime;
				}

				if (iterTime < min)
				{
					min = iterTime;
				}
			}
			iterations++;
		}
		if (iterations == maxIterations && !outputFinished)
		{
			std::sort(iterTimes.begin(), iterTimes.end());
			average = time / (iterations - skip);
			for (auto& time : iterTimes)
			{
				if (time > average)
				{
					biggerThanAverage++;
				}
			}

			//Console output
			std::cout << rm->name() << std::endl;
			std::cout << scene->name() << std::endl;
			std::cout << "Iterations: " << (iterations - skip) << std::endl;
			std::cout << "Average: " << average << std::endl;
			std::cout << "Min: " << min << std::endl;
			std::cout << "Max: " << max << std::endl;
			std::cout << "Bigger than average: " << biggerThanAverage << std::endl;
			std::cout << "Threads: " << OMP_THREADS << std::endl;
			if (OPTIMISE_FOR_CONVEXITY)
			{
				std::cout << "Convexity optimisation ACTIVE" << std::endl;
			}
			else
			{
				std::cout << "Convexity optimisation NOT ACTIVE" << std::endl;
			}
			if (OPTIMISE_FOR_RAY_LENGTH)
			{
				std::cout << "Ray length optimisation ACTIVE" << std::endl;
			}
			else
			{
				std::cout << "Ray length optimisation NOT ACTIVE" << std::endl;
			}
			if (OUTPUT_HEATMAP)
			{
				std::cout << "Output: Scene heatmap" << std::endl;
			}
			else
			{
				std::cout << "Output: Scene" << std::endl;
			}
			std::cout << "10 worst times: \n";
			for (int i = maxIterations - skip - 10; i < maxIterations - skip; i++) {
				std::cout << "\t" << iterTimes[i] << std::endl;
			}
			//File output
			std::ofstream results("__results.txt", std::fstream::app);
			time_t now = std::time(0);
			char* dt = ctime(&now);
			results << "Date and Time: " << dt << std::endl;
			results << "Renderer: " << rm->name() << std::endl;
			if (OUTPUT_HEATMAP)
			{
				results << "Output: Scene heatmap" << std::endl;
			}
			else
			{
				results << "Output: Scene" << std::endl;
			}
			results << "Scene: " << scene->name() << std::endl;
			results << "Threads: " << OMP_THREADS << std::endl;
			if (OPTIMISE_FOR_CONVEXITY)
			{
				results << "Convexity optimisation: ACTIVE" << std::endl;
			}
			else
			{
				results << "Convexity optimisation: NOT ACTIVE" << std::endl;
			}
			if (OPTIMISE_FOR_RAY_LENGTH)
			{
				results << "Ray length optimisation: ACTIVE" << std::endl;
			}
			else
			{
				results << "Ray length optimisation: NOT ACTIVE" << std::endl;
			}
			results << "Iterations: " << (iterations - skip) << std::endl;
			results << "Average: " << average << std::endl;
			results << "Min: " << min << std::endl;
			results << "Max: " << max << std::endl;
			results << "Bigger than average: " << biggerThanAverage << std::endl;
			results << "10 worst times: \n";
			for (int i = maxIterations - skip - 10; i < maxIterations - skip; i++) {
				results << "\t" << iterTimes[i] << std::endl;
			}
			results << "###################################################################" << std::endl << std::endl;


			results.close();
			outputFinished = true;
		}
	}
	////end of render loop
	SDL_DestroyTexture(buffer);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	////end of rendering
	return 0;
}