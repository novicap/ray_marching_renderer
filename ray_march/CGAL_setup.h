#pragma once
#define CGAL_EIGEN3_ENABLED

#include <CGAL/optimal_bounding_box.h>
#include <CGAL/bounding_box.h>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Exact_rational.h>
#include <CGAL/Min_sphere_of_spheres_d.h>
#include <CGAL/double.h>
#include <CGAL/MP_Float.h>
#include <CGAL/centroid.h>

typedef double FT;
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Min_sphere_of_spheres_d_traits_3<K, FT> SphereTraits;
typedef CGAL::Min_sphere_of_spheres_d<SphereTraits> Min_sphere;
typedef SphereTraits::Sphere BoundingSphere_3;

typedef K::Point_3 Point_3;
typedef K::Vector_3 Vector_3;
typedef K::Aff_transformation_3 Transformation_3;
typedef K::Iso_cuboid_3 AABB_3;
