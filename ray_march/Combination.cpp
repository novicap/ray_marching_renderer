#include "Combination.h"
#include "Config.h"
#include <iostream>

Combination::~Combination()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Combination destroyed." << std::endl;
	}
}

void Combination::stream(std::ostream &strm) const
{
	strm << "Combinaton(\n\tFirst: " << *m_firstOperand << ",\n\tSecond: " << *m_secondOperand << ")";
}

std::shared_ptr<Shape>& Combination::firstOperand()
{
	return m_firstOperand;
}

std::shared_ptr<Shape>& Combination::secondOperand()
{
	return m_secondOperand;
}

//operatins for both operands, works for both simple and complex operands
//if operand is simple appropriate method in operand is called
//if operand is complex some of the methods below is called, and so on
//until simple operand is found
Vector3 Combination::estNormal_(const Vector3 &v)
{
	double epsilon = EPSILON;
	m_firstOperand->setAndTransformSdfVector(v);
	m_secondOperand->setAndTransformSdfVector(v);
	double d = sdf();

	Vector3 px = v - Vector3::vector(epsilon, 0.0, 0.0);
	m_firstOperand->setAndTransformSdfVector(px);
	m_secondOperand->setAndTransformSdfVector(px);
	double sdfPx = sdf();

	Vector3 py = v - Vector3::vector(0.0, epsilon, 0.0);
	m_firstOperand->setAndTransformSdfVector(py);
	m_secondOperand->setAndTransformSdfVector(py);
	double sdfPy = sdf();

	Vector3 pz = v - Vector3::vector(0.0, 0.0, epsilon);
	m_firstOperand->setAndTransformSdfVector(pz);
	m_secondOperand->setAndTransformSdfVector(pz);
	double sdfPz = sdf();

	return Vector3::vector(d - sdfPx, d - sdfPy, d - sdfPz).normalized();
}

bool Combination::isConvexOptimizable()
{
	return m_isConvexOptimizable;
}

void Combination::initRay(const Ray &r)
{
	m_firstOperand->initRay(r);
	m_secondOperand->initRay(r);
}

void Combination::applyDistance(double dO)
{
	m_firstOperand->applyDistance(dO);
	m_secondOperand->applyDistance(dO);
}

void Combination::setSdfVector(const Vector3 &v)
{
	m_firstOperand->setSdfVector(v);
	m_secondOperand->setSdfVector(v);
}

void Combination::transform(const Transform& t, bool append)
{
	Transform inverse = Transform(t).inverse();
	if (append) {
		m_transformations.push_back(t);
		m_inverseTransformations.push_back(inverse);
	}
	else {
		m_transformations.insert(m_transformations.begin(), t);
		m_inverseTransformations.insert(m_inverseTransformations.begin(), inverse);
	}

	m_aabb.transform(t, append);
	m_obb.transform(t, append);
	m_boundingSphere.transform(t, append);

	m_firstOperand->transform(t, append);
	m_secondOperand->transform(t, append);
	m_firstOperand->applyBvhTransformations();
	m_secondOperand->applyBvhTransformations();
}

void Combination::setAndTransformSdfVector(const Vector3 &v)
{
	m_firstOperand->setAndTransformSdfVector(v);
	m_secondOperand->setAndTransformSdfVector(v);
}

void Combination::makeAABB()
{
	AABB first = m_firstOperand->aabb();
	AABB second = m_secondOperand->aabb();

	m_aabb = first.merge(second);
}

void Combination::makeOBB()
{
	OBB first = m_firstOperand->obb();
	OBB second = m_secondOperand->obb();

	m_obb = first.merge(second);
}

void Combination::makeBoundingSphere()
{
	BoundingSphere first = m_firstOperand->boundingSphere();
	BoundingSphere second = m_secondOperand->boundingSphere();

	m_boundingSphere = first.merge(second);
}

void Combination::applyBvhTransformations()
{
	m_aabb.applyTransformations();
	m_obb.applyTransformations();
	m_obb.calculateCentroid();
	m_boundingSphere.applyTransformations();
}