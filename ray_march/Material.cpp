#include "Material.h"
#include "Config.h"
#include <iostream>

Material::Material(const Color &diffuse)
{
	Color black = Color::black();
	Color white = Color::white();
	m_diffuse = diffuse;
	m_specular = Color::gray(1);
	m_shininess = 256;
	m_reflective = black;
	m_refractive = black;
	m_refractionIndex = 1;
}

Material Material::glass(double refractiveIndex, double reflectance)
{
	Color black = Color::black();
	return Material(black, black, 0, Color::gray(reflectance), Color::gray(1 - reflectance), refractiveIndex);
}

Material Material::mirror()
{
	Color black = Color::black();
	Color white = Color::white();
	return Material(black, black, 0.0, white, black, 1.0);
}

const Color & Material::diffuse() const
{
	return m_diffuse;
}

const Color & Material::specular() const
{
	return m_specular;
}

const Color & Material::reflective() const
{
	return m_reflective;
}

const Color & Material::refractive() const
{
	return m_refractive;
}

const double Material::shininess() const
{
	return m_shininess;
}

const double Material::refractionIndex() const
{
	return m_refractionIndex;
}

Material::~Material()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Material destroyed." << std::endl;
	}
}