#include "King.h"
#include "Cylinder.h"
#include "SmoothUnion.h"
#include "Torus.h"
#include "Box.h"
#include "Sphere.h"

void King::init() {
	
	Body b = body();
	Transform whiteKingPosition = Transform::translation(Vector3::vector(6.0, 0, 0));
	b.transform(whiteKingPosition, false);
	b.shape()->applyBvhTransformations();

	Body b1 = body();

	std::vector<Light> lights{
		Light(Vector3::vector(0.0, 1.5, 1.0), Color::gray(1.0)),
		Light(Vector3::vector(1.0, 1.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(-1.0, 1.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 1.5, 8.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, 1.0), Color::gray(1.0)),
		Light(Vector3::vector(1.0, 3.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(-1.0, 3.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, 8.0), Color::gray(1.0)),
		Light(Vector3::vector(2.0, -3.5, 1.0), Color::gray(1.0)),
		Light(Vector3::vector(5.0, -3.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(-5.0, -3.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(-2.0, -3.5, 8.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 1.5, -1.0), Color::gray(1.0)),
		Light(Vector3::vector(1.0, 1.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(-1.0, 1.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 1.5, -8.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, -1.0), Color::gray(1.0)),
		Light(Vector3::vector(1.0, 3.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(-1.0, 3.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, -8.0), Color::gray(1.0)),
		Light(Vector3::vector(2.0, -3.5, -1.0), Color::gray(1.0)),
		Light(Vector3::vector(5.0, -3.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(-5.0, -3.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(-2.0, -3.5, -8.0), Color::gray(1.0))
	};


	Scene::addLights(lights);
	std::vector<Body> bodies{
		b,b1
	};

	Scene::addBodies(bodies);
}

Color King::backgroundColor() const
{
	return Color::gray(0.5);
}

Transform King::cameraTransform()
{
	return Transform::rotationAroundX(25).andThen(Transform::translation(Vector3::vector(0.0, 2.0, -12.0)));
}

std::shared_ptr<Scene> King::clone()
{
	std::shared_ptr<King> sb = std::make_shared<King>();

	for (auto& body : m_bodies) {
		sb->addBody(body.clone());
	}
	for (auto& light : m_lights) {
		sb->addLight(Light(light));
	}

	return sb;
}

const std::string King::name() const
{
	return "King";
}

Body King::body()
{
	std::shared_ptr<Cylinder> headBase = std::make_shared<Cylinder>(0.4, 0.5);
	std::shared_ptr<Torus> ring1 = std::make_shared<Torus>(0.02, 0.55);
	Transform tt2 = Transform::translation(Vector3::vector(0, 0.4, 0));
	ring1->transform(tt2);
	ring1->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s1 = std::make_shared<SmoothUnion>(headBase, ring1, 0.3);
	std::shared_ptr<Torus> ring2 = std::make_shared<Torus>(0.07, 0.2);
	Transform tt3 = Transform::translation(Vector3::vector(0, 0.5, 0));
	ring2->transform(tt3);
	ring2->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s2 = std::make_shared<SmoothUnion>(s1, ring2, 0.3);
	std::shared_ptr<Torus> ring3 = std::make_shared<Torus>(0.03, 0.4);
	Transform tt4 = Transform::translation(Vector3::vector(0, -0.3, 0));
	ring3->transform(tt4);
	ring3->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s3 = std::make_shared<SmoothUnion>(s2, ring3, 0.3);
	std::shared_ptr<Box> crossBar1 = std::make_shared<Box>(Vector3::vector(0.15, 0.4, 0.15));
	Transform tt5 = Transform::translation(Vector3::vector(0, 0.9, 0));
	crossBar1->transform(tt5);
	crossBar1->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s4 = std::make_shared<SmoothUnion>(s3, crossBar1, 0.2);
	std::shared_ptr<Box> crossBar2 = std::make_shared<Box>(Vector3::vector(0.4, 0.15, 0.15));
	Transform tt6 = Transform::translation(Vector3::vector(0, 0.9, 0));
	crossBar2->transform(tt6);
	crossBar2->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s5 = std::make_shared<SmoothUnion>(s4, crossBar2, 0.1);


	std::shared_ptr<Cylinder> bodyBase = std::make_shared<Cylinder>(1.5, 0.40);
	Transform tt7 = Transform::translation(Vector3::vector(0, -2, 0));
	bodyBase->transform(tt7);
	bodyBase->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s6 = std::make_shared<SmoothUnion>(bodyBase, s5, 0.3);
	std::shared_ptr<Torus> ring4 = std::make_shared<Torus>(0.15, 0.45);
	Transform tt8 = Transform::translation(Vector3::vector(0, -1.3, 0));
	ring4->transform(tt8);
	ring4->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s7 = std::make_shared<SmoothUnion>(ring4, s6, 0.2);
	std::shared_ptr<Torus> ring5 = std::make_shared<Torus>(0.15, 0.35);
	Transform tt9 = Transform::translation(Vector3::vector(0, -1, 0));
	ring5->transform(tt9);
	ring5->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s8 = std::make_shared<SmoothUnion>(ring5, s7, 0.2);


	std::shared_ptr<Cylinder> baseBase = std::make_shared<Cylinder>(0.4, 0.75);
	Transform tt10 = Transform::translation(Vector3::vector(0, -3, 0));
	baseBase->transform(tt10);
	baseBase->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s9 = std::make_shared<SmoothUnion>(baseBase, s8, 0.3);
	std::shared_ptr<Torus> ring6 = std::make_shared<Torus>(0.1, 0.55);
	Transform t6 = Transform::translation(Vector3::vector(0, -2.45, 0));
	ring6->transform(t6);
	ring6->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s10 = std::make_shared<SmoothUnion>(ring6, s9, 0.3);

	std::shared_ptr<Torus> ring7 = std::make_shared<Torus>(0.15, 0.7);
	Transform t7 = Transform::translation(Vector3::vector(0, -3.3, 0));
	ring7->transform(t7);
	ring7->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s11 = std::make_shared<SmoothUnion>(ring7, s10, 0.03);
	std::shared_ptr<Torus> ring8 = std::make_shared<Torus>(0.15, 0.35);
	Transform t8 = Transform::translation(Vector3::vector(0, -2.0, 0));
	ring8->transform(t8);
	ring8->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s12 = std::make_shared<SmoothUnion>(ring8, s11, 0.3);

	std::shared_ptr<Sphere> crossTip = std::make_shared<Sphere>(0.12);
	Transform tt11 = Transform::translation(Vector3::vector(0, 1.38, 0));
	crossTip->transform(tt11);
	crossTip->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s13 = std::make_shared<SmoothUnion>(crossTip, s12, 0.01);

	return Body(s13, Material(Color::gray(0.0)));
}


King::~King()
{
}
