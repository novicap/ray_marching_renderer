#include "Pawn.h"
#include "Sphere.h"
#include "Cylinder.h"
#include "Torus.h"
#include "SmoothUnion.h"
#include "Union.h"
#include "Cone.h"
#include "Box.h"


void Pawn::init()
{
	Body b = Body(std::make_shared<Sphere>(0.5), Material(Color::gray(0.9)));
	Transform whitePawn2Position =  Transform::rotationAroundY(45);
	b.transform(whitePawn2Position, false);
	b.shape()->applyBvhTransformations();
	//Body b1 = body();
	

	std::vector<Light> lights{
		Light(Vector3::vector(0.0, 1.5, 1.0), Color::gray(1.0)),
		Light(Vector3::vector(1.0, 1.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(-1.0, 1.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 1.5, 8.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, 1.0), Color::gray(1.0)),
		Light(Vector3::vector(1.0, 3.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(-1.0, 3.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, 8.0), Color::gray(1.0)),
		Light(Vector3::vector(2.0, -3.5, 1.0), Color::gray(1.0)),
		Light(Vector3::vector(5.0, -3.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(-5.0, -3.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(-2.0, -3.5, 8.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 1.5, -1.0), Color::gray(1.0)),
		Light(Vector3::vector(1.0, 1.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(-1.0, 1.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 1.5, -8.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, -1.0), Color::gray(1.0)),
		Light(Vector3::vector(1.0, 3.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(-1.0, 3.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, -8.0), Color::gray(1.0)),
		Light(Vector3::vector(2.0, -3.5, -1.0), Color::gray(1.0)),
		Light(Vector3::vector(5.0, -3.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(-5.0, -3.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(-2.0, -3.5, -8.0), Color::gray(1.0))
	};
	std::vector<Body> bodies{
		b
	};

	Scene::addLights(lights);
	Scene::addBodies(bodies);
}

Color Pawn::backgroundColor() const
{
	return Color::gray(0.5);
}

Transform Pawn::cameraTransform()
{
	return Transform::translation(Vector3::vector(0.0, -1.0, -5.0));
}

std::shared_ptr<Scene> Pawn::clone()
{
	std::shared_ptr<Pawn> sb = std::make_shared<Pawn>();

	for (auto& body : m_bodies) {
		sb->addBody(body.clone());
	}
	for (auto& light : m_lights) {
		sb->addLight(Light(light));
	}

	return sb;
}

const std::string Pawn::name() const
{
	return "Pawn";
}

Body Pawn::body()
{
	std::shared_ptr<Sphere> head = std::make_shared<Sphere>(0.4);
	head->applyBvhTransformations();

	std::shared_ptr<Torus> headRing1 = std::make_shared<Torus>(0.1, 0.3);
	Transform t1 = Transform::translation(Vector3::vector(0, -0.4, 0));
	headRing1->transform(t1);
	headRing1->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s1 = std::make_shared<SmoothUnion>(headRing1, head, 0.1);

	std::shared_ptr<Cone> bodyBase = std::make_shared<Cone>(22, 1.7);
	bodyBase->applyBvhTransformations();

	std::shared_ptr<SmoothUnion> s2 = std::make_shared<SmoothUnion>(bodyBase, s1, 0.3);

	std::shared_ptr<Cylinder> baseBase = std::make_shared<Cylinder>(0.25, 0.45);
	Transform tt3 = Transform::translation(Vector3::vector(0, -1.7, 0));
	baseBase->transform(tt3);
	baseBase->applyBvhTransformations();

	std::shared_ptr<SmoothUnion> s3 = std::make_shared<SmoothUnion>(baseBase, s2, 0.2);

	std::shared_ptr<Torus> ring2 = std::make_shared<Torus>(0.05, 0.5);
	Transform t6 = Transform::translation(Vector3::vector(0, -1.35, 0));
	ring2->transform(t6);
	ring2->applyBvhTransformations();

	std::shared_ptr<SmoothUnion> s4 = std::make_shared<SmoothUnion>(ring2, s3, 0.3);

	std::shared_ptr<Torus> ring3 = std::make_shared<Torus>(0.1, 0.65);
	Transform t7 = Transform::translation(Vector3::vector(0, -1.8, 0));
	ring3->transform(t7);
	ring3->applyBvhTransformations();

	std::shared_ptr<SmoothUnion> s5 = std::make_shared<SmoothUnion>(ring3, s4, 0.03);

	return Body(s5, Material(Color::gray(0.9)));
}


Pawn::~Pawn()
{
}
