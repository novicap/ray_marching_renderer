#include "Subtraction.h"
#include "Config.h"
#include <iostream>

void Subtraction::stream(std::ostream &strm) const
{
	strm << "Subtraction(\n\tFirst: " << *m_firstOperand << ",\n\tSecond: " << *m_secondOperand << ")";
}

double Subtraction::sdf()
{
	double firstSdf = m_firstOperand->sdf();
	double secondSdf = m_secondOperand->sdf();

	return std::max(-firstSdf, secondSdf);
}

Subtraction::~Subtraction()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Subtraction destroyed." << std::endl;
	}
}