#include "AABBNode.h"
#include <iostream>

void AABBNode::addBody(const Body& b)
{
	m_bodies.push_back(b);
}

void AABBNode::addBodies(const std::vector<Body>& b)
{
	for (auto &body : b) {
		m_bodies.push_back(body);
	}
}

void AABBNode::cloneBodies(const std::vector<Body>& b)
{
	for (auto &body : b) {
		addBody(Body(body.shape()->clone(), Material(body.material())));
	}
}

const std::vector<Body>& AABBNode::bodies() const
{
	return m_bodies;
}

AABB& AABBNode::aabb()
{
	return m_aabb;
}

const bool AABBNode::isLeaf() const
{
	return m_left == nullptr;
}

const double AABBNode::volume() const
{
	return m_aabb.volume();
}

const std::shared_ptr<AABBNode>& AABBNode::left() const
{
	return m_left;
}

const std::shared_ptr<AABBNode>& AABBNode::right() const
{
	return m_right;
}

const std::shared_ptr<AABBNode>& AABBNode::parent() const
{
	return m_parent;
}

AABB AABBNode::merge(const std::shared_ptr<AABBNode>& aabbNode)
{
	if (m_bodies.size() == 0 && aabbNode->bodies().size() == 0) {
		throw("No bodies to calculate AABB.");
	}

	std::vector<Point_3> points;
	std::array<Vector3, 8> vertices;
	for (auto& body : m_bodies) {
		for (auto& point : body.aabb().vertices()) {
			points.push_back(VectorPointCGALAdapter::toCGALPoint3(point));
		}
	}
	for (auto& body : aabbNode->bodies()) {
		for (auto& point : body.aabb().vertices()) {
			points.push_back(VectorPointCGALAdapter::toCGALPoint3(point));
		}
	}
	AABB_3 aabb = CGAL::bounding_box(std::begin(points), std::end(points));
	for (int i = 0; i < 8; i++) {
		vertices[i] = VectorPointCGALAdapter::toVector3(aabb[i]);
	}

	return AABB(vertices);
}

void AABBNode::addLeft(const std::shared_ptr<AABBNode>& node, const std::shared_ptr<AABBNode>& parent)
{
	m_left = node;
	m_parent = parent;
}

void AABBNode::addRight(const std::shared_ptr<AABBNode>& node, const std::shared_ptr<AABBNode>& parent)
{
	m_right = node;
	m_parent = parent;
}

AABB AABBNode::calculateAABB()
{
	if (m_bodies.size() == 0) {
		throw("No bodies to calculate AABB.");
	}

	if (m_bodies.size() == 1) {
		return AABB(m_bodies[0].aabb());
	}

	std::vector<Point_3> points;
	std::array<Vector3, 8> vertices;
	for (auto& body : m_bodies) {
		for (auto& point : body.aabb().vertices()) {
			points.push_back(VectorPointCGALAdapter::toCGALPoint3(point));
		}
	}
	AABB_3 aabb = CGAL::bounding_box(std::begin(points), std::end(points));
	for (int i = 0; i < 8; i++) {
		vertices[i] = VectorPointCGALAdapter::toVector3(aabb[i]);
	}

	return AABB(vertices);
}

AABBNode::~AABBNode()
{
}