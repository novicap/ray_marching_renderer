#include "Rook.h"
#include "Cylinder.h"
#include "Box.h"
#include "Subtraction.h"
#include "Union.h"
#include "SmoothUnion.h"
#include "Torus.h"

void Rook::init() {

	Body b = body();
	Transform whiteKingPosition = Transform::translation(Vector3::vector(6.0, 0, 0));
	b.transform(whiteKingPosition, false);
	b.shape()->applyBvhTransformations();
	Body b1 = body();

	std::vector<Body> bodies{
		b,b1
	};

	std::vector<Light> lights{
		Light(Vector3::vector(0.0, 1.5, 1.0), Color::gray(1.0)),
		Light(Vector3::vector(1.0, 1.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(-1.0, 1.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 1.5, 8.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, 1.0), Color::gray(1.0)),
		Light(Vector3::vector(1.0, 3.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(-1.0, 3.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, 8.0), Color::gray(1.0)),
		Light(Vector3::vector(2.0, -3.5, 1.0), Color::gray(1.0)),
		Light(Vector3::vector(5.0, -3.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(-5.0, -3.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(-2.0, -3.5, 8.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 1.5, -1.0), Color::gray(1.0)),
		Light(Vector3::vector(1.0, 1.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(-1.0, 1.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 1.5, -8.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, -1.0), Color::gray(1.0)),
		Light(Vector3::vector(1.0, 3.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(-1.0, 3.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, -8.0), Color::gray(1.0)),
		Light(Vector3::vector(2.0, -3.5, -1.0), Color::gray(1.0)),
		Light(Vector3::vector(5.0, -3.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(-5.0, -3.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(-2.0, -3.5, -8.0), Color::gray(1.0))
	};

	Scene::addLights(lights);
	Scene::addBodies(bodies);
}

Color Rook::backgroundColor() const
{
	return Color::gray(0.5);
}

Transform Rook::cameraTransform()
{
	return Transform::rotationAroundX(25).andThen(Transform::translation(Vector3::vector(0.0, 1.0, -6.0)));
}

std::shared_ptr<Scene> Rook::clone()
{
	std::shared_ptr<Rook> sb = std::make_shared<Rook>();

	for (auto& body : m_bodies) {
		sb->addBody(body.clone());
	}
	for (auto& light : m_lights) {
		sb->addLight(Light(light));
	}

	return sb;
}

Body Rook::body()
{
	std::shared_ptr<Cylinder> headBase = std::make_shared<Cylinder>(0.3, 0.5);
	std::shared_ptr<Cylinder> headValey = std::make_shared<Cylinder>(0.4, 0.4);
	std::shared_ptr<Subtraction> s = std::make_shared<Subtraction>(headValey, headBase);
	Transform tt1 = Transform::translation(Vector3::vector(0, 0.21, 0));

	std::shared_ptr<Box> tooth1 = std::make_shared<Box>(Vector3::vector(0.1, 0.1, 0.6));
	Transform t1 = Transform::rotationAroundY(45);
	tooth1->transform(tt1);
	tooth1->transform(t1);
	tooth1->applyBvhTransformations();
	std::shared_ptr<Subtraction> s1 = std::make_shared<Subtraction>(tooth1, s);
	std::shared_ptr<Box> tooth2 = std::make_shared<Box>(Vector3::vector(0.1, 0.1, 0.6));
	Transform t2 = Transform::rotationAroundY(90);
	tooth2->transform(tt1);
	tooth2->transform(t2);
	tooth2->applyBvhTransformations();
	std::shared_ptr<Subtraction> s2 = std::make_shared<Subtraction>(tooth2, s1);
	std::shared_ptr<Box> tooth3 = std::make_shared<Box>(Vector3::vector(0.1, 0.1, 0.6));
	Transform t3 = Transform::rotationAroundY(135);
	tooth3->transform(tt1);
	tooth3->transform(t3);
	tooth3->applyBvhTransformations();
	std::shared_ptr<Subtraction> s3 = std::make_shared<Subtraction>(tooth3, s2);
	std::shared_ptr<Box> tooth4 = std::make_shared<Box>(Vector3::vector(0.1, 0.1, 0.6));
	Transform t4 = Transform::rotationAroundY(180);
	tooth4->transform(tt1);
	tooth4->transform(t4);
	tooth4->applyBvhTransformations();
	std::shared_ptr<Subtraction> s4 = std::make_shared<Subtraction>(tooth4, s3);
	std::shared_ptr<Torus> ring1 = std::make_shared<Torus>(0.05, 0.4);
	Transform t5 = Transform::translation(Vector3::vector(0, -0.45, 0));
	ring1->transform(t5);
	ring1->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s5 = std::make_shared<SmoothUnion>(ring1, s4, 0.3);


	std::shared_ptr<Cylinder> bodyBase = std::make_shared<Cylinder>(1, 0.42);
	Transform tt2 = Transform::translation(Vector3::vector(0, -1, 0));
	bodyBase->transform(tt2);
	bodyBase->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s6 = std::make_shared<SmoothUnion>(bodyBase, s5, 0.3);

	std::shared_ptr<Cylinder> baseBase = std::make_shared<Cylinder>(0.35, 0.65);
	Transform tt3 = Transform::translation(Vector3::vector(0, -2.2, 0));
	baseBase->transform(tt3);
	baseBase->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s7 = std::make_shared<SmoothUnion>(baseBase, s6, 0.3);
	std::shared_ptr<Torus> ring2 = std::make_shared<Torus>(0.05, 0.5);
	Transform t6 = Transform::translation(Vector3::vector(0, -1.65, 0));
	ring2->transform(t6);
	ring2->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s8 = std::make_shared<SmoothUnion>(ring2, s7, 0.3);

	std::shared_ptr<Torus> ring3 = std::make_shared<Torus>(0.1, 0.65);
	Transform t7 = Transform::translation(Vector3::vector(0, -2.5, 0));
	ring3->transform(t7);
	ring3->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> s9 = std::make_shared<SmoothUnion>(ring3, s8, 0.03);
	
	return Body(s9, Material(Color::gray(0.0)));
}

const std::string Rook::name() const
{
	return "Rook";
}


Rook::~Rook()
{
}
