#include "Vector3.h"
#include "Math.h"
#include "Config.h"
#include <math.h>
#include <algorithm>
#include <iostream>

Vector3 Vector3::zero()
{
	return Vector3(0.0, 0.0, 0.0);
}

Vector3 Vector3::ex()
{
	return Vector3(1, 0, 0);
}

Vector3 Vector3::ey()
{
	return Vector3(0, 1, 0);
}

Vector3 Vector3::ez()
{
	return Vector3(0, 0, 1);
}

Vector3 Vector3::exy()
{
	return Vector3(1, 1, 0);
}

Vector3 Vector3::exz()
{
	return Vector3(1, 0, 1);
}

Vector3 Vector3::eyz()
{
	return Vector3(0, 1, 1);
}

Vector3 Vector3::exyz()
{
	return Vector3(1, 1, 1);
}

Vector3 Vector3::vector(double x, double y, double z)
{
	return Vector3(x, y, z);
}

Vector3 Vector3::vec2z(const Vector &v, double z)
{
	return Vector3(v.x(), v.y(), z);
}

Vector3 Vector3::vec2y(const Vector &v, double y)
{
	return Vector3(v.x(), y, v.y());
}

double Vector3::x() const
{
	return m_x;
}

double Vector3::y() const
{
	return m_y;
}

double Vector3::z() const
{
	return m_z;
}

void Vector3::setX(const double x)
{
	m_x = x;
}

void Vector3::setY(const double y)
{
	m_y = y;
}

void Vector3::setZ(const double z)
{
	m_z = z;
}

Vector3 &Vector3::operator+=(const Vector3 &v)
{
	m_x += v.m_x;
	m_y += v.m_y;
	m_z += v.m_z;
	return *this;
}

Vector3 Vector3::operator+(const Vector3 &v) const
{
	return Vector3(m_x + v.m_x, m_y + v.m_y, m_z + v.m_z);
}

Vector3 &Vector3::operator-=(const Vector3 &v)
{
	m_x -= v.m_x;
	m_y -= v.m_y;
	m_z -= v.m_z;
	return *this;
}

Vector3 Vector3::operator-(const Vector3 &v) const
{
	return Vector3(m_x - v.m_x, m_y - v.m_y, m_z - v.m_z);
}

Vector3 &Vector3::operator*=(const Vector3 &v)
{
	m_x *= v.m_x;
	m_y *= v.m_y;
	m_z *= v.m_z;
	return *this;
}

Vector3 Vector3::operator*(const Vector3 &v) const
{
	return Vector3(m_x * v.m_x, m_y * v.m_y, m_z * v.m_z);
}

Vector3 &Vector3::operator/=(const Vector3 &v)
{
	m_x /= v.m_x;
	m_y /= v.m_y;
	m_z /= v.m_z;
	return *this;
}

Vector3 Vector3::operator/(const Vector3 &p_v) const
{
	return Vector3(m_x / p_v.m_x, m_y / p_v.m_y, m_z / p_v.m_z);
}

Vector3 &Vector3::operator*=(double scalar)
{
	m_x *= scalar;
	m_y *= scalar;
	m_z *= scalar;
	return *this;
}

Vector3 Vector3::operator*(double scalar) const
{
	return Vector3(m_x * scalar, m_y * scalar, m_z * scalar);
}

Vector3 &Vector3::operator/=(double scalar)
{
	m_x /= scalar;
	m_y /= scalar;
	m_z /= scalar;
	return *this;
}

Vector3 Vector3::operator/(double scalar) const
{
	return Vector3(m_x / scalar, m_y / scalar, m_z / scalar);
}

Vector3 Vector3::operator-() const
{
	return Vector3(-m_x, -m_y, -m_z);
}

bool Vector3::operator==(const Vector3 &v) const
{
	return m_x == v.m_x && m_y == v.m_y && m_z == v.m_z;
}

bool Vector3::operator!=(const Vector3 &v) const
{
	return m_x != v.m_x || m_y != v.m_y || m_z != v.m_z;
}

Vector3 Vector3::cross(const Vector3 &v) const
{
	Vector3 ret(
		(m_y * v.m_z) - (m_z * v.m_y),
		(m_z * v.m_x) - (m_x * v.m_z),
		(m_x * v.m_y) - (m_y * v.m_x));

	return ret;
}

double Vector3::dot(const Vector3 &v) const
{
	return m_x * v.m_x + m_y * v.m_y + m_z * v.m_z;
}

Vector3 Vector3::abs() const
{
	return Vector3(std::abs(m_x), std::abs(m_y), std::abs(m_z));
}

Vector3 Vector3::sign() const
{
	return Vector3(SIGNUM(m_x), SIGNUM(m_y), SIGNUM(m_z));
}

Vector3 Vector3::floor() const
{
	return Vector3(std::floor(m_x), std::floor(m_y), std::floor(m_z));
}

Vector3 Vector3::ceil() const
{
	return Vector3(std::ceil(m_x), std::ceil(m_y), std::ceil(m_z));
}

Vector3 Vector3::round() const
{
	return Vector3(std::round(m_x), std::round(m_y), std::round(m_z));
}

double Vector3::length() const
{
	double x2 = m_x * m_x;
	double y2 = m_y * m_y;
	double z2 = m_z * m_z;

	return Math::fastSqrt(x2 + y2 + z2);
}

double Vector3::lengthSquared() const
{
	double x2 = m_x * m_x;
	double y2 = m_y * m_y;
	double z2 = m_z * m_z;

	return x2 + y2 + z2;
}

void Vector3::normalize()
{
	double lengthsq = lengthSquared();
	if (lengthsq == 0) {
		m_x = m_y = m_z = 0;
	}
	else {
		double length = Math::fastSqrt(lengthsq);

		m_x /= length;
		m_y /= length;
		m_z /= length;
	}
}

Vector3 Vector3::normalized() const {
	Vector3 v = *this;
	v.normalize();
	return v;
}

Vector3 Vector3::max(const double d) const
{
	return Vector3(std::max(m_x, d), std::max(m_y, d), std::max(m_z, d));
}
Vector3 Vector3::min(const double d) const
{
	return Vector3(std::min(m_x, d), std::min(m_y, d), std::min(m_z, d));
}

Vector3 Vector3::reflectN(Vector3 &normal_) const
{
	return normal_ * (2 * (*this).dot(normal_)) - *this;
}

Vector3 Vector3::reflect(Vector3 &normal) const
{
	return normal * (2 * (*this).dot(normal) / normal.lengthSquared()) - (*this);
}

Vector3::~Vector3()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Vector3 destroyed. " << *this << std::endl;
	}
}