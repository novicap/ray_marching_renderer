#pragma once
#include "Ray.h"
#include "Vector.h"

class Camera
{
public:
	Camera() : m_z{ 1.0 } {};
	Camera(double z) : m_z{ z } {}

	std::shared_ptr<Camera> clone();

	void sampleExitingRay(const Vector &pixel, Ray &ray);

	friend std::ostream& operator<<(std::ostream &strm, const Camera &c) {
		return strm << "Perspective(" << c.m_z << ")";
	}
	~Camera();
private:
	double m_z;
};
