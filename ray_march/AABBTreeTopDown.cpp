#include "AABBTreeTopDown.h"

AABBTreeTopDown::AABBTreeTopDown(const std::vector<Body>& b)
{
	if (b.size() == 0) {
		throw ("Can't build tree, no bodies.");
	}
	std::cout << "Building BVH Tree - AABBTreeTopDown." << std::endl;
	std::vector<Body> bodies = b;
	m_root = std::make_shared<AABBNode>(bodies);

	std::sort(bodies.begin(), bodies.end(), sortMethod(m_root));

	buildTopDown(m_root, bodies, 0, (int)bodies.size() / 2, true);
	buildTopDown(m_root, bodies, (int)bodies.size() / 2, (int)bodies.size(), false);
}

//ordered on axis
//* * * * * * * *
//	     v
//* * * *|* * * *
//	     v
//* *|* *|* *|* *
//	     v
//*|*|*|*|*|*|*|*

void AABBTreeTopDown::buildTopDown(const std::shared_ptr<AABBNode>& parent, const std::vector<Body>& b, int start, int end, bool isLeftChild)
{
	//break condition
	if (end - start <= 0) {
		return;
	}

	std::vector<Body> bodies;
	for (int i = start; i < end; i++) {
		bodies.push_back(b[i]);
	}
	std::shared_ptr<AABBNode> node = std::make_shared<AABBNode>(bodies);

	if (isLeftChild) {
		parent->addLeft(node, parent);
	}
	else {
		parent->addRight(node, parent);
	}

	//break condition
	if (end - start <= 1) {
		return;
	}

	std::sort(bodies.begin(), bodies.end(), sortMethod(node));
	buildTopDown(node, bodies, 0, (int)bodies.size() / 2, true);
	buildTopDown(node, bodies, (int)bodies.size() / 2, (int)bodies.size(), false);
}

std::function<bool(const Body&, const Body&)> AABBTreeTopDown::sortMethod(const std::shared_ptr<AABBNode>& node)
{
	if (node->aabb().width() > node->aabb().height()) {
		if (node->aabb().width() > node->aabb().depth()) {
			return Body::compareAABBX;
		}
		return Body::compareAABBZ;
	}
	else {
		if (node->aabb().height() > node->aabb().depth()) {
			return Body::compareAABBY;
		}
		return Body::compareAABBZ;
	}
}

AABBTreeTopDown::~AABBTreeTopDown()
{
}