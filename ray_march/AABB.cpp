#include "AABB.h"
#include "Config.h"
#include <iostream>
#include <algorithm>

AABB::~AABB()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "AABB destroyed" << std::endl;
	}
}

void AABB::initStVertices() {
	m_vertices[0] = Vector3::vector(m_vertex.x(), m_vertex.y(), m_vertex.z());
	m_vertices[1] = Vector3::vector(m_vertex.x(), m_vertex.y(), -m_vertex.z());
	m_vertices[2] = Vector3::vector(m_vertex.x(), -m_vertex.y(), m_vertex.z());
	m_vertices[3] = Vector3::vector(m_vertex.x(), -m_vertex.y(), -m_vertex.z());
	m_vertices[4] = Vector3::vector(-m_vertex.x(), m_vertex.y(), m_vertex.z());
	m_vertices[5] = Vector3::vector(-m_vertex.x(), m_vertex.y(), -m_vertex.z());
	m_vertices[6] = Vector3::vector(-m_vertex.x(), -m_vertex.y(), m_vertex.z());
	m_vertices[7] = Vector3::vector(-m_vertex.x(), -m_vertex.y(), -m_vertex.z());

	calculateAABB();
}

void AABB::calculateVolume()
{
	m_volume = width() * height() * depth();
}

void AABB::calculateVertices()
{
	calculateAABB();
	calculateVolume();
}

void AABB::calculateAABB()
{
	std::array<Point_3, 8> points;
	for (int i = 0; i < 8; i++) {
		points[i] = VectorPointCGALAdapter::toCGALPoint3(m_vertices[i]);
	}
	AABB_3 aabb = CGAL::bounding_box(std::begin(points), std::end(points));
	for (int i = 0; i < 8; i++) {
		m_vertices[i] = VectorPointCGALAdapter::toVector3(aabb[i]);
	}

	m_centroid = VectorPointCGALAdapter::toVector3(CGAL::centroid(std::begin(points), std::end(points)));

	m_vertex = m_vertices[7] - m_centroid;
	m_rayTransformation = Transform::translation(m_centroid).inverse();
}

void AABB::initRay(const Ray & r)
{
	m_ray = Ray(r);
	m_rayTransformation.applyTo(m_ray);
	m_sdfVector = m_ray.source();
}

double AABB::sdf()
{
	Vector3 q = m_sdfVector.abs() - m_vertex;
	return q.max(0.0).length() + std::min(std::max(q.x(), std::max(q.y(), q.z())), 0.0);
}

void AABB::applyDistance(double dO)
{
	m_sdfVector = m_ray.source() + (m_ray.direction() * dO);
}

const Vector3 & AABB::sdfVector() const
{
	return m_sdfVector;
}

const Vector3 & AABB::vertex() const
{
	return m_vertex;
}

const Vector3 & AABB::vertex(int v) const
{
	if (v < 0 || v > 7) {
		throw("Vertex index out of range.");
	}
	return m_vertices[v];
}

const Vector3 & AABB::centroid() const
{
	return m_centroid;
}

AABB AABB::merge(const AABB & aabb)
{
	std::vector<Point_3> points;
	std::array<Vector3, 8> vertices;
	for (auto& vertex : m_vertices) {
		points.push_back(VectorPointCGALAdapter::toCGALPoint3(vertex));
	}
	for (auto& vertex : aabb.m_vertices) {
		points.push_back(VectorPointCGALAdapter::toCGALPoint3(vertex));
	}
	//CGAL min aabb
	AABB_3 newAABB = CGAL::bounding_box(points.begin(), points.end());
	for (int i = 0; i < 8; i++) {
		vertices[i] = VectorPointCGALAdapter::toVector3(newAABB[i]);
	}
	return AABB(vertices);
}

void AABB::transform(const Transform & t, bool append)
{
	Transform inverse = Transform(t).inverse();
	if (append) {
		m_transformations.push_back(t);
		m_inverseTransformations.push_back(inverse);
	}
	else {
		m_transformations.insert(m_transformations.begin(), t);
		m_inverseTransformations.insert(m_inverseTransformations.begin(), inverse);
	}
}

void AABB::applyTransformations()
{
	std::reverse(m_transformations.begin(), m_transformations.end());
	for (Transform &t : m_transformations) {
		for (Vector3 &v : m_vertices) {
			t.applyTo(v);
		}
	}
	calculateVertices();
}

void AABB::applyInverseTransformations()
{
	for (Vector3 &v : m_vertices) {
		for (Transform &t : m_inverseTransformations) {
			t.applyTo(v);
		}
	}
	calculateVertices();
}

const std::vector<Transform>& AABB::transformations() const
{
	return m_transformations;
}

void AABB::printVertices() const
{
	for (auto &v : m_vertices) {
		std::cout << v << std::endl;
	}
}

void AABB::printAABB() const
{
	std::cout << "min " << -m_vertex.x() << "  " << -m_vertex.y() << "  " << -m_vertex.z() <<
		"\nmax " << m_vertex.x() << "  " << m_vertex.y() << "  " << m_vertex.z() << std::endl;
	std::cout << "SDF Veretex " << m_vertex << std::endl;
}

double AABB::width() const
{
	return 2 * m_vertex.x();
}

double AABB::height() const
{
	return 2 * m_vertex.y();
}

double AABB::depth() const
{
	return 2 * m_vertex.z();
}

double AABB::minX() const
{
	return -m_vertex.x();
}

double AABB::minY() const
{
	return -m_vertex.y();
}

double AABB::minZ() const
{
	return -m_vertex.z();
}

double AABB::maxX() const
{
	return m_vertex.x();
}

double AABB::maxY() const
{
	return m_vertex.y();
}

double AABB::maxZ() const
{
	return m_vertex.z();
}

double AABB::volume() const
{
	return m_volume;
}

const std::array<Vector3, 8>& AABB::vertices() const
{
	return m_vertices;
}