#include "RandomSpread.h"
#include "Torus.h"
#include "Cylinder.h"

void RandomSpread::init()
{
	for (int i = 0; i < 150; i++) {
		double height = (double)rand() / RAND_MAX + 1;
		double radius = (double)rand() / RAND_MAX + 0.1;
		double smallRadius = 0.2;
		double bigRadius = 1.2;
		int signRand = rand() % 2;
		int sign = (signRand == 0) ? 1 : -1;
		double x = (double)rand() / RAND_MAX * 15 * sign;
		signRand = rand() % 2;
		sign = (signRand == 0) ? 1 : -1;
		double y = (double)rand() / RAND_MAX * 10 * sign;
		double z = (double)rand() / RAND_MAX * 10;
		double angleX = (double)rand() / RAND_MAX * 90;
		double angleY = (double)rand() / RAND_MAX * 90;
		double angleZ = (double)rand() / RAND_MAX * 90;

		std::shared_ptr<Shape> shape = std::make_shared<Cylinder>(height, radius);
		//std::shared_ptr<Shape> shape = std::make_shared<Torus>(smallRadius, bigRadius);
		shape->transform(Transform::translation(Vector3::vector(x, y, z)));
		shape->transform(Transform::rotationAroundX(angleX));
		shape->transform(Transform::rotationAroundY(angleY));
		shape->transform(Transform::rotationAroundZ(angleZ));
		shape->applyBvhTransformations();
		Body b = Body(shape, Material(Color::rgb((double)rand() / RAND_MAX, (double)rand() / RAND_MAX, (double)rand() / RAND_MAX)));
		Scene::addBody(b);
	}

	std::vector<Light> lights{
		Light(Vector3::vector(0.0, 1.5, 1.0), Color::gray(1.0)),
		Light(Vector3::vector(1.0, 1.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(-1.0, 1.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 1.5, 8.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, 1.0), Color::gray(1.0)),
		Light(Vector3::vector(1.0, 3.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(-1.0, 3.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, 8.0), Color::gray(1.0)),
		Light(Vector3::vector(2.0, -3.5, 1.0), Color::gray(1.0)),
		Light(Vector3::vector(5.0, -3.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(-5.0, -3.5, 3.0), Color::gray(1.0)),
		Light(Vector3::vector(-2.0, -3.5, 8.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 1.5, -1.0), Color::gray(1.0)),
		Light(Vector3::vector(1.0, 1.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(-1.0, 1.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 1.5, -8.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, -1.0), Color::gray(1.0)),
		Light(Vector3::vector(1.0, 3.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(-1.0, 3.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, -8.0), Color::gray(1.0)),
		Light(Vector3::vector(2.0, -3.5, -1.0), Color::gray(1.0)),
		Light(Vector3::vector(5.0, -3.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(-5.0, -3.5, -3.0), Color::gray(1.0)),
		Light(Vector3::vector(-2.0, -3.5, -8.0), Color::gray(1.0))
	};

	Scene::addLights(lights);
}

Color RandomSpread::backgroundColor() const
{
	return Color::gray(0.6);
}

Transform RandomSpread::cameraTransform()
{
	return Transform::translation(Vector3::vector(0, 0, -15));
}

std::shared_ptr<Scene> RandomSpread::clone()
{
	std::shared_ptr<RandomSpread> scene = std::make_shared<RandomSpread>();
	for (auto& body : m_bodies) {
		scene->addBody(body.clone());
	}
	for (auto& light : m_lights) {
		scene->addLight(Light(light));
	}

	return scene;
}

const std::string RandomSpread::name() const
{
	return "Random Spread";
}

RandomSpread::~RandomSpread()
{
}