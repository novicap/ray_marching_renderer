#pragma once
#include <memory>
#include <map>
#include "BoundingSphereTreeBottomUp.h"
#include "SphereTracer.h"
#include "Config.h"
#include "Scene.h"
#include "Camera.h"
#include "Vector.h"
#include "Ray.h"
#include "ClosestBody.h"
#include "IntersectedBody.h"

namespace single_object
{
	class SphereTracerBvhAcceleratedBoundingSphereBU : public SphereTracer
	{
	public:
		SphereTracerBvhAcceleratedBoundingSphereBU(std::shared_ptr<Scene> scene, const Camera &camera, const Vector &vector);
		void renderIteration(Color*&, int*&, double&) override;
		virtual std::string name();
		void initScene();
		~SphereTracerBvhAcceleratedBoundingSphereBU();
	private:
		ClosestBody rayMarch(const Ray &ray, const std::vector<IntersectedBody>& bodies, bool primaryRay);
		Color sampleRay(const Ray &ray, const Scene &scene, BoundingSphereTree &aabbTree, int depth, bool primaryRay);

		std::shared_ptr<Scene> m_scene;
		std::shared_ptr<BoundingSphereTree> m_boundingSphereTree;
		std::shared_ptr<Camera> m_camera;
		Vector m_size;
		int m_iterations;
		std::unique_ptr<Color[]> m_colors;
	};
}

