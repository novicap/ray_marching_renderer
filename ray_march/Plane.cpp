#include "Plane.h"
#include "Config.h"
#include <iostream>

double Plane::sdf()
{
	return m_sdfVector.dot(m_normal) + m_distanceFromOrigin;
}

void Plane::stream(std::ostream &strm) const
{
	strm << "Plane(normal: " << m_normal << ", d: " << m_distanceFromOrigin << ")";
}

Plane::~Plane()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Plane destroyed." << std::endl;
	}
}