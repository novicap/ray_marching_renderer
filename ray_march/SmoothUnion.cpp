#include "SmoothUnion.h"
#include "Config.h"
#include <iostream>

double SmoothUnion::sdf()
{
	double firstSdf = m_firstOperand->sdf();
	double secondSdf = m_secondOperand->sdf();
	double h = std::max(m_smoothing - abs(firstSdf - secondSdf), 0.0);
	return std::min(firstSdf, secondSdf) - h * h*0.25 / m_smoothing;
}

void SmoothUnion::stream(std::ostream &strm) const
{
	strm << "SmoothUnion(\n\tFirst: " << *m_firstOperand << ",\n\tSecond: " << *m_secondOperand << ")";
}

SmoothUnion::~SmoothUnion()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "SmoothUnion destroyed." << std::endl;
	}
}