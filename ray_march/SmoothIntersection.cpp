#include "SmoothIntersection.h"
#include "Config.h"
#include <iostream>

void SmoothIntersection::stream(std::ostream &strm) const
{
	strm << "SmoothIntersection(\n\tFirst: " << *m_firstOperand << ",\n\tSecond: " << *m_secondOperand << ")";
}

double SmoothIntersection::sdf()
{
	double firstSdf = m_firstOperand->sdf();
	double secondSdf = m_secondOperand->sdf();
	double h = std::max(m_smoothing - std::abs(firstSdf - secondSdf), 0.0);
	return std::max(firstSdf, secondSdf) + h * h*0.25 / m_smoothing;
}

SmoothIntersection::~SmoothIntersection()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "SmoothIntersection destroyed." << std::endl;
	}
}