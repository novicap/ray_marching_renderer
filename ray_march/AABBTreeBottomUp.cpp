#include "AABBTreeBottomUp.h"
#include "Config.h"
#include "IntersectedBody.h"
#include <iostream>

AABBTreeBottomUp::AABBTreeBottomUp(const std::vector<Body>& bodies)
{
	if (bodies.size() == 0) {
		throw ("Can't build tree, no bodies.");
	}
	std::cout << "Building BVH Tree - AABBTreeBottomUp." << std::endl;
	std::vector<std::shared_ptr<AABBNode>> nodes;
	for (auto &body : bodies) {
		nodes.push_back(std::make_shared<AABBNode>(body));
	}
	buildBottomUp(nodes);
}

void AABBTreeBottomUp::buildBottomUp(std::vector<std::shared_ptr<AABBNode>>& nodes)
{
	//final element, product of two final nodes
	if (nodes.size() == 1) {
		m_root = nodes[0];
		return;
	}

	//will be passed to recursive call
	std::vector<std::shared_ptr<AABBNode>> newNodes;

	while (nodes.size() > 0) {
		int minI, minJ;
		double minVolume = DBL_MAX;
		//if only one element left put it in tree
		if (nodes.size() == 1) {
			newNodes.push_back(nodes[0]);
			nodes.clear();
		}
		else {
			std::shared_ptr<AABBNode> testNode, node;
			//find that makes aabb of minimal volume
			for (int i = 0; i < nodes.size() - 1; i++) {
				for (int j = i + 1; j < nodes.size(); j++) {
					
					 testNode = std::make_shared<AABBNode>(nodes[i], nodes[j]);
					
					double volume = testNode->volume();
					if (volume < minVolume) {
						minI = i;
						minJ = j;
						minVolume = volume;
						node = testNode;
					}
				}
			}

			//children of created node are minI-th and minJ-th node
			node->addLeft(nodes[minI], node);
			node->addRight(nodes[minJ], node);

			//clear nodes for while loop (first greter index to avoid index shift)
			nodes.erase(nodes.begin() + minJ);
			nodes.erase(nodes.begin() + minI);

			//add new node to vector to be used
			nodes.push_back(node);
		}
	}

	buildBottomUp(newNodes);
}

AABBTreeBottomUp::~AABBTreeBottomUp()
{
}