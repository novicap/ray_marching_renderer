#pragma once
#include "Scene.h"

class King : public Scene
{
public:
	King() = default;

	void init() override;
	Color backgroundColor() const override;
	Transform cameraTransform() override;
	std::shared_ptr<Scene> clone();
	const std::string name() const;
	Body body();

	~King();
};

