#include "Rounding.h"
#include <iostream>
#include "Config.h"
#include "Scale.h"

const std::shared_ptr<Shape> Rounding::clone() const
{
	return std::make_shared<Rounding>(m_shape->clone(), m_roundingFactor);
}

double Rounding::sdf()
{
	return (m_shape->sdf() - m_roundingFactor);
}

void Rounding::stream(std::ostream &strm) const
{
	strm << "RoundedShape(" << *m_shape << ", RoundingFactor:" << m_roundingFactor << ")" << std::endl;
}

Rounding::~Rounding()
{
}