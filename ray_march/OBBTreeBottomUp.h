#pragma once
#include "OBBTree.h"
#include "OBBNode.h"
#include "IntersectedBody.h"

class OBBTreeBottomUp : public OBBTree
{
public:
	OBBTreeBottomUp(const std::vector<Body>& bodies);
	~OBBTreeBottomUp();
private:
	void buildBottomUp(std::vector<std::shared_ptr<OBBNode>>& nodes);
	int m_size;
};
