#include "Cylinder.h"
#include "Config.h"
#include <iostream>
#include <algorithm>

double Cylinder::sdf()
{
	Vector xz = Vector(m_sdfVector.x(), m_sdfVector.z());
	Vector d = Vector(xz.length(), m_sdfVector.y()).abs() - Vector(m_radius, m_height);

	return std::min(std::max(d.x(), d.y()), 0.0) + d.max(0.0).length();
}

void Cylinder::stream(std::ostream &strm) const
{
	strm << "Cylinder(Height:" << m_height << ", Radius:" << m_radius << ")";
}

Cylinder::~Cylinder()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Cylinder destroyed." << std::endl;
	}
}