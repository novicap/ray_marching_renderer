#include "TripleBuffering.h"
#include "Config.h"
#include <iostream>

TripleBuffering::TripleBuffering(std::shared_ptr<BufferRendering> buffer0, std::shared_ptr<BufferRendering> buffer1, std::shared_ptr<BufferRendering> buffer2)
{
	m_buffers.push_back(buffer0);
	m_buffers.push_back(buffer1);
	m_buffers.push_back(buffer2);
	m_bufferReadNext = buffer0;
}

std::shared_ptr<BufferRendering> TripleBuffering::writingStart()
{
	if (m_bufferWriting != NULL) {
		throw ("Only one writing allowed.");
	}

	for (int i = 0; i < m_buffers.size(); i++) {
		if (m_buffers[i] != m_bufferReadNext && m_buffers[i] != m_bufferReading) {
			m_bufferWriting = m_buffers[i];
			break;
		}
	}

	return m_bufferWriting;
}

void TripleBuffering::writingDone()
{
	m_bufferReadNext = m_bufferWriting;
	m_bufferWriting = NULL;
}

std::shared_ptr<BufferRendering> TripleBuffering::readingStart()
{
	if (m_bufferReading != NULL) {
		throw ("Only one reading allowed.");
	}

	m_bufferReading = m_bufferReadNext;
	return m_bufferReading;
}

void TripleBuffering::readingDone()
{
	m_bufferReading = NULL;
}

void TripleBuffering::write(std::shared_ptr<BufferRendering> buffer)
{
	std::shared_ptr<BufferRendering> b = writingStart();
	b = buffer;

	writingDone();
}

void TripleBuffering::read(std::shared_ptr<BufferRendering> buffer)
{
	std::shared_ptr<BufferRendering> b = readingStart();
	buffer = b;

	readingDone();
}

TripleBuffering::~TripleBuffering()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Tripple buffering detroyed." << std::endl;
	}
}