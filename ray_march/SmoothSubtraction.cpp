#include "SmoothSubtraction.h"
#include "Config.h"
#include <iostream>

double SmoothSubtraction::sdf()
{
	double h = std::max(m_smoothing - std::abs(-m_firstOperand->sdf() - m_secondOperand->sdf()), 0.0);
	return std::max(-m_firstOperand->sdf(), m_secondOperand->sdf()) + h * h*0.25 / m_smoothing;
}

void SmoothSubtraction::stream(std::ostream &strm) const
{
	strm << "SmoothSubtraction(\n\tFirst: " << *m_firstOperand << ",\n\tSecond: " << *m_secondOperand << ")";
}

SmoothSubtraction::~SmoothSubtraction()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "SmoothSubtraction destroyed." << std::endl;
	}
}