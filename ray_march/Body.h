#pragma once

#include "Shape.h"
#include "Material.h"
#include "Acceleratable.h"

class Body : public Acceleratable
{
public:
	Body() = default;
	/*Body(const Body& body) {
		m_shape = body.m_shape->clone();
		m_material = Material(body.material());
	}*/
	Body(const std::shared_ptr<Shape> &shape, const Material &material) :
		m_shape{ shape->clone() }, m_material{ Material(material) } {};

	Body clone();
	const Material& material() const;
	double sdf() const;
	void initRay(const Ray &r) const;
	bool isConvexOptimizable() const;
	const AABB& aabb() const override;
	const OBB& obb() const override;
	const BoundingSphere& boundingSphere() const override;
	void transform(const Transform &t, bool append = true);
	void applyDistance(double dO) const;
	Vector3 estNormal_(const Vector3 &v);
	const std::shared_ptr<Shape>& shape() const;
	void setMaterial(const Material &m);

	const static bool compareAABBX(const Body& o1, const Body& o2);
	const static bool compareAABBY(const Body& o1, const Body& o2);
	const static bool compareAABBZ(const Body& o1, const Body& o2);

	const static bool compareOBBX(const Body& o1, const Body& o2);
	const static bool compareOBBY(const Body& o1, const Body& o2);
	const static bool compareOBBZ(const Body& o1, const Body& o2);

	friend std::ostream& operator<<(std::ostream &strm, const Body &body) {
		body.stream(strm);
		return strm;
	}

	~Body();
protected:
	virtual void stream(std::ostream& strm) const;
private:
	std::shared_ptr<Shape> m_shape;
	Material m_material;
};
