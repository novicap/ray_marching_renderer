#pragma once

#include "OBB.h"
#include "VectorPointCGALAdapter.h"

//per CGAL documentation
//   4---------7             ^z   7y
//  /|		  /|			 |   /
// / |		 / |			 |	/
//5--+------6  |			 | /
//|	 3------|--2      -------+--------->x
//|	/		| /             /|
//|/		|/             / |
//0---------1             /  |

class OBBCGALAdapter
{
public:
	static OBB toObb(const std::array<Point_3, 8>& cgalObb) {
		std::array<Vector3, 8> points;
		for (int i = 0; i < 8; i++) {
			points[i] = VectorPointCGALAdapter::toVector3(cgalObb[i]);
		}

		return OBB(points);
	}
};