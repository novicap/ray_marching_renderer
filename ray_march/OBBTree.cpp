#include "OBBTree.h"
#include "Config.h"

std::shared_ptr<OBBTree> OBBTree::clone()
{
	std::shared_ptr<OBBTree> newOBBTree = std::make_shared<OBBTree>();
	newOBBTree->m_root = std::make_shared<OBBNode>(*m_root);
	copyNode(newOBBTree->m_root, m_root);
	return newOBBTree;
}

void OBBTree::print()
{
	if (m_root == nullptr) {
		return;
	}

	print(m_root);
}

void OBBTree::copyNode(std::shared_ptr<OBBNode> destination, std::shared_ptr<OBBNode> node)
{
	if (node == nullptr) {
		return;
	}

	if (node->left() != nullptr) {
		std::shared_ptr<OBBNode> left = std::make_shared<OBBNode>(*node->left());
		destination->addLeft(left, destination->parent());
		copyNode(destination->left(), node->left());
	}

	if (node->right() != nullptr) {
		std::shared_ptr<OBBNode> right = std::make_shared<OBBNode>(*node->right());
		destination->addRight(right, destination->parent());
		copyNode(destination->right(), node->right());
	}
}

void OBBTree::print(std::shared_ptr<OBBNode> node)
{
	if (node == nullptr) {
		return;
	}

	std::cout << "[";
	for (auto& vertex : node->obb().vertices()) {
		std::cout << "(" << vertex.x() << "," << vertex.y() << "," << vertex.z() << "),";
	}
	std::cout << "]," << std::endl;
	print(node->left());
	print(node->right());
}

std::vector<IntersectedBody> OBBTree::traverseTreeST(const Ray& r, Color& c) {
	std::vector<IntersectedBody> bodies;
	m_steps = 0;
	traverseTreeST(r, m_root, bodies, c, 0.0);
	return bodies;
}

void OBBTree::traverseTreeST(
	const Ray& r, std::shared_ptr<OBBNode>& current,
	std::vector<IntersectedBody>& bodies, Color& c, double passed)
{
	current->obb().initRay(r);
	double dO = passed;
	current->obb().applyDistance(dO);

	double prevDistance = DBL_MAX;
	double epsilon = EPSILON;
	double maxDist = MAX_DIST;
	double distance = 2 * epsilon;
	std::shared_ptr<OBBNode> left = current->left();
	std::shared_ptr<OBBNode> right = current->right();

	while (dO < maxDist && distance > epsilon) {
		distance = current->obb().sdf();
		//"convex optimisation"
		if (OPTIMISE_FOR_CONVEXITY) {
			if (distance < prevDistance) {
				prevDistance = distance;
			}
			else {
				dO = maxDist;
				break;
			}
		}
		//end of "convex optimisation"

		dO += distance;

		if (distance < epsilon && current->isLeaf()) {
			bodies.push_back(IntersectedBody(current->bodies()[0], dO));
			return;
		}

		if (distance < epsilon && left != nullptr) {
			traverseTreeST(r, left, bodies, c, dO);
		}

		if (distance < epsilon && right != nullptr) {
			traverseTreeST(r, right, bodies, c, dO);
		}

		m_steps++;
		current->obb().applyDistance(dO);
	}

	c.setR(m_steps); c.setG(m_steps); c.setB(m_steps);
}