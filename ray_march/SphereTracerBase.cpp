#include "SphereTracerBase.h"
#include "Config.h"
#include "Math.h"
#include <omp.h>
#include <math.h>
#include <iostream>
#include <algorithm>
#include <iostream>
#include <chrono>
#include <limits>
#include <queue>

single_object::SphereTracerBase::SphereTracerBase(
	std::shared_ptr<Scene> scene, const Camera& camera, const Vector& size) :
	m_scene{ scene }, m_camera{ std::make_shared<Camera>(camera) }, m_size{ size } {
	m_colors = std::make_unique<Color[]>((int)m_size.x()*(int)m_size.y());
	m_iterations = 0;
}

void single_object::SphereTracerBase::renderIteration(
	Color*& pixelColors, int*& imageData, double& iterTime)
{
	//render timer start
	std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> elapsed;

	int ompThreads = OMP_THREADS;
	m_iterations += 1;

	double duration = 0.0;
	//omp private vars
	double m;
	int width;
	int height;
	Vector pixel;
	Ray cameraRay;
	Color sample;
	Vector size;
	std::shared_ptr<Camera> camera;
	std::shared_ptr<Scene> scene;
	int iterations;
#pragma omp parallel num_threads(ompThreads) \
private(m, width, height, pixel, cameraRay, sample, size, camera, iterations, scene)
	{
		//generate random
		std::uniform_real_distribution<double> unif(0.0, 1.0);
		std::default_random_engine re;
		re.seed((int)time(0));

		//init required private vars (others stay default constructed)
		width = (int)m_size.x();
		height = (int)m_size.y();
		iterations = m_iterations;
		m = std::min(m_size.x(), m_size.y());
		size = m_size / 2;
		//Clone scene for each thread, so that each thread has all of the objects required
		//for sdf. Cloning can not be avoided due to present racing condition in rayInit for shapes.
		//By cloning every thread has full scene.
		scene = m_scene->clone();
		camera = m_camera->clone();
#pragma omp for
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int position = y * width + x;
				pixel.setX(((x - size.x() + unif(re)) / m));
				pixel.setY(((y - size.y() + unif(re)) / -m));

				camera->sampleExitingRay(pixel, cameraRay);
				scene->cameraTransform().applyTo(cameraRay);

				//for image
				sample = sampleRay(cameraRay, *scene, 3);

				pixelColors[position] += sample;
				imageData[position] = (pixelColors[position] * (1.0 / iterations)).codeClamp();
			}
		}
	}
	std::chrono::high_resolution_clock::time_point finish = std::chrono::high_resolution_clock::now();
	elapsed = finish - start;
	//render timer finish
	iterTime = elapsed.count();
	std::cout << "Elapsed time: " << elapsed.count() << " s\n";
}

std::string single_object::SphereTracerBase::name()
{
	return "SphereTracerBase";
}

void single_object::SphereTracerBase::initScene()
{
	m_scene->init();
}

Color single_object::SphereTracerBase::sampleRay(const Ray &ray, const Scene &scene, int depthRemaining)
{
	if (depthRemaining == 0) {
		return Color::black();
	}

	double epsilon = EPSILON;
	double maxDist = MAX_DIST;

	ClosestBody closestBody = rayMarch(ray, scene);

	if (OUTPUT_HEATMAP) {
		return Color::rgb(closestBody.steps() / 255.0, closestBody.steps() / 255.0, closestBody.steps() / 255.0);
	}

	Vector3 collisionPoint = ray.source() + (ray.direction()*closestBody.distance());

	if (closestBody.distance() >= maxDist) {
		return scene.backgroundColor();
	}

	Vector3 normal_ = closestBody.closestBody().shape()->estNormal_(collisionPoint);	//normal at collision point
	Vector3 rayInverse = -ray.direction();												//inverse of ray
	Vector3 rayReflectionV = rayInverse.reflectN(normal_);								//reflection
	Vector3 marchSource = collisionPoint + ((normal_ * epsilon) * 2.0);
	double reflectionLength = rayReflectionV.length();

	Material material = closestBody.closestBody().material();
	//return material.diffuse();

	Color totalDiffuse = Color::black();
	Color totalSpecular = Color::black();

	for (int i = 0; i < scene.getLights().size(); i++) {
		Vector3 lightP = scene.getLights()[i].position();
		Vector3 lightV = lightP - collisionPoint;
		Vector3 lightV_ = lightV.normalized();

		//move ray from collision point to avoid accident ray march break
		Ray lightRay = Ray::sd(marchSource, lightV_);
		ClosestBody closestBodyLight = rayMarch(lightRay, scene);
		//std::cout << closestBodyLight.distance() << std::endl;
		//if path to light source is not obstructed by some other object
		if (closestBodyLight.distance() >= lightV.length()) {
			//diffuse
			double lightVLengthSquare = lightV.lengthSquared();
			double lightVLength = Math::fastSqrt(lightVLengthSquare);
			double cosLightNormal = (lightV.dot(normal_) / lightVLength);

			if (cosLightNormal > 0) {
				totalDiffuse = totalDiffuse + (scene.getLights()[i].color() * (cosLightNormal / lightVLengthSquare));
			}
			//specular
			if (material.specular().notZero()) {
				double cosLightReflection = lightV.dot(rayReflectionV) / (lightVLength * reflectionLength);
				if (cosLightReflection > 0) {
					double phong = std::pow(cosLightReflection, material.shininess());
					totalSpecular = totalSpecular + (scene.getLights()[i].color() * (phong / lightVLengthSquare));
				}
			}
		}
	}

	Color result = Color::black();

	result = result + (totalDiffuse  * (material.diffuse()));
	result = result + (totalSpecular * (material.specular()));

	//reflection
	if (material.reflective().notZero()) {
		Ray source = Ray::sd(marchSource, rayReflectionV);
		Color reflection = sampleRay(source, scene, depthRemaining - 1);
		result = result + (reflection * (material.reflective()));
	}

	return result;
}

ClosestBody single_object::SphereTracerBase::rayMarch(const Ray &r, const Scene &scene)
{
	double maxDist = MAX_DIST;
	double epsilon = EPSILON;

	ClosestBody cb;

	
	for (auto &body : scene.getBodies()) {
		//init ray inside body for each body
		//so every body has its own copy of ray
		//and all transformations associated with
		//ray and resulting vectors
		body.initRay(r);

		//used for "convex" optimisation
		double prevDistance = DBL_MAX;
		//use for triangle optimization
		double tePrevDistance = 0;
		//2*elipson is to allow while loop to start
		double distance = 2 * epsilon;
		//entire ray length
		double dO = 0;
		//for heatmaps
		int stepCounter = 0;

		while (dO < maxDist && distance >= epsilon) {
			distance = body.sdf();
			stepCounter++;

			//"convex optimisation"
			if (OPTIMISE_FOR_CONVEXITY && body.isConvexOptimizable()) {
				if (distance < prevDistance) {
					prevDistance = distance;
				}
				else {
					dO = maxDist;
					break;
				}
			}
			//end of "convex optimisation"

			dO += distance;

			//"ray length optimisation"
			if (OPTIMISE_FOR_RAY_LENGTH) {
				if (dO > cb.distance()) {
					dO = maxDist;
					break;
				}
			}
			//end of "ray length optimisation"

			body.applyDistance(dO);
		}

		if (dO < cb.distance()) {
			cb = ClosestBody(dO, body, stepCounter);
		}
	}

	return cb;
}

single_object::SphereTracerBase::~SphereTracerBase()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "SphereTracerBase destroyed." << std::endl;
	}
}