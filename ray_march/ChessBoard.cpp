#include "ChessBoard.h"
#include "Box.h"
#include "Union.h"
#include "SmoothUnion.h"
#include "Rook.h"
#include "Pawn.h"
#include "Bishop.h"
#include "King.h"

void ChessBoard::init()
{
	std::shared_ptr<Box> b1 = std::make_shared<Box>(Vector3::vector(8.3, 0.3, 8.3));
	Transform t = Transform::translation(Vector3::vector(0, -0.21, 0));
	std::shared_ptr<Box> b2 = std::make_shared<Box>(Vector3::vector(10.5, 0.3, 10.5));
	Transform t2 = Transform::translation(Vector3::vector(0, -0.5, 0));
	b1->transform(t);
	b1->applyBvhTransformations();
	b2->transform(t2);
	b2->applyBvhTransformations();
	std::shared_ptr<SmoothUnion> b3 = std::make_shared<SmoothUnion>(b1, b2, 0.3);

	std::vector<std::shared_ptr<Box>> whites;
	for (int i = 0; i < 32; i++) {
		std::shared_ptr<Box> b = std::make_shared<Box>(Vector3::vector(1, 0.1, 1));
		Transform t = Transform::translation(Vector3::vector(-((i % 4) * 2 + ((i / 4) % 2) - 3.5) * 2, 0, (i / 4) * 2 - 7));

		b->transform(t);
		b->applyBvhTransformations();
		whites.push_back(b);
	}

	std::vector<std::shared_ptr<Box>> blacks;
	for (int i = 0; i < 32; i++) {
		std::shared_ptr<Box> b = std::make_shared<Box>(Vector3::vector(1, 0.1, 1));
		if (((i / 4) % 2) == 0)
		{
			Transform t = Transform::translation(Vector3::vector(-(((i % 4) * 2 + ((i / 4) % 2)) - 2.5) * 2, 0, (i / 4) * 2 - 7));
			b->transform(t);
			b->applyBvhTransformations();
			blacks.push_back(b);
		}
		else {
			Transform t = Transform::translation(Vector3::vector(-(((i % 4) * 2 + ((i / 4) % 2)) - 4.5) * 2, 0, (i / 4) * 2 - 7));
			b->transform(t);
			b->applyBvhTransformations();
			blacks.push_back(b);
		}

	}

	Rook rook;
	Body whiteRook = rook.body();
	whiteRook.setMaterial(Material(Color::rgb(0.96, 0.96, 0.86)));
	Transform whiteRookPosition = Transform::translation(Vector3::vector(7, 2.7, -7));
	whiteRook.transform(whiteRookPosition, false);
	whiteRook.shape()->applyBvhTransformations();
	Body blackRook = rook.body();
	blackRook.setMaterial(Material(Color::rgb(0.2031, 0.1333, 0.0635)));
	Transform blackRookPosition = Transform::translation(Vector3::vector(-1, 2.7, 3));
	blackRook.transform(blackRookPosition, false);
	blackRook.shape()->applyBvhTransformations();

	Pawn pawn;
	Body whitePawn1 = pawn.body();
	whitePawn1.setMaterial(Material(Color::rgb(0.96, 0.96, 0.86)));
	Transform whitePawn1Position = Transform::translation(Vector3::vector(-7, 2, -1));
	whitePawn1.transform(whitePawn1Position, false);
	whitePawn1.shape()->applyBvhTransformations();
	Body whitePawn2 = pawn.body();
	whitePawn2.setMaterial(Material(Color::rgb(0.96, 0.96, 0.86)));
	Transform whitePawn2Position = Transform::translation(Vector3::vector(-3, 2, -3));
	whitePawn2.transform(whitePawn2Position, false);
	whitePawn2.shape()->applyBvhTransformations();
	Body whitePawn3 = pawn.body();
	whitePawn3.setMaterial(Material(Color::rgb(0.96, 0.96, 0.86)));
	Transform whitePawn3Position = Transform::translation(Vector3::vector(3, 2, 1));
	whitePawn3.transform(whitePawn3Position, false);
	whitePawn3.shape()->applyBvhTransformations();

	Body blackPawn1 = pawn.body();
	blackPawn1.setMaterial(Material(Color::rgb(0.2031, 0.1333, 0.0635)));
	Transform blackPawn1Position = Transform::translation(Vector3::vector(-7, 2, 1));
	blackPawn1.transform(blackPawn1Position, false);
	blackPawn1.shape()->applyBvhTransformations();
	Body blackPawn2 = pawn.body();
	blackPawn2.setMaterial(Material(Color::rgb(0.2031, 0.1333, 0.0635)));
	Transform blackPawn2Position = Transform::translation(Vector3::vector(-5, 2, 3));
	blackPawn2.transform(blackPawn2Position, false);
	blackPawn2.shape()->applyBvhTransformations();
	Body blackPawn3 = pawn.body();
	blackPawn3.setMaterial(Material(Color::rgb(0.2031, 0.1333, 0.0635)));
	Transform blackPawn3Position = Transform::translation(Vector3::vector(7, 2, 3));
	blackPawn3.transform(blackPawn3Position, false);
	blackPawn3.shape()->applyBvhTransformations();

	Bishop bishop;
	Body whiteBishop = bishop.body();
	whiteBishop.setMaterial(Material(Color::rgb(0.96, 0.96, 0.86)));
	Transform whiteBishopPosition = Transform::translation(Vector3::vector(-1, 2.9, -1));
	whiteBishop.transform(whiteBishopPosition, false);
	whiteBishop.shape()->applyBvhTransformations();
	Body blackBishop = bishop.body();
	blackBishop.setMaterial(Material(Color::rgb(0.2031, 0.1333, 0.0635)));
	Transform blackBishopPosition = Transform::translation(Vector3::vector(-1, 2.9, 7));
	blackBishop.transform(blackBishopPosition, false);
	blackBishop.shape()->applyBvhTransformations();

	King king;
	Body whiteKing = king.body();
	whiteKing.setMaterial(Material(Color::rgb(0.96, 0.96, 0.86)));
	Transform whiteKingPosition = Transform::translation(Vector3::vector(-3, 3.5, -1));
	whiteKing.transform(whiteKingPosition, false);
	whiteKing.shape()->applyBvhTransformations();
	Body blackKing = king.body();
	blackKing.setMaterial(Material(Color::rgb(0.2031, 0.1333, 0.0635)));
	Transform blackKingPosition = Transform::translation(Vector3::vector(3, 3.5, 5));
	blackKing.transform(blackKingPosition, false);
	blackKing.shape()->applyBvhTransformations();

	std::vector<Body> bodies;

	for (auto& obj : whites) {
		bodies.push_back(Body(obj, Material(Color::gray(1))));
	}

	for (auto& obj : blacks) {
		bodies.push_back(Body(obj, Material(Color::gray(0.05))));
	}

	bodies.push_back(Body(b3, Material(Color::rgb(0.39, 0.26, 0.13))));
	bodies.push_back(whiteRook);
	bodies.push_back(blackRook);
	bodies.push_back(whitePawn1);
	bodies.push_back(whitePawn2);
	bodies.push_back(whitePawn3);
	bodies.push_back(blackPawn1);
	bodies.push_back(blackPawn2);
	bodies.push_back(blackPawn3);
	bodies.push_back(whiteBishop);
	bodies.push_back(blackBishop);
	bodies.push_back(whiteKing);
	bodies.push_back(blackKing);

	std::vector<Light> lights{
		Light(Vector3::vector(-12.0, 6, -9), Color::gray(1.0)),
		Light(Vector3::vector(-8.0, 6, -9), Color::gray(1.0)),
		Light(Vector3::vector(-4.0, 6, -9), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 6, -9), Color::gray(1.0)),
		Light(Vector3::vector(4.0, 6, -9), Color::gray(1.0)),
		Light(Vector3::vector(8.0, 6, -9), Color::gray(1.0)),
		Light(Vector3::vector(12.0, 6, -9), Color::gray(1.0)),

		Light(Vector3::vector(-12.0, 6, 0), Color::gray(1.0)),
		Light(Vector3::vector(-8.0, 6, 0), Color::gray(1.0)),
		Light(Vector3::vector(-4.0, 6, 0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 6, 0), Color::gray(1.0)),
		Light(Vector3::vector(4.0, 6, 0), Color::gray(1.0)),
		Light(Vector3::vector(8.0, 6, 0), Color::gray(1.0)),
		Light(Vector3::vector(12.0, 6, 0), Color::gray(1.0)),

		Light(Vector3::vector(-12.0, 6, 9), Color::gray(1.0)),
		Light(Vector3::vector(-8.0, 6, 9), Color::gray(1.0)),
		Light(Vector3::vector(-4.0, 6, 9), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 6, 9), Color::gray(1.0)),
		Light(Vector3::vector(4.0, 6, 9), Color::gray(1.0)),
		Light(Vector3::vector(8.0, 6, 9), Color::gray(1.0)),
		Light(Vector3::vector(12.0, 6, 9), Color::gray(1.0)),
	};

	Scene::addLights(lights);
	Scene::addBodies(bodies);
}


Color ChessBoard::backgroundColor() const
{
	return Color::rgb(0.239, 0.302, 0.365);
}

Transform ChessBoard::cameraTransform()
{
	return Transform::rotationAroundX(55).andThen(Transform::translation(Vector3::vector(0.0, 16.0, -14)));
	//return Transform::rotationAroundX(45).andThen(Transform::translation(Vector3::vector(0.0, 15.0, -21))).andThen(Transform::rotationAroundY(-45));
}

std::shared_ptr<Scene> ChessBoard::clone()
{
	std::shared_ptr<ChessBoard> sb = std::make_shared<ChessBoard>();

	for (auto& body : m_bodies) {
		sb->addBody(body.clone());
	}
	for (auto& light : m_lights) {
		sb->addLight(Light(light));
	}

	return sb;
}

const std::string ChessBoard::name() const
{
	return "Chess board";
}

ChessBoard::~ChessBoard()
{
}
