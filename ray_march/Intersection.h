#pragma once
#include "Combination.h"
class Intersection : public Combination
{
public:
	Intersection(std::shared_ptr<Shape> shape1, std::shared_ptr<Shape> shape2) :
		Combination(shape1, shape2) {};

	const std::shared_ptr<Shape> clone() const
	{
		return std::make_shared<Intersection>(*this);
	};
	double sdf() override;
	~Intersection();
protected:
	void stream(std::ostream&) const;
};
