#pragma once

#include <ostream>
#include "Vector.h"

class Vector3
{
public:
	Vector3() : m_x{ 0.0 }, m_y{ 0.0 }, m_z{ 0.0 } {};

	//copy constructor
	Vector3(const Vector3 &v) : m_x{ v.m_x }, m_y{ v.m_y }, m_z{ v.m_z } {};

	static Vector3 zero();
	static Vector3 ex();
	static Vector3 ey();
	static Vector3 ez();
	static Vector3 exy();
	static Vector3 exz();
	static Vector3 eyz();
	static Vector3 exyz();
	static Vector3 vector(double x, double y, double z);
	static Vector3 vec2z(const Vector&, double z);
	static Vector3 vec2y(const Vector&, double y);

	double x() const;
	double y() const;
	double z() const;

	void setX(const double x);
	void setY(const double y);
	void setZ(const double z);

	Vector3 cross(const Vector3 &v) const;
	double dot(const Vector3 &v) const;
	Vector3 abs() const;
	Vector3 sign() const;
	Vector3 floor() const;
	Vector3 ceil() const;
	Vector3 round() const;
	double length() const;
	double lengthSquared() const;
	void normalize();
	Vector3 normalized() const;
	//per component min/max
	Vector3 max(const double) const;
	Vector3 min(const double) const;
	Vector3 reflectN(Vector3 &v) const;
	Vector3 reflect(Vector3 &v) const;

	Vector3 & operator+=(const Vector3 &v);
	Vector3 operator+(const Vector3 &v) const;
	Vector3 & operator-=(const Vector3 &v);
	Vector3 operator-(const Vector3 &v) const;
	Vector3 & operator*=(const Vector3 &v);
	Vector3 operator*(const Vector3 &v) const;
	Vector3 & operator/=(const Vector3 &v);
	Vector3 operator/(const Vector3 &v) const;
	Vector3 & operator*=(double d);
	Vector3 operator*(double d) const;
	Vector3 & operator/=(double d);
	Vector3 operator/(double d) const;
	Vector3 operator-() const;
	bool operator==(const Vector3 &v) const;
	bool operator!=(const Vector3 &v) const;

	~Vector3();

	friend std::ostream& operator<<(std::ostream &strm, const Vector3 &vec3) {
		return strm << "Vector3(" << vec3.m_x << ", " << vec3.m_y << ", " << vec3.m_z << ")";
	}
private:
	double m_x, m_y, m_z;
	Vector3(const double x, const double y, const double z) : m_x{ x }, m_y{ y }, m_z{ z } {};
};
