#pragma once
#include "Vector3.h"
#include "Transform.h"
#include "VectorPointCGALAdapter.h"
#include <array>
#include <vector>

class AABB
{
public:
	AABB() = default;

	AABB(const Vector3& v) {
		m_vertex = v;
		initStVertices();
	}

	AABB(std::array<Vector3, 8>& v) {
		m_vertices = v;
		m_centroid = (m_vertices[7] + m_vertices[0]) / 2;
		m_vertex = m_vertices[7] - m_centroid;
		m_rayTransformation = Transform::translation(m_centroid).inverse();
		calculateVolume();
	}

	//copy constructor
	AABB(const AABB& aabb) {
		m_vertex = Vector3(aabb.m_vertex);
		m_sdfVector = Vector3(aabb.m_sdfVector);
		m_centroid = Vector3(aabb.m_centroid);
		m_rayTransformation = Transform(aabb.m_rayTransformation);
		m_transformations = aabb.m_transformations;
		m_inverseTransformations = aabb.m_inverseTransformations;

		for (int i = 0; i < 8; i++) {
			m_vertices[i] = Vector3(aabb.m_vertices[i]);
		}
	}

	double sdf();
	void initRay(const Ray &r);
	void applyDistance(double dO);
	const Vector3& sdfVector() const;
	const Vector3& vertex() const;
	const Vector3& vertex(int v) const;
	const Vector3& centroid() const;

	AABB merge(const AABB& aabb);

	void calculateAABB();
	void transform(const Transform &t, bool append = true);
	void applyTransformations();
	void applyInverseTransformations();
	const std::vector<Transform>& transformations() const;
	void printVertices() const;
	void printAABB() const;

	double width() const;
	double height() const;
	double depth() const;

	double minX() const;
	double minY() const;
	double minZ() const;
	double maxX() const;
	double maxY() const;
	double maxZ() const;
	double volume() const;

	const std::array<Vector3, 8>& vertices() const;

	friend std::ostream& operator<<(std::ostream& strm, const AABB& aabb) {
		return strm << "AABB: " << aabb.vertex() << std::endl;
	}

	~AABB();
private:
	double m_volume;
	Vector3 m_vertex;
	Vector3 m_sdfVector;
	Vector3 m_centroid;
	Ray m_ray;
	std::array<Vector3, 8> m_vertices;
	Transform m_rayTransformation = Transform::identity();
	std::vector<Transform> m_transformations;
	std::vector<Transform> m_inverseTransformations;

	void calculateVolume();
	void calculateVertices();
	void initStVertices();
};
