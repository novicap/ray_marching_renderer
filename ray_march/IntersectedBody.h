#pragma once
#include "Body.h"
class IntersectedBody
{
public:
	IntersectedBody(const Body body, double distance) : m_body{ body }, m_distance{ distance } {};
	double distance() const;
	const Body& body() const;
	~IntersectedBody();
private:
	double m_distance;
	Body m_body;
};
