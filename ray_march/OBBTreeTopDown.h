#pragma once
#include "OBBTree.h"
#include "Body.h"
#include <functional>

class OBBTreeTopDown : public OBBTree
{
public:
	OBBTreeTopDown(const std::vector<Body>& bodies);
	~OBBTreeTopDown();
private:
	void buildTopDown(const std::shared_ptr<OBBNode>& parent, const std::vector<Body>& b, int start, int end, bool isLeftChild);
	std::function<bool(const Body&, const Body&)> sortMethod(const std::shared_ptr<OBBNode>& node);
};
