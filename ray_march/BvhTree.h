#pragma once
#include "IntersectedBody.h"
#include <vector>
class BvhTree
{
public:
	virtual std::vector<IntersectedBody> traverseTreeST(const Ray& r) = 0;
private:
	void traverseTreeST(const Ray& r, std::shared_ptr<Node>& current,
		std::vector<IntersectedBody>& bodies, double passed);
};