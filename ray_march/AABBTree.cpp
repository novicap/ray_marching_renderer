#include "AABBTree.h"
#include "Config.h"

std::shared_ptr<AABBTree> AABBTree::clone()
{
	std::shared_ptr<AABBTree> newAABBTree = std::make_shared<AABBTree>();
	newAABBTree->m_root = std::make_shared<AABBNode>(*m_root);
	copyNode(newAABBTree->m_root, m_root);
	return newAABBTree;
}

void AABBTree::print()
{
	if (m_root == nullptr) {
		return;
	}

	print(m_root);
}

void AABBTree::copyNode(std::shared_ptr<AABBNode> destination, std::shared_ptr<AABBNode> node)
{
	if (node == nullptr) {
		return;
	}

	if (node->left() != nullptr) {
		std::shared_ptr<AABBNode> left = std::make_shared<AABBNode>(*node->left());
		destination->addLeft(left, destination->parent());
		copyNode(destination->left(), node->left());
	}

	if (node->right() != nullptr) {
		std::shared_ptr<AABBNode> right = std::make_shared<AABBNode>(*node->right());
		destination->addRight(right, destination->parent());
		copyNode(destination->right(), node->right());
	}
}

void AABBTree::print(std::shared_ptr<AABBNode> node)
{
	if (node == nullptr) {
		return;
	}

	std::cout << "[";
	for (auto& vertex : node->aabb().vertices()) {
		std::cout << "(" << vertex.x() << "," << vertex.y() << "," << vertex.z() << "),";
	}
	std::cout << "]," << std::endl;
	print(node->left());
	print(node->right());
}

std::vector<IntersectedBody> AABBTree::traverseTreeST(const Ray& r, Color& c) {
	std::vector<IntersectedBody> bodies;
	m_steps = 0;
	traverseTreeST(r, m_root, bodies, c, 0.0);
	return bodies;
}

void AABBTree::traverseTreeST(const Ray& r, std::shared_ptr<AABBNode>& current, std::vector<IntersectedBody>& bodies, Color &c, double passed)
{
	current->aabb().initRay(r);
	double dO = passed;
	current->aabb().applyDistance(dO);

	double prevDistance = DBL_MAX;
	double epsilon = EPSILON;
	double maxDist = MAX_DIST;
	double distance = 2 * epsilon;
	std::shared_ptr<AABBNode> left = current->left();
	std::shared_ptr<AABBNode> right = current->right();

	while (dO < maxDist && distance > epsilon) {
		distance = current->aabb().sdf();
		//"convex optimisation"
		if (OPTIMISE_FOR_CONVEXITY) {
			if (distance < prevDistance) {
				prevDistance = distance;
			}
			else {
				dO = maxDist;
				break;
			}
		}
		//end of "convex optimisation"

		dO += distance;

		if (distance < epsilon && current->isLeaf()) {
			bodies.push_back(IntersectedBody(current->bodies()[0], dO));
			return;
		}

		if (distance < epsilon && left != nullptr) {

			traverseTreeST(r, left, bodies, c, dO);
		}

		if (distance < epsilon && right != nullptr) {
			
			traverseTreeST(r, right, bodies, c, dO);
		}
		m_steps++;
		current->aabb().applyDistance(dO);
	}

	c.setR(m_steps); c.setG(m_steps); c.setB(m_steps);
}