#pragma once
#include <vector>
#include <ostream>
#include <stdlib.h>
#include "Body.h"
#include "Light.h"
#include "Transform.h"
#include "Config.h"
#include "Math.h"

class Scene
{
public:
	//ST transformation order T(ranslation)S(cale)R(otation)
	Scene() = default;

	void addBody(const Body &body);
	void addBodies(const std::vector<Body> &bodies);
	const std::vector<Body>& getBodies() const;
	void addLight(const Light &light);
	void addLights(const std::vector<Light> &lights);
	const std::vector<Light>& getLights() const;

	virtual void init() = 0;
	virtual std::shared_ptr<Scene> clone() = 0;
	virtual Color backgroundColor() const = 0;
	virtual Transform cameraTransform() = 0;
	virtual const std::string name() const = 0;

	~Scene();

	friend std::ostream& operator<<(std::ostream &strm, const Scene &scene)
	{
		strm << "Bodies: " << std::endl;
		for (int i = 0; i < scene.m_bodies.size(); i++) {
			strm << "\t" << scene.m_bodies[i] << std::endl;
		}
		strm << std::endl << "Lights: " << std::endl;
		for (int i = 0; i < scene.m_lights.size(); i++) {
			strm << "\t" << scene.m_lights[i] << std::endl;
		}

		return strm;
	}

protected:
	std::vector<Body> m_bodies;
	std::vector<Light> m_lights;
};