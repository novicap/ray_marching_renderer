#pragma once
#include "Color.h"
#include <ostream>

class Material
{
public:
	Material() : Material(Color::gray(0.0)) {};
	Material(const Color& diffuse, const Color& specular, const double shininess,
		const Color& reflective, const Color &refractive, const double refractionIndex) :
		m_diffuse{ diffuse }, m_specular{ specular }, m_reflective{ reflective }, m_refractive{ refractive },
		m_shininess{ shininess }, m_refractionIndex{ refractionIndex } {};
	Material(const Color &c);
	Material(const Material &m) {
		m_diffuse = m.m_diffuse;
		m_specular = m.m_specular;
		m_reflective = m.m_reflective;
		m_refractive = m.m_refractive;
		m_shininess = m.m_shininess;
		m_refractionIndex = m.m_refractionIndex;
	}

	static Material glass(double refractiveIndex, double reflectance);
	static Material mirror();

	const Color& diffuse() const;
	const Color& specular() const;
	const Color& reflective() const;
	const Color& refractive() const;
	const double shininess() const;
	const double refractionIndex() const;

	friend std::ostream& operator<<(std::ostream& strm, const Material& m) {
		return strm << "Material\n\t(Diffuse: " << m.m_diffuse << ";\n\tSpecular: " << m.m_specular << ";\n\tReflective: " << m.m_reflective
			<< ";\n\tRefractive: " << m.m_refractive << ";\n\tShininess: " << m.m_shininess << ";\n\tRefraction index: " << m.m_refractionIndex << ")\n\t";
	}

	~Material();
private:
	Color m_diffuse, m_specular, m_reflective, m_refractive;
	double m_shininess, m_refractionIndex;
};
