#pragma once
#include "Scene.h"
class RandomSpread : public Scene
{
public:
	RandomSpread() = default;

	void init() override;
	Color backgroundColor() const override;
	Transform cameraTransform() override;
	std::shared_ptr<Scene> clone();
	const std::string name() const;

	~RandomSpread();
};
