#pragma once
#include "Scene.h"
#include "Camera.h"
#include "Color.h"
#include <omp.h>

class RayMarcher
{
public:
	RayMarcher(std::shared_ptr<Scene> scene, Camera&, Vector&);
	void renderIteration(std::unique_ptr<Color[]>&, std::unique_ptr<int[]>&);
	double rayMarch(Ray&, Scene&);
	double distanceTo(Vector3&, Scene&);
	int getBodyIndexClosestTo(Vector3&, Scene&);
	Vector3 estNormal_(Vector3&, Scene&);
	Color sampleRay(Ray&, Scene&, int);
	~RayMarcher();
private:
	std::shared_ptr<Scene> m_scene;
	Camera& m_camera;
	Vector& m_size;
	int m_iterations;
	std::unique_ptr<Color[]> m_colors;
};

