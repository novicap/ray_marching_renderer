#pragma once
#include "Body.h"
#include <limits>
class ClosestBody
{
public:
	ClosestBody() : m_distance{ std::numeric_limits<double>::max() }, m_stepCounter{ 0 } {};
	ClosestBody(double distance, const Body& closestBody, int stepCounter) :
		m_distance{ distance }, m_closestBody{ closestBody }, m_stepCounter{ stepCounter } {};

	Body& closestBody();

	int steps() const;
	double distance() const;
	void setDistance(double distance);

	~ClosestBody();
private:
	int m_stepCounter;
	double m_distance;
	Body m_closestBody;
};

struct CompareDistance {
	bool operator()(ClosestBody const& b1, ClosestBody const& b2)
	{
		return b1.distance() > b2.distance();
	}
};