#pragma once
#include "Alteration.h"
class Scale : public Alteration
{
public:
	Scale(std::shared_ptr<Shape> shape, double scaleFactor) :
		Alteration(shape), m_scaleFactor{ scaleFactor } {};
	const std::shared_ptr<Shape> clone() const override;
	double sdf() override;
	~Scale();
private:
	double m_scaleFactor;
protected:
	void stream(std::ostream& str) const;
};
