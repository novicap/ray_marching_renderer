#include "Color.h"
#include "Math.h"
#include "Config.h"
#include <cmath>
#include <iostream>
#include <algorithm>

Color::Color()
{
	m_r = 0;
	m_g = 0;
	m_b = 0;
}

Color Color::rgb(const double r, const double g, const double b)
{
	return Color(r, g, b);
}

Color Color::hsb(double h, double s, double b)
{
	h = Math::mod1(h / 360.0);
	int base = (int)(h * 60);
	double f = h * 6.0 - base;
	double p = b * (1.0 - s);
	double q = b * (1.0 - s * f);
	double t = b * (1.0 - s * (1.0 - f));
	switch (base) {
	case 0:
		return rgb(b, t, p);
	case 1:
		return rgb(q, b, p);
	case 2:
		return rgb(p, b, t);
	case 3:
		return rgb(p, q, b);
	case 4:
		return rgb(t, p, b);
	case 5:
		return rgb(b, p, q);
	default:
		throw ("HSB base is not valid.");
	}
}

Color Color::vector(const Vector3 &v)
{
	return Color(v.x(), v.y(), v.z());
}

Color Color::white()
{
	return Color(255, 255, 255);
}

Color Color::black()
{
	return Color(0, 0, 0);
}

Color Color::gray(double k)
{
	return Color(k, k, k);
}

void Color::setR(const double r)
{
	m_r = r;
}

void Color::setG(const double g)
{
	m_g = g;
}

void Color::setB(const double b)
{
	m_b = b;
}

double Color::r()
{
	return m_r;
}

double Color::g()
{
	return m_g;
}

double Color::b()
{
	return m_b;
}

Color & Color::operator+=(const Color &c)
{
	m_r += c.m_r;
	m_g += c.m_g;
	m_b += c.m_b;
	return *this;
}

Color Color::operator+(const Color &c) const
{
	return Color(m_r + c.m_r, m_g + c.m_g, m_b + c.m_b);
}

Color & Color::operator*=(const Color &c)
{
	m_r *= c.m_r;
	m_g *= c.m_g;
	m_b *= c.m_b;
	return *this;
}

Color Color::operator*(const Color &c) const
{
	return Color(m_r * c.m_r, m_g * c.m_g, m_b * c.m_b);
}

Color & Color::operator*=(double c)
{
	m_r *= c;
	m_g *= c;
	m_b *= c;
	return *this;
}

Color Color::operator*(double c) const
{
	return Color(m_r * c, m_g * c, m_b * c);
}

Color & Color::operator/=(const Color &c)
{
	m_r /= c.m_r;
	m_g /= c.m_g;
	m_b /= c.m_b;
	return *this;
}

Color Color::operator/(const Color &c) const
{
	return Color(m_r / c.m_r, m_g / c.m_g, m_b / c.m_b);
}

bool Color::operator==(const Color &c) const
{
	return m_r == c.m_r && m_g == c.m_g && m_b == c.m_b;
}

bool Color::operator!=(const Color &c) const
{
	return m_r != c.m_r || m_g != c.m_g || m_b != c.m_b;
}

Color & Color::pow(double c)
{
	m_r = std::pow(m_r, c);
	m_g = std::pow(m_g, c);
	m_b = std::pow(m_b, c);
	return *this;
}

double Color::luminance() const
{
	return
		0.212655 * m_r +
		0.715158 * m_g +
		0.072187 * m_b;
}

double Color::perceivedLightness() const
{
	double y = luminance();

	if (y <= 216.0 / 24389.0) {
		return y * (24389.0 / 27.0);
	}
	else {
		return std::pow(y, 1.0 / 3.0) * 116.0 - 16.0;
	}
}

double Color::inverseGamma(double v) const
{
	if (v <= 0.04045) {
		return v / 12.92;
	}
	else {
		return std::pow((v + 0.055) / 1.055, 2.4);
	}
}

double Color::gamma(double v) const
{
	if (v <= 0.0031308) {
		return v * 12.92;
	}
	else {
		return 1.055 * std::pow(v, 1.0 / 2.4) - 0.055;
	}
}

int Color::code()
{
	return
		(0xFF000000) |
		(toByte(m_r) << 16) |
		(toByte(m_g) << 8) |
		(toByte(m_b));
}

int Color::codeClamp()
{
	return
		(0xFF000000) |
		(toByteClamp(m_r) << 16) |
		(toByteClamp(m_g) << 8) |
		(toByteClamp(m_b));
}

bool Color::notZero() const
{
	return (m_r != 0) || (m_g != 0) || (m_b != 0);
}

int Color::toByte(const double x)
{
	return (int)(gamma(x) * 255 + 0.5);
}

int Color::toByteClamp(const double x)
{
	return std::min(toByte(x), 255);
}

Color::~Color()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Color destroyed. " << *this << std::endl;
	}
}