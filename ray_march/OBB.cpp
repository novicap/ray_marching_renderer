#include "OBB.h"
#include "OBBCGALAdapter.h"
#include <iostream>

void OBB::initStVertices() {
	m_vertices[0] = Vector3::vector(m_vertex.x(), m_vertex.y(), m_vertex.z());
	m_vertices[1] = Vector3::vector(m_vertex.x(), m_vertex.y(), -m_vertex.z());
	m_vertices[2] = Vector3::vector(m_vertex.x(), -m_vertex.y(), m_vertex.z());
	m_vertices[3] = Vector3::vector(m_vertex.x(), -m_vertex.y(), -m_vertex.z());
	m_vertices[4] = Vector3::vector(-m_vertex.x(), m_vertex.y(), m_vertex.z());
	m_vertices[5] = Vector3::vector(-m_vertex.x(), m_vertex.y(), -m_vertex.z());
	m_vertices[6] = Vector3::vector(-m_vertex.x(), -m_vertex.y(), m_vertex.z());
	m_vertices[7] = Vector3::vector(-m_vertex.x(), -m_vertex.y(), -m_vertex.z());
}

void OBB::calculateVolume()
{
	m_volume = width() * height() * depth();
}

void OBB::calculateCentroid()
{
	std::array<Point_3, 8> cgalPoints;
	for (int i = 0; i < 8; i++) {
		cgalPoints[i] = VectorPointCGALAdapter::toCGALPoint3(m_vertices[i]);
	}
	m_centroid = VectorPointCGALAdapter::toVector3(
		CGAL::centroid(std::begin(cgalPoints), std::end(cgalPoints))
	);
}

void OBB::initRay(const Ray & r)
{
	m_ray = Ray(r);

	m_rayTransformation.applyTo(m_ray);

	m_sdfVector = m_ray.source();
}

double OBB::sdf()
{
	Vector3 q = m_sdfVector.abs() - m_vertex;
	return q.max(0.0).length() + std::min(std::max(q.x(), std::max(q.y(), q.z())), 0.0);
}

void OBB::applyDistance(double dO)
{
	m_sdfVector = m_ray.source() + (m_ray.direction() * dO);
}

const Vector3 & OBB::sdfVector() const
{
	return m_sdfVector;
}

const Vector3 & OBB::vertex() const
{
	return m_vertex;
}

const Vector3 & OBB::vertex(int v) const
{
	if (v < 0 || v > 7) {
		throw("Vertex index out of range.");
	}
	return m_vertices[v];
}

const Vector3 & OBB::centroid() const
{
	return m_centroid;
}

OBB OBB::merge(const OBB & obb)
{
	std::vector<Point_3> points;
	for (auto& vertex : m_vertices) {
		points.push_back(VectorPointCGALAdapter::toCGALPoint3(vertex));
	}
	for (auto& vertex : obb.vertices()) {
		points.push_back(VectorPointCGALAdapter::toCGALPoint3(vertex));
	}

	std::array<Point_3, 8> cgalObb;
	//CGAL min obb
	CGAL::oriented_bounding_box(points, cgalObb, CGAL::parameters::use_convex_hull(true));

	return OBBCGALAdapter::toObb(cgalObb);
}

void OBB::transform(const Transform & t, bool append)
{
	Transform inverse = Transform(t).inverse();
	if (append) {
		m_transformations.push_back(t);
		m_inverseTransformations.push_back(inverse);
	}
	else {
		m_transformations.insert(m_transformations.begin(), t);
		m_inverseTransformations.insert(m_inverseTransformations.begin(), inverse);
	}
}

void OBB::applyTransformations()
{
	std::reverse(m_transformations.begin(), m_transformations.end());
	for (Vector3 &v : m_vertices) {
		for (Transform &t : m_transformations) {
			t.applyTo(v);
		}
	}
}

void OBB::applyInverseTransformations()
{
	for (Vector3 &v : m_vertices) {
		for (Transform &t : m_inverseTransformations) {
			t.applyTo(v);
		}
	}
}

const std::vector<Transform>& OBB::transformations() const
{
	return m_transformations;
}

void OBB::printVertices() const
{
	for (auto &v : m_vertices) {
		std::cout << v << std::endl;
	}
}

void OBB::printOBB() const
{
	std::cout << "SDF Veretex " << m_vertex << std::endl;
}

double OBB::width() const
{
	return 2 * m_vertex.x();
}

double OBB::height() const
{
	return 2 * m_vertex.y();
}

double OBB::depth() const
{
	return 2 * m_vertex.z();
}

double OBB::volume() const
{
	return m_volume;
}

const std::array<Vector3, 8>& OBB::vertices() const
{
	return m_vertices;
}

OBB::~OBB()
{
}