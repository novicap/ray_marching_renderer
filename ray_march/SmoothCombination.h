#pragma once
#include "Combination.h"
class SmoothCombination : public Combination
{
public:
	SmoothCombination(const std::shared_ptr<Shape> shape1, const std::shared_ptr<Shape> shape2, double smoothing) :
		Combination(shape1, shape2), m_smoothing{ smoothing } {
	
	};
	virtual const std::shared_ptr<Shape> clone() const = 0;
	virtual double sdf() override = 0;
	~SmoothCombination();
protected:
	void stream(std::ostream&) const;
	double m_smoothing;
};
