#include "ManyObjects.h"
#include "Box.h"
#include "Sphere.h"
#include "Cylinder.h"
#include "Torus.h"
#include "Cone.h"

void ManyObjects::init()
{
	std::vector<Body> bodies;
	int objects = 288;
	double width = 45;
	double depth = 45;
	std::shared_ptr<Box> table = std::make_shared<Box>(Vector3::vector(width, 0.1, depth));
	Body b = Body(table, Material(Color::rgb((double)rand() / RAND_MAX, (double)rand() / RAND_MAX, (double)rand() / RAND_MAX)));
	bodies.push_back(b);
	double t1 = -8;
	double t2 = -8;
	for (int i = 0; i < objects / 6; i++) {
		double radius = 0.1+((double)rand() / RAND_MAX)/3;
		std::shared_ptr<Sphere> s = std::make_shared<Sphere>(radius);
		Transform t = Transform::translation(Vector3::vector(t1, radius+0.1, t2));
		s->transform(t);
		s->applyBvhTransformations();
		t2++;
		if (t2 == 8) {
			t1 += 1;
			t2 = -8;
		}
		Body b = Body(s, Material(Color::rgb((double)rand() / RAND_MAX, (double)rand() / RAND_MAX, (double)rand() / RAND_MAX)));
		bodies.push_back(b);
	}
	t1 = -5;
	t2 = -8;
	for (int i = 0; i < objects / 6; i++) {
		double radius = 0.1 + ((double)rand() / RAND_MAX) / 6;
		double height = 0.1 + ((double)rand() / RAND_MAX) / 2;
		std::shared_ptr<Cylinder> c = std::make_shared<Cylinder>(height, radius);
		Transform t = Transform::rotationAroundX(45).andThen(Transform::translation(Vector3::vector(t1, 2*radius + 0.1, t2)));
		c->transform(t);
		c->applyBvhTransformations();
		t2++;
		if (t2 == 8) {
			t1 += 1;
			t2 = -8;
		}
		Body b = Body(c, Material(Color::rgb((double)rand() / RAND_MAX, (double)rand() / RAND_MAX, (double)rand() / RAND_MAX)));
		bodies.push_back(b);
	}
	t1 = -2.0;
	t2 = -8;
	for (int i = 0; i < objects / 3; i++) {
		double side = 0.1 + ((double)rand() / RAND_MAX)/4;
		std::shared_ptr<Box> x = std::make_shared<Box>(Vector3::vector(side, side, side));
		Transform t = Transform::translation(Vector3::vector(t1, side*sqrt(3) + 0.1, t2));
		Transform tt1 = Transform::rotationAroundZ(45);
		Transform tt2 = Transform::rotationAroundX(45);
		x->transform(t);
		x->transform(tt1);
		x->transform(tt2);
		x->applyBvhTransformations();
		t2++;
		if (t2 == 8) {
			t1 += 1;
			t2 = -8;
		}
		Body b = Body(x, Material(Color::rgb((double)rand() / RAND_MAX, (double)rand() / RAND_MAX, (double)rand() / RAND_MAX)));
		bodies.push_back(b);
	}
	t1 = 4.0;
	t2 = -8;
	for (int i = 0; i < objects / 6; i++) {
		double radius = 0.1 + ((double)rand() / RAND_MAX) / 15;
		double bigRadius = 0.15 + ((double)rand() / RAND_MAX) / 4;
		std::shared_ptr<Torus> c = std::make_shared<Torus>(radius, bigRadius);
		Transform t = Transform::rotationAroundX(-45).andThen(Transform::translation(Vector3::vector(t1, bigRadius+radius + 0.1, t2)));
		c->transform(t);
		c->applyBvhTransformations();
		t2++;
		if (t2 == 8) {
			t1 += 1;
			t2 = -8;
		}
		Body b = Body(c, Material(Color::rgb((double)rand() / RAND_MAX, (double)rand() / RAND_MAX, (double)rand() / RAND_MAX)));
		bodies.push_back(b);
	}
	t1 = 7.0;
	t2 = -8;
	for (int i = 0; i < objects / 6; i++) {
		double angle = 15 + ((double)rand() / RAND_MAX) * 5;
		double height = 1 + ((double)rand() / RAND_MAX) * 2;
		std::shared_ptr<Cone> s = std::make_shared<Cone>(angle, height);
		Transform t = Transform::translation(Vector3::vector(t1, height/2 + 0.1, t2));
		s->transform(t);
		s->applyBvhTransformations();
		t2++;
		if (t2 == 8) {
			t1 += 1;
			t2 = -8;
		}
		Body b = Body(s, Material(Color::rgb((double)rand() / RAND_MAX, (double)rand() / RAND_MAX, (double)rand() / RAND_MAX)));
		bodies.push_back(b);
	}

	std::vector<Light> lights{
		Light(Vector3::vector(-8.0, 3.5, -8.0), Color::gray(1.0)),
		Light(Vector3::vector(-4.0, 3.5, -8.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, -8.0), Color::gray(1.0)),
		Light(Vector3::vector(4.0, 3.5, -8.0), Color::gray(1.0)),
		Light(Vector3::vector(8.0, 3.5, -8.0), Color::gray(1.0)),
		Light(Vector3::vector(-8.0, 3.5, -4.0), Color::gray(1.0)),
		Light(Vector3::vector(-4.0, 3.5, -4.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, -4.0), Color::gray(1.0)),
		Light(Vector3::vector(4.0, 3.5, -4.0), Color::gray(1.0)),
		Light(Vector3::vector(8.0, 3.5, -4.0), Color::gray(1.0)),
		Light(Vector3::vector(-8.0, 3.5, 0.0), Color::gray(1.0)),
		Light(Vector3::vector(-4.0, 3.5, 0.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, 0.0), Color::gray(1.0)),
		Light(Vector3::vector(4.0, 3.5, 0.0), Color::gray(1.0)),
		Light(Vector3::vector(8.0, 3.5, 0.0), Color::gray(1.0)),
		Light(Vector3::vector(-8.0, 3.5, 4.0), Color::gray(1.0)),
		Light(Vector3::vector(-4.0, 3.5, 4.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, 4.0), Color::gray(1.0)),
		Light(Vector3::vector(4.0, 3.5, 4.0), Color::gray(1.0)),
		Light(Vector3::vector(8.0, 3.5, 4.0), Color::gray(1.0)),
		Light(Vector3::vector(-8.0, 3.5, 8.0), Color::gray(1.0)),
		Light(Vector3::vector(-4.0, 3.5, 8.0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 3.5, 8.0), Color::gray(1.0)),
		Light(Vector3::vector(4.0, 3.5, 8.0), Color::gray(1.0)),
		Light(Vector3::vector(8.0, 3.5, 8.0), Color::gray(1.0)),
	};

	Scene::addLights(lights);
	Scene::addBodies(bodies);
}

Color ManyObjects::backgroundColor() const
{
	return Color::gray(0.6);
}

Transform ManyObjects::cameraTransform()
{
	return Transform::rotationAroundX(45).andThen(Transform::translation(Vector3::vector(0, 12, -15)));
}

std::shared_ptr<Scene> ManyObjects::clone()
{
	std::shared_ptr<ManyObjects> scene = std::make_shared<ManyObjects>();
	for (auto& body : m_bodies) {
		scene->addBody(body.clone());
	}
	for (auto& light : m_lights) {
		scene->addLight(Light(light));
	}

	return scene;
}

const std::string ManyObjects::name() const
{
	return "Spheres And Boxes";
}

ManyObjects::~ManyObjects()
{
}
