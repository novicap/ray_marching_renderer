#pragma once
#include "OBB.h"
#include "Body.h"
#include "OBBCGALAdapter.h"

class OBBNode
{
public:
	OBBNode() : m_parent{ nullptr }, m_left{ nullptr }, m_right{ nullptr }, m_obb{ OBB() } {};

	//pass to boundingsphere node either vector of bodies or single body,
	//node will calculate boundingsphere box for itself with
	//calculateBoundingSphere method
	OBBNode(const std::vector<Body>& b) {
		m_parent = nullptr;
		m_left = nullptr;
		m_right = nullptr;
		addBodies(b);
		m_obb = calculateOBB();
		m_isUsableForCalculation = true;
	};

	OBBNode(const Body& b) {
		m_parent = nullptr;
		m_left = nullptr;
		m_right = nullptr;
		addBody(b);
		m_obb = calculateOBB();
		m_isUsableForCalculation = true;
	};

	OBBNode(const std::shared_ptr<OBBNode>& first,
		const std::shared_ptr<OBBNode>& second) {
		m_parent = nullptr;
		m_left = nullptr;
		m_right = nullptr;
		m_obb = first->obb().merge(second->obb());
		m_isUsableForCalculation = true;
	}

	//copy constructor for root nodes
	OBBNode(const OBBNode& obbNode) {
		m_parent = nullptr;
		m_left = nullptr;
		m_right = nullptr;
		m_obb = OBB(obbNode.m_obb);
		cloneBodies(obbNode.bodies());
		m_isUsableForCalculation = obbNode.m_isUsableForCalculation;
	}

	OBB& obb();

	void addBody(const Body& b);
	void addBodies(const std::vector<Body>& b);
	void cloneBodies(const std::vector<Body>& b);

	const std::vector<Body>& bodies() const;
	const bool isLeaf() const;
	const double volume() const;
	const std::shared_ptr<OBBNode>& left() const;
	const std::shared_ptr<OBBNode>& right() const;
	const std::shared_ptr<OBBNode>& parent() const;

	void addLeft(const std::shared_ptr<OBBNode>& node, const std::shared_ptr<OBBNode>& parent);
	void addRight(const std::shared_ptr<OBBNode>& node, const std::shared_ptr<OBBNode>& parent);

	bool isUsableForCalculation();
	void setCalculationUsability(bool isUsableForCalculation);
	~OBBNode();
private:
	std::shared_ptr<OBBNode> m_parent, m_left, m_right;
	int m_height;
	OBB m_obb;
	std::vector<Body> m_bodies;
	OBB calculateOBB();
	std::array<Vector3, 8> vertices();
	bool m_isUsableForCalculation;
};
