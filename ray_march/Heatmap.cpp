#include "Heatmap.h"
#include "Box.h"
#include "Union.h"
#include "SmoothUnion.h"
#include "Rook.h"
#include "Pawn.h"
#include "Bishop.h"
#include "King.h"

void Heatmap::init()
{

	Rook rook;
	Body whiteRook = rook.body();
	whiteRook.setMaterial(Material(Color::rgb(0.96, 0.96, 0.86)));

	std::vector<Body> bodies;
	bodies.push_back(whiteRook);

	std::vector<Light> lights{
		Light(Vector3::vector(-12.0, 6, -9), Color::gray(1.0)),
		Light(Vector3::vector(-8.0, 6, -9), Color::gray(1.0)),
		Light(Vector3::vector(-4.0, 6, -9), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 6, -9), Color::gray(1.0)),
		Light(Vector3::vector(4.0, 6, -9), Color::gray(1.0)),
		Light(Vector3::vector(8.0, 6, -9), Color::gray(1.0)),
		Light(Vector3::vector(12.0, 6, -9), Color::gray(1.0)),

		Light(Vector3::vector(-12.0, 6, 0), Color::gray(1.0)),
		Light(Vector3::vector(-8.0, 6, 0), Color::gray(1.0)),
		Light(Vector3::vector(-4.0, 6, 0), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 6, 0), Color::gray(1.0)),
		Light(Vector3::vector(4.0, 6, 0), Color::gray(1.0)),
		Light(Vector3::vector(8.0, 6, 0), Color::gray(1.0)),
		Light(Vector3::vector(12.0, 6, 0), Color::gray(1.0)),

		Light(Vector3::vector(-12.0, 6, 9), Color::gray(1.0)),
		Light(Vector3::vector(-8.0, 6, 9), Color::gray(1.0)),
		Light(Vector3::vector(-4.0, 6, 9), Color::gray(1.0)),
		Light(Vector3::vector(0.0, 6, 9), Color::gray(1.0)),
		Light(Vector3::vector(4.0, 6, 9), Color::gray(1.0)),
		Light(Vector3::vector(8.0, 6, 9), Color::gray(1.0)),
		Light(Vector3::vector(12.0, 6, 9), Color::gray(1.0)),
	};

	Scene::addLights(lights);
	Scene::addBodies(bodies);
}


Color Heatmap::backgroundColor() const
{
	return Color::rgb(0.239, 0.302, 0.365);
}

Transform Heatmap::cameraTransform()
{
	return Transform::rotationAroundX(45).andThen(Transform::translation(Vector3::vector(0.0, 3.0, -4)));
	//return Transform::rotationAroundX(45).andThen(Transform::translation(Vector3::vector(0.0, 15.0, -21))).andThen(Transform::rotationAroundY(-45));
}

std::shared_ptr<Scene> Heatmap::clone()
{
	std::shared_ptr<Heatmap> sb = std::make_shared<Heatmap>();

	for (auto& body : m_bodies) {
		sb->addBody(body.clone());
	}
	for (auto& light : m_lights) {
		sb->addLight(Light(light));
	}

	return sb;
}

const std::string Heatmap::name() const
{
	return "Chess board";
}

Heatmap::~Heatmap()
{
}
