#pragma once
#include "Combination.h"
class Subtraction : public Combination
{
public:
	Subtraction() = default;
	Subtraction(std::shared_ptr<Shape> shape1, std::shared_ptr<Shape> shape2) :
		Combination(shape1, shape2) {};

	const std::shared_ptr<Shape> clone() const
	{
		return std::make_shared<Subtraction>(*this);
	};

	double sdf() override;
	~Subtraction();
protected:
	void stream(std::ostream&) const;
};
