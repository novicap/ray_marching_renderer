#include "BoundingSphereNode.h"

void BoundingSphereNode::addBody(const Body& b)
{
	m_bodies.push_back(b);
}

void BoundingSphereNode::addBodies(const std::vector<Body>& b)
{
	for (auto &body : b) {
		m_bodies.push_back(body);
	}
}

void BoundingSphereNode::cloneBodies(const std::vector<Body>& b)
{
	for (auto &body : b) {
		addBody(Body(body.shape()->clone(), Material(body.material())));
	}
}

const std::vector<Body>& BoundingSphereNode::bodies() const
{
	return m_bodies;
}

BoundingSphere& BoundingSphereNode::boundingSphere()
{
	return m_boundingSphere;
}

double BoundingSphereNode::bodySetXAxisLength() const
{
	return abs(maxBodyXAxis() - minBodyXAxis());
}

double BoundingSphereNode::bodySetYAxisLength() const
{
	return abs(maxBodyYAxis() - minBodyYAxis());
}

double BoundingSphereNode::bodySetZAxisLength() const
{
	return abs(maxBodyZAxis() - minBodyZAxis());
}

const bool BoundingSphereNode::isLeaf() const
{
	return m_left == nullptr;
}

const double BoundingSphereNode::volume() const
{
	return m_boundingSphere.volume();
}

const std::shared_ptr<BoundingSphereNode>& BoundingSphereNode::left() const
{
	return m_left;
}

const std::shared_ptr<BoundingSphereNode>& BoundingSphereNode::right() const
{
	return m_right;
}

const std::shared_ptr<BoundingSphereNode>& BoundingSphereNode::parent() const
{
	return m_parent;
}

void BoundingSphereNode::addLeft(const std::shared_ptr<BoundingSphereNode>& node, const std::shared_ptr<BoundingSphereNode>& parent)
{
	m_left = node;
	m_parent = parent;
}

void BoundingSphereNode::addRight(const std::shared_ptr<BoundingSphereNode>& node, const std::shared_ptr<BoundingSphereNode>& parent)
{
	m_right = node;
	m_parent = parent;
}

BoundingSphere BoundingSphereNode::merge(const std::shared_ptr<BoundingSphereNode>& node)
{
	if (m_bodies.size() == 0 && node->bodies().size() == 0) {
		throw("No bodies to calculate BoundingSphere.");
	}
	std::vector<BoundingSphere_3> cgalSpheres;
	for (auto& body : m_bodies) {
		cgalSpheres.push_back(BoundingSphereCGALAdapter::toCGALSphere(body.boundingSphere()));
	}
	for (auto& body : node->bodies()) {
		cgalSpheres.push_back(BoundingSphereCGALAdapter::toCGALSphere(body.boundingSphere()));
	}

	//CGAL min sphere of spheres
	Min_sphere ms(cgalSpheres.begin(), cgalSpheres.end());

	//transform cgal minsphere to bounding sphere
	return BoundingSphereCGALAdapter::toBoundingSphere(ms);
}

BoundingSphere BoundingSphereNode::calculateBoundingSphere()
{
	if (m_bodies.size() == 0) {
		throw("No bodies to calculate BoundingSphere.");
	}

	//create vector of spheres compatible with CGAL
	std::vector<BoundingSphere_3> cgalSpheres;
	for (auto& body : m_bodies) {
		cgalSpheres.push_back(BoundingSphereCGALAdapter::toCGALSphere(body.boundingSphere()));
	}
	//CGAL min sphere of spheres
	Min_sphere ms(cgalSpheres.begin(), cgalSpheres.end());

	//transform cgal minsphere to bounding sphere
	return BoundingSphereCGALAdapter::toBoundingSphere(ms);
}

BoundingSphereNode::~BoundingSphereNode()
{
}

//to find longest axis we will use aabb as aabb should match minX, minY etc
//of body
double BoundingSphereNode::minBodyXAxis() const
{
	double minX = DBL_MAX;
	for (auto& body : m_bodies) {
		if (body.aabb().minX() < minX) {
			minX = body.aabb().minX();
		}
	}

	return minX;
}

double BoundingSphereNode::minBodyYAxis() const
{
	double minY = DBL_MAX;
	for (auto& body : m_bodies) {
		if (body.aabb().minY() < minY) {
			minY = body.aabb().minY();
		}
	}

	return minY;
}

double BoundingSphereNode::minBodyZAxis() const
{
	double minZ = DBL_MAX;
	for (auto& body : m_bodies) {
		if (body.aabb().minZ() < minZ) {
			minZ = body.aabb().minZ();
		}
	}

	return minZ;
}

double BoundingSphereNode::maxBodyXAxis() const
{
	double maxX = DBL_MIN;
	for (auto& body : m_bodies) {
		if (body.aabb().maxX() > maxX) {
			maxX = body.aabb().maxX();
		}
	}

	return maxX;
}

double BoundingSphereNode::maxBodyYAxis() const
{
	double maxY = DBL_MIN;
	for (auto& body : m_bodies) {
		if (body.aabb().maxY() > maxY) {
			maxY = body.aabb().maxY();
		}
	}

	return maxY;
}

double BoundingSphereNode::maxBodyZAxis() const
{
	double maxZ = DBL_MIN;
	for (auto& body : m_bodies) {
		if (body.aabb().maxZ() > maxZ) {
			maxZ = body.aabb().maxZ();
		}
	}

	return maxZ;
}