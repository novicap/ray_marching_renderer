#pragma once

#include "Vector3.h"
#include "Color.h"
#include <ostream>

class Light
{
public:
	Light(const Vector3 &p, const Color &c) : m_position{ p }, m_color{ c } {};
	Light(const Light& l) {
		m_position = Vector3(l.m_position);
		m_color = Color(l.m_color);
	}
	const Vector3& position() const;
	const Color& color() const;

	~Light();
	friend std::ostream& operator<<(std::ostream &strm, const Light &l) {
		return strm << "Light(" << l.m_position << ", " << l.m_color << ")";
	}
private:
	Vector3 m_position;
	Color m_color;
};
