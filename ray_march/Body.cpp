#include "Body.h"

#include "Config.h"
#include <iostream>

Body Body::clone()
{
	return Body(m_shape, m_material);
}

const Material& Body::material() const
{
	return m_material;
}

double Body::sdf() const
{
	return m_shape->sdf();
}

void Body::initRay(const Ray &r) const
{
	m_shape->initRay(r);
}

bool Body::isConvexOptimizable() const
{
	return m_shape->isConvexOptimizable();
}

const AABB& Body::aabb() const
{
	return m_shape->aabb();
}

const OBB & Body::obb() const
{
	return m_shape->obb();
}

const BoundingSphere & Body::boundingSphere() const
{
	return m_shape->boundingSphere();
}

void Body::transform(const Transform &t, bool append)
{
	m_shape->transform(t, append);
}

void Body::applyDistance(double dO) const
{
	m_shape->applyDistance(dO);
}

const bool Body::compareAABBX(const Body& o1, const Body& o2)
{
	return o1.aabb().centroid().x() < o2.aabb().centroid().x();
}

const bool Body::compareAABBY(const Body& o1, const Body& o2)
{
	return o1.aabb().centroid().y() < o2.aabb().centroid().y();
}

const bool Body::compareAABBZ(const Body& o1, const Body& o2)
{
	return o1.aabb().centroid().z() < o2.aabb().centroid().z();
}

const bool Body::compareOBBX(const Body & o1, const Body & o2)
{
	return o1.obb().centroid().x() < o2.obb().centroid().x();
}

const bool Body::compareOBBY(const Body & o1, const Body & o2)
{
	return o1.obb().centroid().y() < o2.obb().centroid().y();
}

const bool Body::compareOBBZ(const Body & o1, const Body & o2)
{
	return o1.obb().centroid().z() < o2.obb().centroid().z();
}

Vector3 Body::estNormal_(const Vector3 &v)
{
	return m_shape->estNormal_(v);
}

const std::shared_ptr<Shape>& Body::shape() const
{
	return m_shape;
}

void Body::setMaterial(const Material & m)
{
	m_material = m;
}

//Body & Body::operator=(const Body & a)
//{
//	m_shape = a.shape();
//	m_material = a.material();
//
//	return *this;
//}

Body::~Body()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Body destroyed." << std::endl;
	}
}

void Body::stream(std::ostream &strm) const
{
	strm << "Body (\n\t" << *m_shape << "\n\t" << m_material << "\n)";
}