#include "Light.h"
#include "Config.h"
#include <iostream>

const Color & Light::color() const
{
	return m_color;
}

const Vector3 & Light::position() const
{
	return m_position;
}

Light::~Light()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Light destroyed." << std::endl;
	}
}