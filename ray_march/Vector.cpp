#include "Vector.h"
#include "Config.h"
#include "Math.h"
#include <iostream>

Vector Vector::vector(double x, double y) {
	return Vector(x, y);
}

double Vector::x() const {
	return m_x;
}

double Vector::y() const {
	return m_y;
}

void Vector::setX(const double x)
{
	m_x = x;
}

void Vector::setY(const double y)
{
	m_y = y;
}

double Vector::length() const
{
	double x2 = m_x * m_x;
	double y2 = m_y * m_y;

	return Math::fastSqrt(x2 + y2);
}

Vector Vector::abs() const
{
	return Vector(std::abs(m_x), std::abs(m_y));
}

double Vector::dot(const Vector &v) const
{
	return m_x * v.m_x + m_y * v.m_y;
}

Vector Vector::max(const double d) const
{
	return Vector(std::max(m_x, d), std::max(m_y, d));
}

Vector Vector::min(const double d) const
{
	return Vector(std::min(m_x, d), std::min(m_y, d));
}

Vector & Vector::operator+=(const Vector &v)
{
	m_x += v.x();
	m_y += v.y();

	return *this;
}

Vector Vector::operator+(const Vector &v) const
{
	return Vector(m_x + v.x(), m_y + v.y());
}

Vector & Vector::operator-=(const Vector &v)
{
	m_x -= v.x();
	m_y -= v.y();

	return *this;
}

Vector Vector::operator-(const Vector &v) const
{
	return Vector(m_x - v.x(), m_y - v.y());
}

Vector & Vector::operator*=(const Vector &v)
{
	m_x *= v.x();
	m_y *= v.y();

	return *this;
}

Vector Vector::operator*(const Vector &v) const
{
	return Vector(m_x * v.x(), m_y * v.y());
}

Vector & Vector::operator/=(const Vector &v)
{
	m_x /= v.x();
	m_y /= v.y();

	return *this;
}

Vector Vector::operator/(const Vector &v) const
{
	return Vector(m_x / v.x(), m_y / v.y());
}

Vector & Vector::operator*=(double d)
{
	m_x *= d;
	m_y *= d;

	return *this;
}

Vector Vector::operator*(double d) const
{
	return Vector(m_x * d, m_y * d);
}

Vector & Vector::operator/=(double d)
{
	m_x /= d;
	m_y /= d;

	return *this;
}

Vector Vector::operator/(double d) const
{
	return Vector(m_x / d, m_y / d);
}

Vector Vector::operator-() const
{
	return Vector(-m_x, -m_y);
}

bool Vector::operator==(const Vector &v) const
{
	return m_x == v.x() && m_y == v.y();
}

bool Vector::operator!=(const Vector &v) const
{
	return  m_x != v.x() || m_y != v.y();;
}

Vector::~Vector()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Vector destroyed." << std::endl;
	}
}