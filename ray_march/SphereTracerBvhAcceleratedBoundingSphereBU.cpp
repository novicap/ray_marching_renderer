#include "SphereTracerBvhAcceleratedBoundingSphereBU.h"
#include <omp.h>
#include <math.h>
#include <iostream>
#include <algorithm>
#include <iostream>
#include <chrono>
#include <limits>
#include <queue>

single_object::SphereTracerBvhAcceleratedBoundingSphereBU::SphereTracerBvhAcceleratedBoundingSphereBU(
	std::shared_ptr<Scene> scene, const Camera& camera, const Vector& size) :
	m_scene{ scene }, m_camera{ std::make_shared<Camera>(camera) }, m_size{ size },
	m_boundingSphereTree{ std::make_shared<BoundingSphereTreeBottomUp>(scene->getBodies()) }
{
	m_colors = std::make_unique<Color[]>((int)m_size.x()*(int)m_size.y());
	m_iterations = 0;
}

void single_object::SphereTracerBvhAcceleratedBoundingSphereBU::renderIteration(
	Color*& pixelColors, int*& imageData, double& iterTime)
{
	//render timer start
	std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> elapsed;

	int ompThreads = OMP_THREADS;
	m_iterations += 1;

	//omp private vars
	double m;
	int width;
	int height;
	Vector pixel;
	Ray cameraRay;
	Color sample;
	Vector size;
	std::shared_ptr<Camera> camera;
	std::shared_ptr<Scene> scene;
	std::shared_ptr<BoundingSphereTree> boundingSphereTree;
	int iterations;
#pragma omp parallel num_threads(ompThreads) \
private(m, width, height, pixel, cameraRay, sample, size, camera, iterations, scene, boundingSphereTree)
	{
		//generate random
		std::uniform_real_distribution<double> unif(0.0, 1.0);
		std::default_random_engine re;
		re.seed((int)time(0));
		//init required private vars (others stay default constructed)
		width = (int)m_size.x();
		height = (int)m_size.y();
		iterations = m_iterations;
		m = std::min(m_size.x(), m_size.y());
		size = m_size / 2;
		//Clone scene for each thread, so that each thread has all of the objects required
		//for sdf. Cloning can not be avoided due to present racing condition in rayInit for shapes.
		//By cloning every thread has full scene.
		scene = m_scene->clone();
		camera = m_camera->clone();
		boundingSphereTree = m_boundingSphereTree->clone();
#pragma omp for
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int position = y * width + x;
				if (ANTI_ALIASING) {
					pixel.setX(((x - size.x() + unif(re)) / m));
					pixel.setY(((y - size.y() + unif(re)) / -m));
				}
				else {
					pixel.setX(((x - size.x()) / m));
					pixel.setY(((y - size.y()) / -m));
				}

				camera->sampleExitingRay(pixel, cameraRay);
				scene->cameraTransform().applyTo(cameraRay);

				//for image
				sample = sampleRay(cameraRay, *scene, *boundingSphereTree, 3, true);

				pixelColors[position] += sample;
				imageData[position] = (pixelColors[position] * (1.0 / iterations)).codeClamp();
			}
		}
	}
	std::chrono::high_resolution_clock::time_point finish = std::chrono::high_resolution_clock::now();
	elapsed = finish - start;
	iterTime = elapsed.count();
	//render timer finish
	std::cout << "Elapsed time: " << elapsed.count() << " s\n";
}

std::string single_object::SphereTracerBvhAcceleratedBoundingSphereBU::name()
{
	return "SphereTracerBvhAcceleratedBoundingSphereBU - Bounding Sphere Bottom Up";
}

void single_object::SphereTracerBvhAcceleratedBoundingSphereBU::initScene()
{
	m_scene->init();
}

Color single_object::SphereTracerBvhAcceleratedBoundingSphereBU::sampleRay(
	const Ray &ray, const Scene &scene, BoundingSphereTree &boundingSphereTree, int depthRemaining, bool primaryRay)
{
	if (depthRemaining == 0) {
		return Color::black();
	}

	Color heatmap = Color::black();
	double epsilon = EPSILON;
	double maxDist = MAX_DIST;

	std::vector<IntersectedBody> bodies = boundingSphereTree.traverseTreeST(ray, heatmap);
	ClosestBody closestBody = rayMarch(ray, bodies, primaryRay);

	primaryRay = false;

	if (OUTPUT_HEATMAP) {
		int heatmapSteps = closestBody.steps();
		heatmap.setR((heatmap.r() + heatmapSteps) / 255);
		heatmap.setG((heatmap.g() + heatmapSteps) / 255);
		heatmap.setB((heatmap.b() + heatmapSteps) / 255);
		return heatmap;
	}

	Vector3 collisionPoint = ray.source() + (ray.direction()*closestBody.distance());
	if (closestBody.distance() >= maxDist) {
		return scene.backgroundColor();
	}
	Vector3 normal_ = closestBody.closestBody().shape()->estNormal_(collisionPoint);	//normal at collision point
	Vector3 rayInverse = -ray.direction();												//inverse of ray
	Vector3 rayReflectionV = rayInverse.reflectN(normal_);								//reflection
	Vector3 marchSource = collisionPoint + ((normal_ * epsilon) * 2.0);
	double reflectionLength = rayReflectionV.length();

	Material material = closestBody.closestBody().material();
	//return material.diffuse();

	Color totalDiffuse = Color::black();
	Color totalSpecular = Color::black();

	for (int i = 0; i < scene.getLights().size(); i++) {
		Vector3 lightP = scene.getLights()[i].position();
		Vector3 lightV = lightP - collisionPoint;
		Vector3 lightV_ = lightV.normalized();

		//move ray from collision point to avoid accident ray march break
		Ray lightRay = Ray::sd(marchSource, lightV_);
		std::vector<IntersectedBody> bodies = boundingSphereTree.traverseTreeST(lightRay, heatmap);
		ClosestBody closestBodyLight = rayMarch(lightRay, bodies, primaryRay);
		//std::cout << bodies[0].distance() << std::endl;
		//if path to light source is not obstructed by some other object
		if (closestBodyLight.distance() >= lightV.length()) {
			//diffuse
			double lightVLengthSquare = lightV.lengthSquared();
			double lightVLength = Math::fastSqrt(lightVLengthSquare);
			double cosLightNormal = (lightV.dot(normal_) / lightVLength);

			if (cosLightNormal > 0) {
				totalDiffuse = totalDiffuse + (scene.getLights()[i].color() * (cosLightNormal / lightVLengthSquare));
			}
			//specular
			if (material.specular().notZero()) {
				double cosLightReflection = lightV.dot(rayReflectionV) / (lightVLength * reflectionLength);
				if (cosLightReflection > 0) {
					double phong = std::pow(cosLightReflection, material.shininess());
					totalSpecular = totalSpecular + (scene.getLights()[i].color() * (phong / lightVLengthSquare));
				}
			}
		}
	}

	Color result = Color::black();

	result = result + (totalDiffuse  * (material.diffuse()));
	result = result + (totalSpecular * (material.specular()));

	//reflection
	if (material.reflective().notZero()) {
		Ray source = Ray::sd(marchSource, rayReflectionV);
		Color reflection = sampleRay(source, scene, boundingSphereTree, depthRemaining - 1, primaryRay);
		result = result + (reflection * (material.reflective()));
	}

	return result;
}

ClosestBody single_object::SphereTracerBvhAcceleratedBoundingSphereBU::rayMarch(const Ray &r, const std::vector<IntersectedBody>& bodies, bool primaryRay)
{
	double maxDist = MAX_DIST;
	double epsilon = EPSILON;

	ClosestBody cb;
	/*if(bodies.size() > 0)
		std::cout << "Bodies size " << bodies.size() << std::endl;*/
	for (auto &intersectedBody : bodies) {
		//init ray inside body for each body
		//so every body has its own copy of ray
		//and all transformations associated with
		//ray and resulting vectors
		intersectedBody.body().initRay(r);

		//used for "convex" optimisation
		double prevDistance = DBL_MAX;
		//use for triangle optimization
		double tePrevDistance = 0;
		//2*elipson is to allow while loop to start
		double distance = 2 * epsilon;
		//entire ray length
		//apply distance already passed for this body
		//while traversing bvh
		//std::cout << intersectedBody.distance() << std::endl;
		double dO = 0.0;
		//secondary rays could be inside bounding box hence passed
		//distance will be < 0 for aab ray is inside
		//sidenote: for other BoundingSpheres distance would be useful but for now
		//complicates programming
		if (primaryRay) {
			dO = intersectedBody.distance();
			intersectedBody.body().applyDistance(dO);
		}
		//for heatmaps
		int stepCounter = 0;

		while (dO < maxDist && distance >= epsilon) {
			distance = intersectedBody.body().sdf();
			stepCounter++;
			//"convex optimisation"
			if (OPTIMISE_FOR_CONVEXITY && intersectedBody.body().isConvexOptimizable()) {
				if (distance < prevDistance) {
					prevDistance = distance;
				}
				else {
					dO = maxDist;
					break;
				}
			}
			//end of "convex optimisation"

			dO += distance;

			//"ray length optimisation"
			if (OPTIMISE_FOR_RAY_LENGTH) {
				if (dO > cb.distance()) {
					dO = maxDist;
					break;
				}
			}
			//end of "ray length optimisation"

			intersectedBody.body().applyDistance(dO);
		}

		if (dO < cb.distance()) {
			cb = ClosestBody(dO, intersectedBody.body(), stepCounter);
		}
	}

	return cb;
}


single_object::SphereTracerBvhAcceleratedBoundingSphereBU::~SphereTracerBvhAcceleratedBoundingSphereBU()
{
}

