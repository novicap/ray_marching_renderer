#pragma once
#include "Vector3.h"
#include "Material.h"
#include "Transform.h"
#include "Acceleratable.h"
#include "Marchable.h"
#include "Math.h"
#include <iostream>
#include <vector>

class Shape : public Acceleratable, public Marchable
{
public:
	Shape() = default;
	Shape(const Shape& shape) {
		m_ray = Ray(shape.m_ray);
		m_sdfVector = Vector3(shape.m_sdfVector);
		m_aabb = AABB(shape.m_aabb);
		m_obb = OBB(shape.m_obb);
		m_boundingSphere = BoundingSphere(shape.m_boundingSphere);
		m_isConvexOptimizable = shape.m_isConvexOptimizable;
		m_transformations = shape.m_transformations;
		m_inverseTransformations = shape.m_inverseTransformations;
		m_volume = shape.m_volume;
		m_isConvexOptimizable = shape.m_isConvexOptimizable;
	}

	virtual const std::shared_ptr<Shape> clone() const = 0;

	//calculates sdf for Vector3 m_sdfVector attribute
	virtual bool isConvexOptimizable();
	virtual void initRay(const Ray &r) override;
	virtual void applyDistance(double dO);
	virtual Vector3 estNormal_(const Vector3 &v);
	virtual void setSdfVector(const Vector3 &v);
	virtual void setAndTransformSdfVector(const Vector3 &v);
	virtual const AABB& aabb() const override;
	virtual const OBB& obb() const override;
	virtual const BoundingSphere& boundingSphere() const override;
	virtual void transform(const Transform &t, bool append = true);

	void recalculateAABB();
	void applyBvhTransformations();
	const Vector3& getSdfVector() const;
	Vector3 applyTransforms(const Vector3 &v);
	Vector3 applyInverseTransforms(const Vector3 &v);
	void transformAABB(const Transform &t);
	std::vector<Transform>& transformations();
	std::vector<Transform>& inverseTransformations();

	~Shape();

	friend std::ostream& operator<<(std::ostream &strm, const Shape &s) {
		s.stream(strm);
		return strm;
	}
protected:
	Ray m_ray;
	Vector3 m_sdfVector;
	Vector3 m_contactPoint;
	AABB m_aabb;
	OBB m_obb;
	BoundingSphere m_boundingSphere;
	double m_volume;
	virtual void stream(std::ostream& str) const = 0;
	bool m_isConvexOptimizable = true;
	std::vector<Transform> m_transformations;
	std::vector<Transform> m_inverseTransformations;
};
