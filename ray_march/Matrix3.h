#pragma once
#include "Vector3.h"
#include <array>

class Matrix3
{
public:
	Matrix3() = default;

	Matrix3(double a_00, double a_01, double a_02,
		double a_10, double a_11, double a_12,
		double a_20, double a_21, double a_22) :
		m_00{ a_00 }, m_01{ a_01 }, m_02{ a_02 },
		m_10{ a_10 }, m_11{ a_11 }, m_12{ a_12 },
		m_20{ a_20 }, m_21{ a_21 }, m_22{ a_22 } {};

	Matrix3(const Vector3& v1, const Vector3& v2, const Vector3& v3) :
		m_00{ v1.x() }, m_01{ v2.x() }, m_02{ v3.x() },
		m_10{ v1.y() }, m_11{ v2.y() }, m_12{ v3.y() },
		m_20{ v1.z() }, m_21{ v2.z() }, m_22{ v3.z() } {};

	Matrix3 inverse();

	Matrix3 minors();

	Matrix3 cofactors();

	Matrix3 transpose();

	double determinant();

	const std::array<double, 9> values() const;

	Matrix3 & operator+=(const Matrix3 &m);
	Matrix3 operator+(const Matrix3 &m) const;
	Matrix3 & operator-=(const Matrix3 &m);
	Matrix3 operator-(const Matrix3 &m) const;
	Matrix3 operator*(const Matrix3 &m) const;
	Matrix3 & operator*=(double d);
	Matrix3 operator*(double d) const;

	friend std::ostream& operator<<(std::ostream &strm, const Matrix3 &mat3) {
		return strm << "Matrix3(" << mat3.m_00 << ", " << mat3.m_01 << ", " << mat3.m_02 << "\n" <<
			mat3.m_10 << ", " << mat3.m_11 << ", " << mat3.m_12 << "\n" <<
			mat3.m_20 << ", " << mat3.m_21 << ", " << mat3.m_22 << ")";
	}

	~Matrix3();
private:
	double
		m_00, m_01, m_02,
		m_10, m_11, m_12,
		m_20, m_21, m_22;
};
