#include "Alteration.h"
#include "Config.h"
#include <iostream>

void Alteration::initRay(const Ray &r)
{
	m_shape->initRay(r);
}

void Alteration::applyDistance(double dO)
{
	m_shape->applyDistance(dO);
}

void Alteration::setSdfVector(const Vector3 & v)
{
	m_shape->setSdfVector(v);
}

void Alteration::setAndTransformSdfVector(const Vector3 & v)
{
	m_shape->setAndTransformSdfVector(v);
}

Vector3 Alteration::estNormal_(const Vector3 &v)
{
	double epsilon = EPSILON;
	setAndTransformSdfVector(v);
	double d = sdf();

	Vector3 px = v - Vector3::vector(epsilon, 0.0, 0.0);
	setAndTransformSdfVector(px);
	double sdfPx = sdf();

	Vector3 py = v - Vector3::vector(0.0, epsilon, 0.0);
	setAndTransformSdfVector(py);
	double sdfPy = sdf();

	Vector3 pz = v - Vector3::vector(0.0, 0.0, epsilon);
	setAndTransformSdfVector(pz);
	double sdfPz = sdf();

	Vector3 n = Vector3::vector(d - sdfPx, d - sdfPy, d - sdfPz);

	return n.normalized();
}

Alteration::~Alteration()
{
}