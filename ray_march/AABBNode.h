#pragma once
#include "Body.h"
#include <iostream>

class AABBNode
{
public:
	AABBNode() : m_parent{ nullptr }, m_left{ nullptr }, m_right{ nullptr }, m_aabb{ AABB() } {};

	//pass to aabb node either vector of bodies or single body, node will calculate aabb box for itself with
	//calculateAABB method
	AABBNode(const std::vector<Body>& b) {
		m_parent = nullptr;
		m_left = nullptr;
		m_right = nullptr;
		addBodies(b);
		m_aabb = calculateAABB();
	};

	AABBNode(const Body& b) {
		m_parent = nullptr;
		m_left = nullptr;
		m_right = nullptr;
		addBody(b);
		m_aabb = b.aabb();
	};

	AABBNode(const std::shared_ptr<AABBNode>& first,
		const std::shared_ptr<AABBNode>& second) {
		m_parent = nullptr;
		m_left = nullptr;
		m_right = nullptr;
		m_aabb = first->aabb().merge(second->aabb());
	}

	//copy constructor for root nodes
	AABBNode(const AABBNode& aabbNode) {
		m_parent = nullptr;
		m_left = nullptr;
		m_right = nullptr;
		m_aabb = AABB(aabbNode.m_aabb);
		cloneBodies(aabbNode.bodies());
	}

	AABB& aabb();

	void addBody(const Body& b);
	void addBodies(const std::vector<Body>& b);
	void cloneBodies(const std::vector<Body>& b);

	const std::vector<Body>& bodies() const;
	const bool isLeaf() const;
	const double volume() const;
	const std::shared_ptr<AABBNode>& left() const;
	const std::shared_ptr<AABBNode>& right() const;
	const std::shared_ptr<AABBNode>& parent() const;

	AABB merge(const std::shared_ptr<AABBNode>&);

	void addLeft(const std::shared_ptr<AABBNode>& node, const std::shared_ptr<AABBNode>& parent);
	void addRight(const std::shared_ptr<AABBNode>& node, const std::shared_ptr<AABBNode>& parent);

	~AABBNode();
private:
	std::shared_ptr<AABBNode> m_parent, m_left, m_right;
	int m_height;
	AABB m_aabb;
	std::vector<Body> m_bodies;
	AABB calculateAABB();
};
