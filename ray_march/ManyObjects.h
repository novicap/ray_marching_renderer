#pragma once
#include "Scene.h"

class ManyObjects : public Scene
{
public:
	ManyObjects() = default;

	void init() override;
	Color backgroundColor() const override;
	Transform cameraTransform() override;
	std::shared_ptr<Scene> clone();
	const std::string name() const override;

	~ManyObjects();
};

