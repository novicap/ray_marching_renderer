#pragma once
#include <vector>
#include <mutex>
#include "BufferRendering.h"

//not worked on
class TripleBuffering
{
public:
	TripleBuffering(std::shared_ptr<BufferRendering>, std::shared_ptr<BufferRendering>, std::shared_ptr<BufferRendering>);
	std::shared_ptr<BufferRendering> writingStart();
	void writingDone();
	std::shared_ptr<BufferRendering> readingStart();
	void readingDone();
	void write(std::shared_ptr<BufferRendering>);
	void read(std::shared_ptr<BufferRendering>);

	~TripleBuffering();
private:
	std::shared_ptr<BufferRendering> m_bufferWriting, m_bufferReading, m_bufferReadNext;
	std::vector<std::shared_ptr<BufferRendering>> m_buffers;
};
