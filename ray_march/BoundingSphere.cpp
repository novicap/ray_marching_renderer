#include "BoundingSphere.h"
#include "VectorPointCGALAdapter.h"
#include "BoundingSphereCGALAdapter.h"

BoundingSphere::BoundingSphere()
{
}

void BoundingSphere::initRay(const Ray & r)
{
	m_ray = Ray(r);
	m_rayTransformation.applyTo(m_ray);
	m_sdfVector = m_ray.source();
}

double BoundingSphere::sdf()
{
	return m_sdfVector.length() - m_radius;
}

void BoundingSphere::applyDistance(double dO)
{
	m_sdfVector = m_ray.source() + (m_ray.direction() * dO);
}

const Vector3 & BoundingSphere::sdfVector() const
{
	return m_sdfVector;
}

const double BoundingSphere::radius() const
{
	return m_radius;
}

const Vector3 & BoundingSphere::center() const
{
	return m_center;
}

void BoundingSphere::transform(const Transform & t, bool append)
{
	Transform inverse = Transform(t).inverse();
	if (append) {
		m_transformations.push_back(t);
		m_inverseTransformations.push_back(inverse);
	}
	else {
		m_transformations.insert(m_transformations.begin(), t);
		m_inverseTransformations.insert(m_inverseTransformations.begin(), inverse);
	}
}

void BoundingSphere::applyTransformations()
{
	std::reverse(m_transformations.begin(), m_transformations.end());
	for (Transform &t : m_transformations) {
		t.applyTo(m_center);
	}
	m_rayTransformation = Transform::translation(m_center).inverse();
}

const std::vector<Transform>& BoundingSphere::transformations() const
{
	return m_transformations;
}

const std::vector<Transform>& BoundingSphere::inverseTransformations() const
{
	return m_inverseTransformations;
}

const Transform & BoundingSphere::rayTransformation() const
{
	return m_rayTransformation;
}

double BoundingSphere::volume() const
{
	return m_volume;
}

double BoundingSphere::width() const
{
	return 2 * m_radius;
}

double BoundingSphere::height() const
{
	return 2 * m_radius;
}

double BoundingSphere::depth() const
{
	return 2 * m_radius;
}

BoundingSphere BoundingSphere::merge(const BoundingSphere &sphere)
{
	std::vector<BoundingSphere_3> cgalSpheres;
	cgalSpheres.push_back(BoundingSphereCGALAdapter::toCGALSphere(sphere));
	cgalSpheres.push_back(BoundingSphereCGALAdapter::toCGALSphere(*this));
	//CGAL min sphere of spheres
	Min_sphere ms(cgalSpheres.begin(), cgalSpheres.end());
	//boundingSphere
	return BoundingSphereCGALAdapter::toBoundingSphere(ms);
}

BoundingSphere::~BoundingSphere()
{
}

void BoundingSphere::calculateVolume()
{
	m_volume = m_radius * m_radius * m_radius * M_PI * 4 / 3;
}