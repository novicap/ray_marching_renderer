#pragma once
#include "Shape.h"
class Plane : public Shape
{
public:
	Plane(const Vector3& normal, double distanceFromOrigin) : m_normal{ normal }, m_distanceFromOrigin{ distanceFromOrigin } {};

	Plane(const Plane& plane) : Shape(plane)
	{
		m_normal = Vector3(plane.m_normal);
		m_distanceFromOrigin = plane.m_distanceFromOrigin;
	}

	const std::shared_ptr<Shape> clone() const override
	{
		return std::make_shared<Plane>(*this);
	}
	double sdf() override;
	~Plane();

	friend std::ostream& operator<<(std::ostream &strm, const Plane &p) {
		return strm << "Plane(normal:" << p.m_normal << ", d:" << p.m_distanceFromOrigin << ")";
	}
protected:
	void stream(std::ostream&) const;
private:
	Vector3 m_normal;
	double m_distanceFromOrigin;
};
