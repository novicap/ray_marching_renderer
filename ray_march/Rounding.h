#pragma once
#include "Alteration.h"
#include <memory>
class Rounding : public Alteration
{
public:
	Rounding(std::shared_ptr<Shape> shape, double roundingFactor) :
		Alteration(shape), m_roundingFactor{ roundingFactor } {};
	const std::shared_ptr<Shape> clone() const override;
	double sdf() override;
	~Rounding();
private:
	double m_roundingFactor;
protected:
	void stream(std::ostream& str) const;
};