#include "RayMarcher.h"
#include "Config.h"
#include "Math.h"
#include "Spheres.h"
#include <omp.h>
#include <math.h>
#include <iostream>
#include <algorithm>
#include <iostream>
#include <chrono>
#include <limits>

RayMarcher::RayMarcher(std::shared_ptr<Scene> scene, Camera& camera, Vector& size) :
	m_scene{ scene }, m_camera{ camera }, m_size{ size } {
	m_colors = std::make_unique<Color[]>((int)m_size.x()*(int)m_size.y());
	m_iterations = 0;
}

void RayMarcher::renderIteration(std::unique_ptr<Color[]>& pixelColors, std::unique_ptr<int[]>& imageData)
{
	m_iterations += 1;
	auto start = std::chrono::high_resolution_clock::now();
	
	//omp private vars
	double m;
	int width;
	int height;
	Vector pixel;
	Ray cameraRay;
	Color sample;
	Vector3 p;
	Vector size;
	Camera camera;
	std::shared_ptr<Scene> scene;
	int iterations;
	double sum = 0;
#pragma omp parallel num_threads(OMP_THREADS) \
private(m, width, height, pixel, cameraRay, sample, p, size, scene, camera, iterations)
	{
		//generate random
		std::uniform_real_distribution<double> unif(-0.5, 0.5);
		std::default_random_engine re;
		re.seed((int)time(0));
		
		//init required private vars (others stay default constructed)
		width = (int)m_size.x();
		height = (int)m_size.y();
		iterations = m_iterations;
		m = std::min(m_size.x(), m_size.y());
		size = m_size / 2;
		scene = m_scene;
		camera = m_camera;
//#pragma omp for
#pragma omp for reduction(+: sum)
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int position = y*width + x;
				pixel.setX(((x - size.x() + unif(re)) / m));
				pixel.setY(((y - size.y() + unif(re)) / -m));

				camera.sampleExitingRay(pixel, cameraRay);
				scene->cameraTransform().applyTo(cameraRay);
				
				//for image
				//sample = sampleRay(cameraRay, *scene, 3);

				/*for heatmap (return iterations in rayMarch, comment sampleRay(), 
				comment `omp for`, uncomment `omp for reduction`)*/
				double distance = rayMarch(cameraRay, *scene);
				sum += distance;
				sample.setR(distance/255);
				sample.setG(distance/255);
				sample.setB(distance/255);

				pixelColors[position] += sample;
				//imageData[position] = distance;
				imageData[position] = (pixelColors[position] * (1.0 / iterations)).codeClamp();
			}
		}
	}
	auto finish = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> elapsed = finish - start;
	std::cout << "Elapsed time: " << elapsed.count() << " s\n";
	if (sum > 0) {
		std::cout << "Ray marching iterations: " << (sum * m_scene->getBodies().size()) << std::endl;
	}
}

Color RayMarcher::sampleRay(Ray &ray, Scene &scene, int depthRemaining)
{
	if (depthRemaining == 0) {
		return Color::black();
	}

	double epsilon = EPSILON;
	double distance = rayMarch(ray, scene);
	Vector3 collisionPoint = ray.source() + (ray.direction() * distance);
	int bodyIndex = getBodyIndexClosestTo(collisionPoint, scene);
	if (bodyIndex == -1) {
		return scene.backgroundColor();
	}

	Vector3 normal_ = estNormal_(collisionPoint, scene);	//normal at collision point
	Vector3 rayInverse = -ray.direction();					//inverse of ray
	Vector3 rayReflectionV = rayInverse.reflectN(normal_);	//reflection
	Vector3 marchSource = collisionPoint + ((normal_ * epsilon) * 2.0);
	double reflectionLength = rayReflectionV.length();

	Material material = scene.getBodies()[bodyIndex].material();

	Color totalDiffuse = Color::black();
	Color totalSpecular = Color::black();

	for (int i = 0; i < scene.getLights().size(); i++) {
		Vector3 lightP = scene.getLights()[i].position();
		Vector3 lightV = lightP - collisionPoint;
		Vector3 lightV_ = lightV.normalized();

		//move ray from collision point to avoid accident ray march break
		Ray lightRay = Ray::sd(marchSource, lightV_);
		double lightDistance = rayMarch(lightRay, scene);
		//if path to light source is not obstructed by some other object
		if (lightDistance >= lightV.length()) {
			//diffuse
			double lightVLengthSquare = lightV.lengthSquared();
			double lightVLength = sqrt(lightVLengthSquare);
			double cosLightNormal = (lightV.dot(normal_) / lightVLength);

			if (cosLightNormal > 0) {
				totalDiffuse = totalDiffuse + (scene.getLights()[i].color() * (cosLightNormal / lightVLengthSquare));
				
			}
			//specular
			if (material.specular().notZero()) {
				double cosLightReflection = lightV.dot(rayReflectionV) / (lightVLength * reflectionLength);
				if (cosLightReflection > 0) {
					double phong = std::pow(cosLightReflection, material.shininess());
					totalSpecular = totalSpecular + (scene.getLights()[i].color() * (phong / lightVLengthSquare));
				}
			}
		}
	}

	Color result = Color::black();
	
	result = result + (totalDiffuse  * (material.diffuse()));
	result = result + (totalSpecular * (material.specular()));
	
	//reflection
	if (material.reflective().notZero()) {
		Ray source = Ray::sd(marchSource, rayReflectionV);
		Color reflection = sampleRay(source, scene, depthRemaining - 1);
		result = result + (reflection * (material.reflective()));
	}

	return result;
}

double RayMarcher::rayMarch(Ray &r, Scene& scene)
{
	double maxDist = MAX_DIST;
	int maxSteps = MAX_STEPS;
	double epsilon = EPSILON;

	double dO = 0.0;
	Vector3 p = r.source();
	double distance = 0.0;
	int i;
	for (i = 0; i < maxSteps; i++) {
		distance = distanceTo(p, scene);
		dO += distance;
		if (distance < epsilon || dO > maxDist) {
			break;
		}

		p = r.source() + (r.direction() * dO);
	}
	return i;
	//return dO;
}

double RayMarcher::distanceTo(Vector3 &v, Scene& scene) {
	if (scene.getBodies().size() == 0) {
		throw ("Scene is empty.");
	}

	double distance = scene.getBodies()[0].sdf(v);
	for (int i = 1; i < scene.getBodies().size(); i++) {
		distance = std::min(distance, scene.getBodies()[i].sdf(v));
	}
	
	return distance;
}

int RayMarcher::getBodyIndexClosestTo(Vector3 &v, Scene &scene) {
	if (scene.getBodies().size() == 0) {
		throw ("Scene is empty.");
	}
	double epsilon = EPSILON;
	epsilon += epsilon / 10;
	
	int bodyIndex = -1;
	for (int i = 0; i < scene.getBodies().size(); i++) {
		double dist = scene.getBodies()[i].sdf(v);
		if (dist <= epsilon) {
			bodyIndex = i;
		}
	}

	return bodyIndex;
}



Vector3 RayMarcher::estNormal_(Vector3 &v, Scene &scene) {
	double epsilon = EPSILON;
	double d = distanceTo(v, scene);
	Vector3 px = v - Vector3::vector(epsilon, 0.0, 0.0);
	Vector3 py = v - Vector3::vector(0.0, epsilon, 0.0);
	Vector3 pz = v - Vector3::vector(0.0, 0.0, epsilon);

	return Vector3::vector(d - distanceTo(px, scene), d - distanceTo(py, scene), d - distanceTo(pz, scene)).normalized();
}

RayMarcher::~RayMarcher()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "RayMarcher destroyed." << std::endl;
	}
}
