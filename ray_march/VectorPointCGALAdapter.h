#pragma once
#include "Vector3.h"
#include "CGAL_setup.h"

class VectorPointCGALAdapter
{
public:

	static Point_3 toCGALPoint3(const Vector3& v) {
		return Point_3(v.x(), v.y(), v.z());
	}

	static Vector_3 toCGALVector3(const Vector3& v) {
		return Vector_3(v.x(), v.y(), v.z());
	}

	static Vector3 toVector3(const Point_3& p) {
		return Vector3::vector(CGAL::to_double(p.x()), CGAL::to_double(p.y()), CGAL::to_double(p.z()));
	}

	static Vector3 toVector3(const Vector_3& v) {
		return Vector3::vector(CGAL::to_double(v.x()), CGAL::to_double(v.y()), CGAL::to_double(v.z()));
	}
};
