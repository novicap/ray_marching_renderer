#pragma once
#include "Vector3.h"
#include <ostream>

class Ray
{
public:
	Ray() : m_source{ Vector3::zero() }, m_direction{ Vector3::zero() } {};

	//copy constructor
	Ray(const Ray &r) : m_source{ r.m_source }, m_direction{ r.m_direction } {};

	static Ray sd(const Vector3 &source, const Vector3 &direction);
	static Ray sv(Vector3&, Vector3&);

	const Vector3& source() const;
	const Vector3& direction() const;

	void setSource(const Vector3 &source);
	void setDirection(const Vector3 &direction);

	Ray & normalized_();

	~Ray();

	friend std::ostream& operator<<(std::ostream &strm, const Ray &r)
	{
		return strm << "Ray(" << r.m_source << ", " << r.m_direction << ")";
	}
private:
	Ray(const Vector3 &source, const Vector3 &direction) : m_source{ source }, m_direction{ direction } {};
	Vector3 m_source, m_direction;
};
