#include "ClosestBody.h"
#include "Config.h"
#include <iostream>

Body & ClosestBody::closestBody()
{
	return m_closestBody;
}

int ClosestBody::steps() const
{
	return m_stepCounter;
}

double ClosestBody::distance() const
{
	return m_distance;
}

void ClosestBody::setDistance(double distance)
{
	m_distance = distance;
}

ClosestBody::~ClosestBody()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "ClosestBody destroyed." << std::endl;
	}
}