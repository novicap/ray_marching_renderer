#pragma once
#include "AABB.h"
#include "OBB.h"
#include "BoundingSphere.h"

class Acceleratable
{
public:
	virtual const AABB& aabb() const = 0;
	virtual const OBB& obb() const = 0;
	virtual const BoundingSphere& boundingSphere() const = 0;
};