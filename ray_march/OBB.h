#pragma once
#include "VectorPointCGALAdapter.h"
#include "Vector3.h"
#include "Transform.h"
#include <array>
#include <vector>
#include "Matrix3.h"

class OBB
{
public:
	OBB() = default;
	OBB(const Vector3& v) {
		m_vertex = Vector3(v);
		initStVertices();
		calculateVolume();
	}

	OBB(const std::array<Vector3, 8>& v) {
		//goal is to determina inverse ray transformations
		//cetnroid represents translation
		m_vertices = v;
		std::array<Vector3, 8> boxxObb = v;
		calculateCentroid();
		
		//m_vertices[0] is `our` box vertex under rotation
		double x = (m_vertices[0] - m_vertices[1]).length() / 2;
		double y = (m_vertices[0] - m_vertices[3]).length() / 2;
		double z = (m_vertices[0] - m_vertices[5]).length() / 2;

		//`our` box vertex
		Vector3 translatedVertex = Vector3(m_vertices[5]);
		Transform::translation(m_centroid).inverse().applyTo(translatedVertex);

		m_vertex = Vector3::vector(x, y, z);
		//my obb, that is centered obb
		std::array<Vector3, 8> box = {
			Vector3::vector(m_vertex.x(), m_vertex.y(), m_vertex.z()),
			Vector3::vector(m_vertex.x(), m_vertex.y(), -m_vertex.z()),
			Vector3::vector(m_vertex.x(), -m_vertex.y(), m_vertex.z()),
			Vector3::vector(m_vertex.x(), -m_vertex.y(), -m_vertex.z()),
			Vector3::vector(-m_vertex.x(), m_vertex.y(), m_vertex.z()),
			Vector3::vector(-m_vertex.x(), m_vertex.y(), -m_vertex.z()),
			Vector3::vector(-m_vertex.x(), -m_vertex.y(), m_vertex.z()),
			Vector3::vector(-m_vertex.x(), -m_vertex.y(), -m_vertex.z())
		};

		for (auto& vertex : boxxObb) {
			Transform::translation(m_centroid).inverse().applyTo(vertex);
		}
		//find rotation between m_vertex and translatedObbVertex;
		Matrix3 m1 = Matrix3(box[0], box[1], box[2]);
		Matrix3 m2 = Matrix3(boxxObb[5], boxxObb[0], boxxObb[4]);
		//rotation between centered obb, and calculated obb
		Transform rotation = Transform((m2 * m1.inverse()));
		Transform translation = Transform::translation(m_centroid);
		
		m_rayTransformation = translation.inverse().andThen(rotation.inverse());

		calculateVolume();
	}

	//copy constructor
	OBB(const OBB& obb) {
		m_vertex = Vector3(obb.m_vertex);
		m_sdfVector = Vector3(obb.m_sdfVector);
		m_centroid = Vector3(obb.m_centroid);
		m_rayTransformation = Transform(obb.m_rayTransformation);
		m_transformations = obb.m_transformations;
		m_inverseTransformations = obb.m_inverseTransformations;
		m_volume = obb.m_volume;

		for (int i = 0; i < 8; i++) {
			m_vertices[i] = Vector3(obb.m_vertices[i]);
		}
	}

	double sdf();
	void initRay(const Ray &r);
	void applyDistance(double dO);
	const Vector3& sdfVector() const;
	const Vector3& vertex() const;
	const Vector3& vertex(int v) const;
	const Vector3& centroid() const;

	OBB merge(const OBB& obb);

	void transform(const Transform &t, bool append = true);
	//isComplexShapeObb is quick fix for double applying transformation problem
	void applyTransformations();
	void applyInverseTransformations();
	const std::vector<Transform>& transformations() const;
	void printVertices() const;
	void printOBB() const;

	double width() const;
	double height() const;
	double depth() const;
	void calculateCentroid();

	double volume() const;
	const std::array<Vector3, 8>& vertices() const;

	friend std::ostream& operator<<(std::ostream& strm, const OBB& obb) {
		return strm << "OBB: " << obb.vertex() << std::endl;
	}

	~OBB();
private:
	double m_volume = DBL_MAX;
	Vector3 m_vertex;
	Vector3 m_sdfVector;
	Vector3 m_centroid;
	Ray m_ray;
	std::array<Vector3, 8> m_vertices;
	Transform m_rayTransformation;
	std::vector<Transform> m_transformations;
	std::vector<Transform> m_inverseTransformations;
	int lastTransform;
	void calculateVolume();
	void initStVertices();
};