#pragma once

#include "Shape.h"

class Torus : public Shape
{
public:
	Torus(double smallRadius, double bigRadius) :
		m_smallRadius{ smallRadius }, m_bigRadius{ bigRadius } {
		m_aabb = AABB(
			Vector3::vector((m_bigRadius + m_smallRadius), m_smallRadius, (m_bigRadius + m_smallRadius))
		);
		m_obb = OBB(Vector3::vector(
			(m_bigRadius + m_smallRadius), m_smallRadius, (m_bigRadius + m_smallRadius)
		));
		m_boundingSphere = BoundingSphere((m_bigRadius + m_smallRadius));

		m_volume = (M_PI * m_smallRadius * m_smallRadius)*(2 * M_PI * m_bigRadius);
	};

	Torus(const Torus& torus) : Shape(torus) {
		m_smallRadius = torus.m_smallRadius;
		m_bigRadius = torus.m_bigRadius;
		m_isConvexOptimizable = torus.m_isConvexOptimizable;
	};

	const std::shared_ptr<Shape> clone() const override
	{
		return std::make_shared<Torus>(*this);
	};

	double sdf() override;
	virtual bool isConvexOptimizable() override;
	~Torus();
protected:
	bool m_isConvexOptimizable = false;
	void stream(std::ostream&) const;
private:
	double m_smallRadius;
	double m_bigRadius;
};
