#include "SmoothCombination.h"
#include "Config.h"
#include <iostream>

SmoothCombination::~SmoothCombination()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "SmoothCombination destroyed." << std::endl;
	}
}

void SmoothCombination::stream(std::ostream &strm) const
{
	strm << "SmoothCombination(\n\tFirst: " << *m_firstOperand << ",\n\tSecond: " << *m_secondOperand << ")";
}