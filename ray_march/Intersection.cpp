#include "Intersection.h"
#include "Config.h"
#include <iostream>

double Intersection::sdf()
{
	double firstSdf = m_firstOperand->sdf();
	double secondSdf = m_secondOperand->sdf();

	return std::max(firstSdf, secondSdf);
}

void Intersection::stream(std::ostream &strm) const
{
	strm << "Intersection(\n\tFirst: " << *m_firstOperand << ",\n\tSecond: " << *m_secondOperand << ")";
}

Intersection::~Intersection()
{
	if (OUTPUT_DESTRUCTORS) {
		std::cout << "Intersection destroyed." << std::endl;
	}
}