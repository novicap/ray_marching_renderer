#pragma once
#include "BoundingSphereTree.h"
#include "BoundingSphereNode.h"
#include "IntersectedBody.h"
class BoundingSphereTreeTopDown : public BoundingSphereTree
{
public:
	BoundingSphereTreeTopDown(const std::vector<Body>& bodies);
	~BoundingSphereTreeTopDown();
private:
	void buildTopDown(const std::shared_ptr<BoundingSphereNode>& parent,
		const std::vector<Body>& b, int start, int end, bool isLeftChild);

	std::function<bool(const Body&, const Body&)>
		sortMethod(const std::shared_ptr<BoundingSphereNode>& node);
};
